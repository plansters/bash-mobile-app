import React from 'react';
import {
    TouchableNativeFeedback,
    View,
    TouchableWithoutFeedback,
    Text, ScrollView, TouchableOpacity, Platform,
    ActivityIndicator,
} from 'react-native';
import {
    Touchable,
    Cart,
    colors,
    Search,
    typography,
    LinearGradient,
    wp,
    hp,
    ApplicationLayout,
    ArrowLeft,
    AppStyle,
    localize,
    ToggleDrawer,
    Logo,
    Loader,
    Header,
    SecondHeader,

} from './../../all';
import {NAVIGATION_SEARCH_SCREEN} from '../../navigation/types';

class SearchHeader extends React.Component {


    constructor(props) {
        super(props);


        this.toggleDrawerAction = this.toggleDrawerAction.bind(this);
        this.toggleSearchScreen = this.toggleSearchScreen.bind(this);
        this.DrawerGoBack = this.DrawerGoBack.bind(this);
    }


    toggleDrawerAction = () => {
        setTimeout(function () {

            this.props.navigation.toggleDrawer();

        }.bind(this), AppStyle.timeOut);
    };

    toggleSearchScreen = () => {
        setTimeout(function () {

            this.props.navigation.navigate(NAVIGATION_SEARCH_SCREEN);

        }.bind(this), AppStyle.timeOut);

    };
    DrawerGoBack = () => {
        setTimeout(function () {

            this.props.navigation.goBack();

        }.bind(this), AppStyle.timeOut);

    };


    render() {

        const {children, ...props} = this.props;

        return (

            <View {...props} style={{flex: 1}}>

                <View style={{flex: 1, zIndex: 1000}} {...this.props}>


                    <View style={{flex: 1}} {...props}>

                        {!this.props.didFinishInitialAnimation ?
                            <Loader didFinishInitialAnimation={this.props.didFinishInitialAnimation}/>
                            :

                            <View style={{
                                backgroundColor: 'white',
                                flex: 1,
                                paddingTop: Platform.OS=="ios"?hp('6%'):hp("2%"),
                            }}>

                                {this.props.type !== 'home' ?
                                    <SecondHeader type={this.props.HeaderSecondType} {...this.props}/> : null}
                                {children}
                            </View>
                        }
                    </View>
                </View>

            </View>

        );
    }
}

const styles = {};
export default SearchHeader;

