import React from 'react';
import {View, Text, Image, ImageBackground} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
// import Spinner from 'react-native-loading-spinner-overlay';
// import AppStyles from '../../constants/AppStyles';
import colors from './../../constants/colors'

class ApplicationLayout extends React.Component {


    render() {

        const {children, ...props} = this.props;

        return (

            <View style={{flex: 1,}}>
                {/*<Spinner*/}
                {/*visible={this.props.isLoading}*/}
                {/*textContent={''}*/}
                {/*textStyle={AppStyles.spinnerText}*/}
                {/*/>*/}
                <View style={{flex:1}} {...props}>
                    {children}
                </View>

            </View>

        );
    }
}

export default ApplicationLayout;
