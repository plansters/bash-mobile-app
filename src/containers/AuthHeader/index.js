import React from 'react';
import {Platform, ScrollView, View, Text, ImageBackground, TouchableOpacity, I18nManager, Image} from 'react-native';
import {
    typography,
    LinearGradient,
    wp,
    hp,
    ApplicationLayout,
    ArrowLeft,
    AppStyle,
    localize,
    Loader, colors,
} from './../../all';

class AuthHeader extends React.Component {


    constructor(props) {
        super(props);
        // //////////console.log(this.props)

    }

    goBackAction = () => {

        this.props.navigation.goBack();
    };

    render() {

        const {children, ...props} = this.props;

        return (

            <ApplicationLayout {...props}>

                <ScrollView>
                    <View style={{flex: 1,}}>
                        <View style={[AppStyle.CenterBottomLeftContent, {flex: 1}]}>

                            <TouchableOpacity onPress={this.goBackAction} style={[styles.ArrowPadding]}>
                                <ArrowLeft width={wp('10%')} height={wp('10%')}/>
                            </TouchableOpacity>
                        </View>
                        <View stly={[AppStyle.CenterContent, {}]}>
                            <LinearGradient style={[AppStyle.CenterContent, styles.TitlePagePadding]}
                                            colors={AppStyle.GradientColorDark}><Text
                                style={[typography.Text24penSansRegular, AppStyle.FontWhite]}>{this.props.titleHeader}</Text></LinearGradient>
                        </View>
                        <View style={{flex: 6}} {...props}>

                            <View style={{flex: 1}}>
                                {children}
                            </View>

                        </View>
                        {this.props.isLoadingAction?<View style={{position:'absolute',flex:1, backgroundColor:colors.LoaderTransparent,width:wp("100%"),height:hp("140%")}}>
                            <Loader didFinishInitialAnimation={!this.props.isLoadingAction} />

                        </View>:null}
                    </View>


                </ScrollView>
            </ApplicationLayout>

        );
    }
}

const styles = {

    ArrowPadding: {
        paddingBottom: hp('1%'),
        paddingEnd: hp('3%'),
        paddingStart: hp('2%'),
        paddingTop: Platform.OS === 'ios' ? hp('8%') : hp('5%'),
    },

    TitlePagePadding: {
        paddingBottom: hp('3%'),
        paddingTop: hp('3%'),
    },

};

export default AuthHeader;
