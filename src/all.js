// Pages
import localize from './locales/i18n';
import colors from './constants/colors';
import typography from './constants/typography';
import actionType from './constants/actionsType';
import Logo from './assets/images/Logo.svg';
import OrderIcon from './assets/images/order.svg';
import ArrowLeft from './assets/icons/arrow_left.svg';
import ArrowTop from './assets/icons/arrow_top.svg';
import ArrowBottom from './assets/icons/arrow_bottom.svg';
import EyeActive from './assets/icons/eye_active.svg';
import EyeUnActive from './assets/icons/eye_unactive.svg';
import ToggleDrawer from './assets/icons/toggle.svg';
import Close from './assets/icons/close.svg';
import Search from './assets/icons/search.svg';
import Cart from './assets/icons/cart.svg';
import Instagram from './assets/icons/instgram.svg';
import Phone from './assets/icons/phone.svg';
import Facebook from './assets/icons/facebook.svg';
import CodePush from 'react-native-code-push';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ApplicationLayout from './containers/Layout';
import Storage from 'react-native-storage';
import AsyncStorage from '@react-native-community/async-storage';
import AppStyle from './constants/AppStyles';
import DefaultHeader from './containers/DefaultHeader';
import SearchHeader from './containers/SearchHeader';
import AuthHeader from './containers/AuthHeader';
import LinearGradient from 'react-native-linear-gradient';
import CartProductItem from './components/elements/CartProductItem/index';
import InputElement from './components/elements/TextInput/index';
import TextUnderLine from './components/elements/TextUnderLine/index';
import CustomButton from './components/elements/CustomButton/index';
import ButtonTransparent from './components/elements/ButtonTransparent/index';
import ButtonStroked from './components/elements/ButtonStroked/index';
import Touchable from './components/elements/Touchable/index';
import Category from './components/elements/Category/index';
import Product from './components/elements/Product/index';
import Loader from './components/elements/Loader/index';
import ProductAttributes from './components/elements/ProductAttributes/index';
import ProductAttributesVariable from './components/elements/ProductAttributesVariable/index';
import ProductAttributesListVariable from './components/elements/ProductAttributesListVariable/index';
import CategoryForCollapse from './components/elements/CategoryForCollapse/index';
import CollapsibleItem from './components/elements/CollapsibleItem/index';
import CategoryList from './components/elements/CategoryList/index';
import CollapsibleList from './components/elements/CollapsibleList/index';
import CartProductList from './components/elements/CartProductList/index';
import MyOrderList from './components/elements/MyOrderList/index';
import MyOrderItem from './components/elements/MyOrderItem/index';
import ProductListTwoRowHorizontal from './components/elements/ProductListTwoRowHorizontal/index';
import ProductListTwoColumnVertical from './components/elements/ProductListTwoColumnVertical/index';
import RNPickerSelect from 'react-native-picker-select';
import Swiper from 'react-native-swiper';
import FastImage from 'react-native-fast-image';
import Header from './components/template/Header/index';
import SecondHeader from './components/template/SecondHeader/index';
import FooterSocialMedia from './components/template/FooterSocialMedia/index';
import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';
import ImageViewer from 'react-native-image-zoom-viewer';
import Tabs from 'react-native-tabs';
import Reactron from 'reactotron-react-native';
import axios from 'axios';
import { connect} from 'react-redux';
import AwesomeAlert from 'react-native-awesome-alerts';

export {
    localize,
    colors,
    typography,
    actionType,
    Logo,
    CodePush,
    wp,
    hp,
    ApplicationLayout, Storage,
    AsyncStorage,
    AppStyle,
    AuthHeader,
    DefaultHeader,
    ArrowLeft,
    LinearGradient,
    InputElement,
    EyeActive,
    EyeUnActive,
    TextUnderLine,
    CustomButton,
    RNPickerSelect,
    Search,
    Cart,
    ToggleDrawer,
    Close,
    Instagram,
    Facebook,
    Phone,
    Swiper,
    FastImage,
    Category, CategoryList,
    Product, ProductListTwoRowHorizontal,
    ButtonTransparent,
    ButtonStroked,
    Touchable,
    ProductListTwoColumnVertical,
    Header,
    Loader,
    SecondHeader,
    FooterSocialMedia,
    Collapsible,
    Accordion,
    CategoryForCollapse, CollapsibleItem, CollapsibleList,
    ArrowTop, ArrowBottom, CartProductItem, CartProductList,
    ImageViewer,
    Tabs,
    ProductAttributes, ProductAttributesVariable, ProductAttributesListVariable,
    MyOrderItem, MyOrderList,
    OrderIcon,
    SearchHeader,
    connect,
    Reactron,
    axios,
    AwesomeAlert
};



