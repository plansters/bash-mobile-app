import React from 'react';
import {Text, Platform, View, ScrollView, TouchableOpacity} from 'react-native';
import {
    ArrowLeft,
    Facebook,
    Phone,
    Instagram,
    colors,
    typography,
    Logo,
    wp,
    hp,
    Close,
    AppStyle,
    localize,
} from './../../../all';
import {
    NAVIGATION_HOME2_SCREEN,
    NAVIGATION_HOME_SCREEN,
    NAVIGATION_OFFER_HOME,
    NAVIGATION_OFFER_SCREEN, NAVIGATION_SEARCH_SCREEN,
} from '../../../navigation/types';

class SecondHeader extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Welcome', header: null};

    logged = false;

    constructor(props) {
        super(props);

        this.DrawerGoBack = this.DrawerGoBack.bind(this);
    }

    DrawerGoBack = () => {
        setTimeout(function () {

            this.props.navigation.goBack();

        }.bind(this), AppStyle.timeOut);

    };

    render() {

        const {children, ...props} = this.props;

        return (

            <View style={[this.props.HeaderSecondType == 'absolute' ? styles.navBackAbsolute : styles.navBack, {}]}>
                <Text
                    style={[typography.Text22OpenSansBold, AppStyle.FontYellow]}>{this.props.headerTitle}</Text>
                <TouchableOpacity onPress={this.DrawerGoBack}><ArrowLeft width={wp('8%')}
                                                                         height={wp('8%')}/></TouchableOpacity>
            </View>


        );
    }
}

const styles = {

    navBack: {
        //position: 'absolute',
        alignItems: 'center',
        paddingHorizontal: wp('6%'),
        justifyContent: 'space-between',
        flexDirection: 'row',
        // backgroundColor: 'red',
        paddingBottom: wp('3%'),
        paddingTop: wp('4%'),
        //    zIndex: 9,
        //    left: 0,
        //   top: 0,
        width: wp('100%'),
    },
    navBackAbsolute: {
        position: 'absolute',
        alignItems: 'center',
        paddingHorizontal: wp('6%'),
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingBottom: wp('3%'),
        paddingTop: wp('8%'),
        zIndex: 9,
        left: 0,
        top: 0,
        width: wp('100%'),
    },

};

export default SecondHeader;
