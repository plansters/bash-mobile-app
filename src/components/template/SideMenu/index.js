import React from 'react';
import {Image, Text, Platform, View, ScrollView, TouchableOpacity, InteractionManager} from 'react-native';
import {FooterSocialMedia, colors, typography, Logo, wp, hp, Close, AppStyle, localize} from './../../../all';
import {
    NAVIGATION_ABOUT_SCREEN, NAVIGATION_APP,
    NAVIGATION_CART_SCREEN,
    NAVIGATION_CATEGORY_SCREEN, NAVIGATION_COMPLAINS_SCREEN,
    NAVIGATION_HOME2_SCREEN,
    NAVIGATION_HOME_SCREEN,
    NAVIGATION_OFFER_HOME,
    NAVIGATION_OFFER_SCREEN, NAVIGATION_PROFILE_SCREEN, NAVIGATION_SEARCH_SCREEN,
} from '../../../navigation/types';

class SideMenu extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Welcome', header: null};

    logged = false;

    constructor(props) {
        super(props);
        this.state = {
            didFinish: false,
        };

        setTimeout(function () {
            this.setState({
                didFinish: true,
            });

        }.bind(this), 1000);


        this.closeDrawer = this.closeDrawer.bind(this);
        this.toggleHome = this.toggleHome.bind(this);
        this.toggleSearchOffer = this.toggleSearchOffer.bind(this);
        this.toggleCategory = this.toggleCategory.bind(this);
        this.toggleCart = this.toggleCart.bind(this);
    }


    closeDrawer = () => {

        this.props.navigation.closeDrawer();
        //
        // setTimeout(function () {
        //
        // }.bind(this), AppStyle.timeOut);

    };
    toggleHome = () => {
        this.props.navigation.navigate(NAVIGATION_HOME_SCREEN);


        // setTimeout(function () {
        //
        // }.bind(this), AppStyle.timeOut);

    };
    toggleCart = () => {
        this.props.navigation.navigate(NAVIGATION_CART_SCREEN);

        // setTimeout(function () {
        //
        //
        // }.bind(this), AppStyle.timeOut);

    };
    toggleProfile = () => {

        this.props.navigation.navigate(NAVIGATION_PROFILE_SCREEN);
        //
        // setTimeout(function () {
        //
        //
        // }.bind(this), AppStyle.timeOut);

    };
    toggleLoginRegister = () => {

        this.props.navigation.navigate(NAVIGATION_APP);

        // setTimeout(function () {
        //
        //
        // }.bind(this), AppStyle.timeOut);

    };
    toggleCategory = () => {

        this.props.navigation.navigate(NAVIGATION_CATEGORY_SCREEN);

        // setTimeout(function () {
        //
        //
        // }.bind(this), AppStyle.timeOut);

    };
    toggleSearchOffer = () => {
        this.props.navigation.navigate(NAVIGATION_OFFER_SCREEN);

        // setTimeout(function () {
        //
        //
        // }.bind(this), AppStyle.timeOut);


    };
    toggleComplains = () => {

        this.props.navigation.navigate(NAVIGATION_COMPLAINS_SCREEN);
        //
        // setTimeout(function () {
        //
        //
        // }.bind(this), AppStyle.timeOut);


    };

    toggleAboutUs = () => {
        this.props.navigation.navigate(NAVIGATION_ABOUT_SCREEN);

        // setTimeout(function () {
        //
        //
        // }.bind(this), AppStyle.timeOut);


    };


    render() {

        return (

            <View>
                <ScrollView>

                    {!this.state.didFinish ?
                        null
                        :
                        <View style={{flex: 1}}>
                            <View style={[AppStyle.CenterBottomLeftContent, {}]}>
                                <TouchableOpacity onPress={this.closeDrawer} style={[styles.paddingClose]}>
                                    <Close width={hp('10%')} height={hp('10%')}/>
                                </TouchableOpacity>
                            </View>
                            <View style={[AppStyle.CenterContent, styles.paddingLogo]}>
                                <Image
                                    style={{
                                        width: hp('34%'),
                                        height: hp('17%'),
                                    }}
                                    source={require('./../../../../src/assets/images/Logo.png')}
                                />
                                {/*<Logo width={hp('22%')} height={hp('22%')}/>*/}
                            </View>
                            <View style={[AppStyle.CenterContent, styles.itemDrawer]}>
                                <TouchableOpacity style={[styles.itemDrawerTouch]}
                                                  onPress={this.toggleHome}>
                                    <Text style={[typography.Text18OpenSansRegular]}>{localize.t('home')}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[AppStyle.CenterContent, styles.itemDrawer]}>
                                <TouchableOpacity style={[styles.itemDrawerTouch]}
                                                  onPress={this.toggleAboutUs}>
                                    <Text style={[typography.Text18OpenSansRegular]}> {localize.t('about_us')}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[AppStyle.CenterContent, styles.itemDrawer]}>
                                <TouchableOpacity style={[styles.itemDrawerTouch]}
                                                  onPress={this.toggleCategory}>
                                    <Text style={[typography.Text18OpenSansRegular]}> {localize.t('buy')}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[AppStyle.CenterContent, styles.itemDrawer]}>
                                <TouchableOpacity style={[styles.itemDrawerTouch]} onPress={this.toggleSearchOffer}>
                                    <Text style={[typography.Text18OpenSansRegular]}> {localize.t('offers')}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[AppStyle.CenterContent, styles.itemDrawer]}>
                                <TouchableOpacity style={[styles.itemDrawerTouch]} onPress={this.toggleComplains}>
                                    <Text style={[typography.Text18OpenSansRegular]}> {localize.t('report')}</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={[AppStyle.CenterContentRow, {
                                justifyContent: 'space-around',
                                marginHorizontal: hp('4%'),
                                marginVertical: hp('3.5%'),
                            }]}>
                                {global.logged == true ? <TouchableOpacity style={[AppStyle.itemButtonAutoWidth]}
                                                                           onPress={this.toggleProfile}>
                                    <Text style={[typography.Text14OpenSansRegular]}>{localize.t('profile')}</Text>
                                </TouchableOpacity> : <TouchableOpacity style={[AppStyle.itemButtonAutoWidth]}
                                                                        onPress={this.toggleLoginRegister}>
                                    <Text
                                        style={[typography.Text14OpenSansRegular]}>{localize.t('login_register')}</Text>
                                </TouchableOpacity>}

                                <TouchableOpacity style={[AppStyle.itemButtonAutoWidth]} onPress={this.toggleCart}>
                                    <Text style={[typography.Text14OpenSansRegular]}>{localize.t('cart')}</Text>
                                </TouchableOpacity>
                            </View>

                            <FooterSocialMedia/>
                        </View>
                    }


                </ScrollView>
            </View>

        );
    }
}

const styles = {
    paddingClose: {
        paddingTop: Platform.OS === 'android' ? hp('4%') : hp('6%'),
        paddingStart: hp('3%'),
        paddingEnd: hp('2%'),
        paddingBottom: hp('2%'),
    },
    paddingLogo: {

        paddingBottom: hp('4%'),
    },
    itemDrawer: {


        marginHorizontal: wp('16%'),
        borderBottomWidth: 1,
        borderBottomColor: colors.CustomBlack,

    }, itemDrawerTouch: {
        width: wp('70%'),
        textAlign: 'center',
        alignContent: 'center',
        alignItems: 'center',
        paddingTop: hp('1.1%'),
        paddingBottom: hp('1%'),
        paddingHorizontal: hp('4%'),
    },
    iconFooterPadding: {

        paddingHorizontal: hp('1%'),
        paddingVertical: hp('1%'),
    },
};

export default SideMenu;

