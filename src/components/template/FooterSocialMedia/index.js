import React from 'react';
import {Text, Platform, View, ScrollView, TouchableOpacity, InteractionManager} from 'react-native';
import {Facebook, Phone, Instagram, colors, typography, Logo, wp, hp, Close, AppStyle, localize} from './../../../all';
import {
    NAVIGATION_CART_SCREEN,
    NAVIGATION_CATEGORY_SCREEN, NAVIGATION_COMPLAINS_SCREEN,
    NAVIGATION_HOME2_SCREEN,
    NAVIGATION_HOME_SCREEN,
    NAVIGATION_OFFER_HOME,
    NAVIGATION_OFFER_SCREEN, NAVIGATION_PROFILE_SCREEN, NAVIGATION_SEARCH_SCREEN,
} from '../../../navigation/types';
import Loader from '../../elements/Loader';
import SecondHeader from '../SecondHeader';
import {Linking} from 'react-native'

class FooterSocialMedia extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Welcome', header: null};

    logged = false;

    constructor(props) {
        super(props);


    }


    render() {

        return (

            <View>
                <ScrollView>


                    <View style={[AppStyle.CenterContentRow]}>

                        <TouchableOpacity
                            onPress={() => {

                                //  https://www.instagram.com/etloob/
                                Linking.openURL('https://www.instagram.com/bash_baghdad');

                            }}
                            style={[styles.iconFooterPadding]}>
                            <Instagram/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {

                            //  https://www.facebook.com/Etloob
                            Linking.openURL('https://m.facebook.com/BashForClothes');

                        }}
                                          style={[styles.iconFooterPadding]}>
                            <Facebook/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{
                            Linking.openURL(`tel:07728889997`)
                        }} style={[styles.iconFooterPadding]}>
                            <Phone/>
                        </TouchableOpacity>
                    </View>


                </ScrollView>
            </View>

        );
    }
}

const styles = {

    iconFooterPadding: {

        paddingHorizontal: hp('1%'),
        paddingVertical: hp('1%'),
    },
};

export default FooterSocialMedia;

