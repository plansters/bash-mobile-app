import React from 'react';
import {Text, Platform, View, ScrollView, TouchableOpacity, Image} from 'react-native';
import {Facebook, Phone, Instagram, colors, typography, Logo, wp, hp, Close, AppStyle, localize} from './../../../all';
import { connect} from 'react-redux';

import {
    NAVIGATION_CART_SCREEN,
    NAVIGATION_HOME2_SCREEN,
    NAVIGATION_HOME_SCREEN,
    NAVIGATION_OFFER_HOME,
    NAVIGATION_OFFER_SCREEN, NAVIGATION_SEARCH_SCREEN,
} from '../../../navigation/types';
import Touchable from '../../elements/Touchable';
import Search from '../../../assets/icons/search.svg';
import Cart from '../../../assets/icons/cart.svg';
import ToggleDrawer from '../../../assets/icons/toggle.svg';
import {openSelectedCategory} from '../../../store/category/actions';
import {CountCartUpdate, HomeCategories, HomeOffers, HomeSliders} from '../../../store/home/actions';
import {openSelectedProduct} from '../../../store/product/actions';

class Header extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Welcome', header: null};

    logged = false;

    constructor(props) {
        super(props);
        this.state = {
            didFinish: true,
        };
        setTimeout(function () {
            this.setState({
                didFinish: true,
            });

        }.bind(this), 100);

        this.toggleDrawerAction = this.toggleDrawerAction.bind(this);
        this.toggleSearchScreen = this.toggleSearchScreen.bind(this);
        this.DrawerGoBack = this.DrawerGoBack.bind(this);
        this.toggleCart = this.toggleCart.bind(this);
        this.getCartSum = this.getCartSum.bind(this);
    }


    toggleDrawerAction = () => {
        setTimeout(function () {

            this.props.navigation.toggleDrawer();
        }.bind(this), AppStyle.timeOut);
    };

    toggleSearchScreen = () => {
        setTimeout(function () {

            this.props.navigation.navigate(NAVIGATION_SEARCH_SCREEN);

        }.bind(this), AppStyle.timeOut);

    };
    DrawerGoBack = () => {
        setTimeout(function () {

            this.props.navigation.goBack();

        }.bind(this), AppStyle.timeOut);

    };


    toggleCart = () => {

        setTimeout(function () {

            this.props.navigation.navigate(NAVIGATION_CART_SCREEN);

        }.bind(this), AppStyle.timeOut);

    };

    getCartSum() {


        return this.props.cart !== null && this.props.cart !== undefined ? this.props.cart.line_items : [];

    }

    render() {

        return (

            <View style={{backgroundColor: 'white'}}>
                {!this.state.didFinish ?
                    null
                    : <View style={[styles.HeaderStyle, {flexDirection: 'row', justifyContent: 'space-between'}]}>

                        <View style={[AppStyle.CenterContentRow, {}]}>

                            <View style={[, {alignContent: 'flex-start'}]}>
                                <TouchableOpacity style={{padding: hp('2%')}} onPress={this.toggleSearchScreen}>
                                    <Search width={hp('3.5%')} height={hp('3.5%')}/>
                                </TouchableOpacity>
                            </View>
                            <View style={[, {alignItems: 'flex-end'}]}>
                                <TouchableOpacity style={{padding: hp('2%')}} onPress={this.toggleCart}>
                                    <Cart width={hp('3.5%')} height={hp('3.5%')}/>
                                    <View style={[styles.badgeCart]}>
                                        <Text style={[AppStyle.FontWhite, {fontSize: 12}]}>{this.props.cartCount}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>

                        </View>
                        <View style={[AppStyle.CenterContentRow, {}]}>

                            <View style={[, {paddingEnd: 4}]}>
                                <Touchable style={{}}>
                                    {/*<Logo width={hp('7.5%')} height={hp('7.5%')}/>*/}
                                    <Image
                                        style={{
                                            width: hp('10%'),
                                            height: hp('5%'),
                                        }}
                                        source={require('./../../../../src/assets/images/Logo.png')}
                                    />
                                </Touchable>
                            </View>
                            <View style={[, {alignItems: 'flex-end'}]}>
                                <TouchableOpacity style={{padding: hp('2%')}} onPress={this.toggleDrawerAction}>
                                    <ToggleDrawer width={hp('3.5%')} height={hp('3.5%')}/>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>
                }

            </View>

        );
    }
}

const styles = {

    HeaderStyle: {


        paddingBottom: Platform.OS == 'android' ? hp('2%') : hp('1%'),
        paddingStart: hp('1%'),
        paddingEnd: hp('1%'),
        borderBottomWidth: 1,
        borderBottomColor: colors.GrayTransparent,
        borderColor: 'transparent',


        ...Platform.select({
            ios: {
                shadowColor: colors.GrayBorder,
                shadowOpacity: 1,
                shadowRadius: 3,
                shadowOffset: {
                    height: 3,
                    width: 1,
                },
                paddingTop: hp('5%'),
            },

            android: {
                elevation: 1,
                paddingTop: hp('2%'),
            },
        }),
    },

    badgeCart: {
        position: 'absolute',
        top: hp('1.2%'),
        left: hp('1%'),
        backgroundColor: 'red',
        width: hp('2%'),
        height: hp('2%'),
        borderRadius: 50,
        padding: 0,
        justifyContent: 'center',
        alignItems: 'center',


    },


};


const mapStateToProps = state => {
    return {
        cartCount: state.home.cartCount,

    };
};
const mapDispatchToProps = dispatch => {
    return {

        updateCart: (payload) => dispatch(CountCartUpdate(payload)),

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
