import React from 'react';
import {
    TouchableWithoutFeedback,
    StyleSheet,
    InteractionManager,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput,Platform
} from 'react-native';
import {
    ProductListTwoColumnVertical,
    FastImage,
    DefaultHeader,
    wp,
    hp,
    Swiper,
    AppStyle,
    colors,
    localize,connect
} from './../../../all';
import {NAVIGATION_PRODUCT_SCREEN} from '../../../navigation/types';
import ProductListTwoRowHorizontal from '../../elements/ProductListTwoRowHorizontal';
import {productLists, productListsRest} from '../../../store/ProductList/actions';
import {openSelectedProduct} from '../../../store/product/actions';
import {OfferList, OfferListRest} from '../../../store/OfferList/actions';
import Reactron from 'reactotron-react-native';

class OffersScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);
        this.state = {};

        this.state = {
            order_by: '',
            page: 1,
            didFinishInitialAnimation: false,

        };
        this.onPressProduct = this.onPressProduct.bind(this);
        this.handleLoadMore = this.handleLoadMore.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
        this.OrderBy = this.OrderBy.bind(this);
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {

            setTimeout(function () {
                this.setState({
                    didFinishInitialAnimation: true,
                });
                this.props.offerListsRest({});


                    this.props.getProducts({
                        page: this.state.page,
                        refresh: false,
                        order_by: this.state.order_by,
                    });

            }.bind(this), 1);

        });
    }

    onPressProduct(item) {

        //console.log('product=====');
        //console.log(item);
        this.props.selectedProduct(item);
        this.props.navigation.push(NAVIGATION_PRODUCT_SCREEN);
    }

    handleLoadMore() {

        ////console.log('load more ');
        var page = this.state.page;
        page = page + 1;

        ////console.log(page);
        ////console.log(this.props.last_page);
        if (this.props.last_page >= page && !this.props.isLoadingAction) {
            this.setState({page: page}, () => {

                    this.props.getProducts({
                        page: this.state.page,
                        refresh: false,
                        order_by: this.state.order_by,
                    });


            });
        }


    }

    handleRefresh() {

        ////console.log('handle refresh ');
        this.props.offerListsRest({});

        this.setState({page: 1}, () => {

                this.props.getProducts({
                    page: this.state.page,
                    refresh: true,
                    order_by: this.state.order_by,
                });

        });

    }

    OrderBy(value) {
        this.setState({order_by: value}, function () {

            this.props.offerListsRest();
            this.handleRefresh();

        }.bind(this));
    }
    render() {

        return (


            <DefaultHeader headerTitle={localize.t('offers')}
                           didFinishInitialAnimation={this.state.didFinishInitialAnimation} {...this.props}>

                <View style={{ paddingBottom:Platform.os=="android"?wp("15%"):wp("20%"),}}>
                    <ProductListTwoColumnVertical
                        onPressItem={this.onPressProduct}

                        {...this.props}
                        home={false}
                        data={this.props.offerList}

                        handleLoadMore={this.handleLoadMore}
                        handleRefresh={this.handleRefresh}

                    />
                </View>


            </DefaultHeader>

        );
    }
}



const mapStateToProps = state => {
    return {
        last_page: state.offerList.last_page,
        offerList: state.offerList.offerList,
        isLoadingAction: state.offerList.isLoadingAction,
        status: state.offerList.status,
        errorMessage: state.offerList.ErrorMessage,

    };
};
const mapDispatchToProps = dispatch => {
    return {

        getProducts: (payload) => dispatch(OfferList(payload)),
        selectedProduct: (payload) => dispatch(openSelectedProduct(payload)),
        offerListsRest: (payload) => dispatch(OfferListRest(payload)),

    };


};

export default connect(mapStateToProps, mapDispatchToProps)

(
    OffersScreen,
)
;
