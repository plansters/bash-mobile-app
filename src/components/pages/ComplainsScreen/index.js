import React from 'react';
import {
    TouchableWithoutFeedback,
    StyleSheet,
    InteractionManager,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput, Platform, KeyboardAvoidingView, Alert,
} from 'react-native';
import {
    ProductListTwoColumnVertical,
    FastImage,
    DefaultHeader,
    wp,
    hp,
    Swiper,
    AppStyle,
    colors,
    localize,
    Facebook,
    Phone, connect,

} from './../../../all';
import InputElement from '../../elements/TextInput';
import typography from '../../../constants/typography';
import CustomButton from '../../elements/CustomButton';
import Instagram from '../../../assets/icons/instgram.svg';
import FooterSocialMedia from '../../template/FooterSocialMedia';
import {CustomerCancelOrder, CustomerCancelOrderReset} from '../../../store/order/actions';
import {CustomerAddComplaint, CustomerComplaintReset} from '../../../store/complaint/actions';
import {FAILURE, SUCCESS} from '../../../constants/actionsType';
import {NAVIGATION_HOME_SCREEN} from '../../../navigation/types';
import AwesomeAlert from 'react-native-awesome-alerts';

class ComplainsScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);
        this.state = {
            title: '',
            content: '',
            didFinishInitialAnimation: false,
            showAlert: false,
            type: 'default',
            first: false,

        };
        this.addComplaint = this.addComplaint.bind(this);
        this.renderErrorMessage = this.renderErrorMessage.bind(this);
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {

            setTimeout(function () {
                this.setState({
                    didFinishInitialAnimation: true,
                });
                this.props.ComplaintReset();

            }.bind(this), 1);

        });
    }

    addComplaint() {

        if (global.logged) {

            this.props.AddComplaint({
                user_id: global.user.id,
                title: this.state.title,
                content: this.state.content,
            });

        } else {
            this.setState({showAlert:true})
            this.setState({type:'error'})
            // Alert.alert(localize.t('please_login_first'));

        }

    }

    renderErrorMessage() {
        if (this.props.statusComplaint === FAILURE) {
            return <View style={[AppStyle.CenterContent, {flexDirection: 'column'}]}>

                {this.props.ErrorMessageComplaint && this.props.ErrorMessageComplaint.map((e) => {
                    return <TouchableWithoutFeedback key={e}><Text
                        style={[typography.Text12Regular, {color: colors.red}]}>{e}</Text></TouchableWithoutFeedback>;

                })}

            </View>;
        }
        else if (this.props.statusComplaint === SUCCESS) {

            if(!this.state.first)
            {
                this.setState({first:true})
                this.setState({showAlert:true})
                this.setState({type:'default'})
            }
            this.props.ComplaintReset();



            return null;


        }
    }

    render() {

        return (


            <DefaultHeader headerTitle={localize.t('report')}
                           didFinishInitialAnimation={this.state.didFinishInitialAnimation} {...this.props}>

                <ScrollView>
                    <KeyboardAvoidingView
                        behavior={Platform.OS === 'ios' ? 'padding' : 'padding'}
                        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
                        style={[styles.ContainerMargin]}>

                        <View>
                            <TextInput
                                onChangeText={(value) => {
                                    this.setState({title: value});
                                }}
                                placeholder={'موضوع الشكوى'}
                                keyboardType={'default'}
                                style={[typography.Text14OpenSansRegular, AppStyle.inputBordered]}/>

                        </View>
                        <View style={[styles.marginButtonTopInput]}>
                            <TextInput
                                onChangeText={(value) => {
                                    this.setState({content: value});
                                }}
                                placeholder={'رسالة الشكوى'}
                                keyboardType={'default'} multiline={true}
                                numberOfLines={4}
                                style={[typography.Text14OpenSansRegular, AppStyle.inputBordered, styles.TextArea]}/>

                        </View>
                        <View style={[AppStyle.CenterContent, styles.marginButtonTop]}>
                            <CustomButton onPress={this.addComplaint} gradientColor={AppStyle.GradientColor}
                                          text={localize.t('send')}/>
                        </View>

                        {this.renderErrorMessage()}


                    </KeyboardAvoidingView>

                    <View style={{marginTop: wp('6%'), justifyContent: 'center', alignItems: 'center', flex: 1}}>

                        <Text style={[typography.Text16OpenSansBold]}> يمكنك التواصل معنا عبر </Text>
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center', marginTop: wp('3%'),
                        }}>

                            <FooterSocialMedia/>

                        </View>
                    </View>
                </ScrollView>

                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={false}
                    title={this.state.type === 'error' ? '' : 'شكراً لك'}
                    message={this.state.type === 'error' ? localize.t('please_login_first') : localize.t('complaint_added_success')}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={true}
                    showCancelButton={true}
                    showConfirmButton={false}
                    cancelText={localize.t('ok')}
                    confirmText=""
                    confirmButtonColor={colors.GrayTransparent}
                    onCancelPressed={() => {
                        this.setState({showAlert: false});
                        this.setState({first: false});

                        setTimeout(function () {

                            this.setState({type: 'default'});

                        }.bind(this), 500);

                        setTimeout(function () {

                            this.props.navigation.navigate(NAVIGATION_HOME_SCREEN);

                        }.bind(this), 200);

                    }}
                    onConfirmPressed={() => {
                    }}
                />

            </DefaultHeader>

        );
    }
}


const styles = {
    ContainerMargin: {
        marginStart: hp('3%'),
        marginEnd: hp('3%'),
        marginTop: hp('2%'),
    },
    marginButtonTopInput: {
        marginTop: hp('3%'),

    },
    TextArea: {
        height: hp('30%'),
        justifyContent: 'flex-start',
    }, marginButtonTop: {
        marginTop: hp('2%'),
        marginBottom: hp('2%'),

    },
    iconFooterPadding: {

        paddingHorizontal: hp('1%'),
        paddingVertical: hp('1%'),
    },
};
const mapStateToProps = state => {
    return {
        isLoadingAction: state.complaint.isLoadingAction,
        ErrorMessageComplaint: state.complaint.ErrorMessageComplaint,
        statusComplaint: state.complaint.statusComplaint,
    };
};
const mapDispatchToProps = dispatch => {
    return {

        AddComplaint: (payload) => dispatch(CustomerAddComplaint(payload)),
        ComplaintReset: (payload) => dispatch(CustomerComplaintReset(payload)),

    };
};

export default connect(mapStateToProps, mapDispatchToProps)

(
    ComplainsScreen,
)
;
