import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
    TextInput,
    Linking,
} from 'react-native';
import {
    CustomButton,
    LinearGradient,
    localize,
    InputElement,
    AuthHeader,
    AppStyle,
    TextUnderLine,
    typography, connect, Loader, colors,axios
} from './../../../all';
import CodePush from 'react-native-code-push';
import {NAVIGATION_DRAWER, NAVIGATION_SIGNUP_SCREEN} from '../../../navigation/types';
import {CustomerLogin, resetAuthState} from '../../../store/login/actions';
import {FAILURE, SUCCESS} from '../../../constants/actionsType';
import {url} from '../../../api/api';

class LoginScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);
        this.state = {
            password: '',
            email: '',
            showPassword: true,
        };
        this.props.reset();

        this.navigateToHomeGuest=this.navigateToHomeGuest.bind(this)
        this.navigateToSignUp=this.navigateToSignUp.bind(this)
    }

    navigateToHome = () => {

        setTimeout(function () {


            this.props.Login({
                phone: this.state.phone,
                password: this.state.password,
            });
        }.bind(this), AppStyle.timeOut);

    };
    navigateToHomeGuest = () => {

        setTimeout(function () {


            this.props.navigation.navigate(NAVIGATION_DRAWER);

        }.bind(this), AppStyle.timeOut);

    };
    navigateToSignUp = () => {

        setTimeout(function () {

            this.props.navigation.navigate(NAVIGATION_SIGNUP_SCREEN);

        }.bind(this), AppStyle.timeOut);

    };

    Loading() {
        if (this.props.isLoadingAction) {
            return <Loader didFinishInitialAnimation={false}/>;
        }
        else {


        }
    }

    renderErrorMessage() {
        if (this.props.status === FAILURE) {
            return <View style={[AppStyle.CenterContent, {flexDirection: 'column'}]}>

                {this.props.ErrorMessage && this.props.ErrorMessage.map((e) => {
                    return <TouchableWithoutFeedback key={e}><Text
                        style={[typography.Text12Regular, {color: colors.red}]}>{e}</Text></TouchableWithoutFeedback>;

                })}

            </View>;
        }
        else if (this.props.status === SUCCESS) {

            this.props.reset();

            global.storage.save({
                key: 'user',
                data: {user: this.props.user, logged: true, is_active: true},
                expires: null,
            });
            global.user = this.props.user;
            global.logged = true;
            global.role = 'customer';
            global.is_active = true;

            ////console.log('global.user');
            ////console.log(global.user);

            axios.post(url+'users/auth/token',
                {
                    user_id: global.user.id,
                    os: Platform.OS,
                    token: global.token + "",
                }
                , null)
                .then(function (response) {
                    console.log("firebase save  req");
                    // console.log(response);
                    // console.log("instide req");
                    return {status: 200, response: response.data, message: ""}
                })
                .catch(function (error) {
                    console.log("firebase save  err");
                    // console.log(error.response.data);
                    return {status: 400, response: null, message: error.response.data.message}
                });

            this.props.navigation.navigate(NAVIGATION_DRAWER);

            return null;
        }
    }

    render() {

        return (


            <AuthHeader isLoadingAction={this.props.isLoadingAction} {...this.props} titleHeader={localize.t('login')}>

                <View style={[Styles.ContainerMargin]}>

                    <View>
                        <InputElement

                            onChangeText={(value) => {
                                this.setState({phone: value});
                            }}
                            text={localize.t('phone')}
                        />
                    </View>

                    <View style={[Styles.marginSecondInput]}>
                        <InputElement
                            text={localize.t('password')}
                            textLength={this.state.password.length}
                            onPressShow={() => {
                                this.setState({showPassword: !this.state.showPassword});
                            }}
                            onChangeText={(value) => {
                                this.setState({password: value});
                            }}
                            type={'password'}
                            secureTextEntry={this.state.showPassword}/>
                    </View>
                    <View style={[AppStyle.CenterContent, {flexDirection: 'row'}]}>
                        <TouchableOpacity><Text
                            style={[typography.Text12Regular, AppStyle.opacity05, Styles.marginSecondText]}>{localize.t('forgot_password')}</Text></TouchableOpacity>
                        <TextUnderLine onPress={()=>{
                            Linking.openURL(`tel:07728889997`)
                        }} text={localize.t('contact')}/>
                    </View>
                    {this.renderErrorMessage()}
                    <View style={[AppStyle.CenterContent, Styles.marginButtonTop]}>
                        <CustomButton onPress={this.navigateToHome} gradientColor={AppStyle.GradientColor}
                                      text={localize.t('login')}/>
                    </View>

                    {/*<View style={[AppStyle.CenterContent, {flexDirection: 'row', marginTop: hp('2%')}]}>*/}
                        {/*<TouchableOpacity onPress={this.navigateToSignUp}><Text*/}
                            {/*style={[typography.Text12Regular, AppStyle.opacity05]}>{localize.t('dont_have_account')}</Text></TouchableOpacity>*/}
                        {/*<TextUnderLine text={localize.t('sign_up_new')}/>*/}
                    {/*</View>*/}
                    <View style={[AppStyle.CenterContent, {flexDirection: 'row', marginTop: hp('2%')}]}>
                        <TouchableOpacity onPress={this.navigateToHomeGuest}><Text
                            style={[typography.Text14OpenSansBold, {padding: wp('3%')}]}>{localize.t('as_gust')}</Text></TouchableOpacity>
                    </View>

                </View>


            </AuthHeader>


        );
    }
}

const Styles = {

    ContainerMargin: {
        marginTop: hp('5%'),
        marginStart: hp('4%'),
        marginEnd: hp('4%'),
    },
    marginSecondInput: {
        marginTop: hp('4%'),

    },
    marginButtonTop: {
        marginTop: hp('1%'),

    },
};


const mapStateToProps = state => {
    return {
        isLoadingAction: state.login.isLoadingAction,
        status: state.login.status,
        ErrorMessage: state.login.ErrorMessage,
        user: state.login.user,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        Login: (payload) => dispatch(CustomerLogin(payload)),
        reset: (payload) => dispatch(resetAuthState(payload)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
