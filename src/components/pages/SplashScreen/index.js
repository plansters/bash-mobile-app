import React from 'react';
import {Text, View, Platform, I18nManager, Image} from 'react-native';
import {
    NAVIGATION_APP,
    NAVIGATION_ChooseLanguage_SCREEN,
    NAVIGATION_DRAWER,
} from './../../../navigation/types';
import {FastImage, Storage, AsyncStorage, wp, hp, Logo, axios} from './../../../all';
import {url} from '../../../api/api';
import CodePush from 'react-native-code-push';
import * as Animatable from 'react-native-animatable';
import firebase, {Notification, NotificationOpen} from 'react-native-firebase';

class SplashScreen extends React.Component {
    player = null;

    constructor(props) {
        super(props);

        const storage = new Storage({
            size: 1000,


            storageBackend: AsyncStorage,

            defaultExpires: 1000 * 3600 * 102400,

            enableCache: true,

        });
        global.storage = storage;


    }

    performTimeConsumingTask = async () => {
        return new Promise((resolve) =>
            setTimeout(
                () => {
                    resolve('result');
                },
                1200,
            ),
        );
    };


    async fetchToken() {

        console.warn('fetchToken');
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        if (!fcmToken) {
            console.warn(fcmToken);
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                console.warn("inside fcm geteeing")
                console.warn(fcmToken)


                global.token = fcmToken
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
        } else {
            // console.warn("inside restore fcm ")
            global.token = fcmToken
            // console.log(global.token)

            console.warn(fcmToken);

        }
    }

    async askPermission() {
        try {
            await firebase.messaging().requestPermission();
            console.warn('Permissions allowed');
            this.fetchToken();
        } catch (error) {
            console.warn('Permissions denied');
        }
    }

    async componentDidMount() {

        const data = await this.performTimeConsumingTask();

        const granted = await firebase.messaging().hasPermission();
        if (granted) {
            console.warn('fetchToken');

            this.fetchToken();
        } else {

            this.askPermission();
        }


        global.current = 'home';

        axios.get(url + 'cities',
            {}
            , null)
            .then(function (response) {
                var cities = [];
                response.data.map((e) => {
                    cities.push({
                        label: e.name, value: e.id,
                    });
                });
                global.cities = cities;
                ////console.log('global.cities');
                ////console.log(global.cities);
            })
            .catch(function (error) {
                global.cities = [];

            });

        axios.get(url + 'categories?per_page=100',
            {}
            , null)
            .then(function (response_c) {
                var categories = [];
                response_c.data.data.map((e) => {
                    categories.push({
                        label: e.name, value: e.id,
                    });
                });
                global.categories = categories;
                ////console.log('global.categories');
                ////console.log(global.categories);
            })
            .catch(function (error) {
                global.categories = [];

            });
        // this.props.navigation.navigate(NAVIGATION_ChooseLanguage_SCREEN);

        if (data !== null) {


            AsyncStorage.getItem('lang')
                .then(language => {
                    I18nManager.allowRTL(true);
                    ////console.log('language');
                    ////console.log(language);

                    if (language === 'ar' || language === 'en') {

                        if (language === 'ar') {

                            I18nManager.forceRTL(true);
                        } else if (language === 'en') {
                            I18nManager.forceRTL(false);

                        }
                        storage.load({
                            key: 'user',
                            autoSync: true,
                            syncInBackground: true,

                        })
                            .then(ret => {
                                if (ret.logged && ret.is_active) {
                                    global.logged = true;
                                    global.is_active = true;
                                    global.user = ret.user;
                                    console.log(user);


                                    axios.post(url+'users/auth/token',
                                        {
                                            user_id: ret.user.id,
                                            os: Platform.OS,
                                            token: global.token + "",
                                        }
                                        , null)
                                        .then(function (response) {
                                            console.log("firebase save  req");
                                            // console.log(response);
                                            // console.log("instide req");
                                            return {status: 200, response: response.data, message: ""}
                                        })
                                        .catch(function (error) {
                                            console.log("firebase save  err");
                                            // console.log(error.response.data);
                                            return {status: 400, response: null, message: error.response.data.message}
                                        });

                                   this.props.navigation.navigate(NAVIGATION_DRAWER);
                                }
                                else {


                                   this.props.navigation.navigate(NAVIGATION_APP);
                                }

                            })
                            .catch(err => {

                                switch (err.name) {
                                    case 'NotFoundError':

                                      this.props.navigation.navigate(NAVIGATION_APP);
                                        break;
                                    case 'ExpiredError':

                                        break;
                                }
                            });


                    } else {

                        AsyncStorage.setItem('lang', 'ar')
                            .then(() => {
                                I18nManager.forceRTL(true);
                                CodePush.restartApp();

                            });

                        // //console.log('language');
                        // this.props.navigation.navigate(NAVIGATION_ChooseLanguage_SCREEN);

                    }
                });


            storage.load({
                key: 'cart',
                autoSync: true,
                syncInBackground: true,

            })
                .then(ret => {

                    global.cart = ret;

                })
                .catch(err => {

                    switch (err.name) {
                        case 'NotFoundError':
                            break;
                        case 'ExpiredError':

                            break;
                    }
                });


            if (Platform.OS === 'ios') {
                // axios.get('http://etloob.com/check.php',
                //     null)
                //     .then(function (response) {
                //         ////console.log('instide req');
                //         ////console.log(response);
                //
                //         // pro or dev
                //         global.mode = response.data.data + '';
                //
                //
                //         ////console.log(global.mode);
                //     })
                //     .catch(function (error) {
                //         global.mode = 'pro';
                //     });

                global.mode = 'pro';
            }
            else {

                global.mode = 'dev';

            }


        }


    }


    render() {
        return (
            <View style={{flex:1,}}>
                <FastImage

                    style={{
                        width:wp("100%"),
                        height:hp("100%"),
                    }}
                    width={wp("100%")}
                    height={hp("100%")}
                    // resizeMode={FastImage.resizeMode.contain}
                    source={require('./../../../assets/images/splash.jpg')}
                />
                <Animatable.Image
                    animation="fadeInDown"
                    iterationCount={1}
                    style={{
                        position:'absolute',
                        bottom:hp("10%"),
                        right:hp("2%"),
                        width:hp("24%"),
                        height:hp("12%"),
                    }}
                    source={require('./../../../../src/assets/images/Logo.png')}
                />

            </View>
        );
    }
}


export default SplashScreen;
