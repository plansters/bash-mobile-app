import React from 'react';
import {
    TouchableWithoutFeedback,
    StyleSheet,
    InteractionManager,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput, Platform,
} from 'react-native';
import {
    ProductListTwoColumnVertical,
    FastImage,
    DefaultHeader,
    wp,
    hp,
    Swiper,
    AppStyle,
    colors,
    localize, connect,
} from './../../../all';
import {NAVIGATION_PRODUCT_SCREEN} from '../../../navigation/types';
import ProductListTwoRowHorizontal from '../../elements/ProductListTwoRowHorizontal';
import {productLists, productListsRest} from '../../../store/ProductList/actions';
import {openSelectedProduct} from '../../../store/product/actions';
import {OfferList, OfferListRest} from '../../../store/OfferList/actions';
// import Reactron from 'reactotron-react-native';
import {SearchList, SearchListRest} from '../../../store/SearchList/actions';

class SearchProductsScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);
        this.state = {};

        this.state = {

            order_by: '',
            page: 1,
            didFinishInitialAnimation: false,
            term: '',
            category: '',
            min_price: '',
            max_price: '',

        };
        this.onPressProduct = this.onPressProduct.bind(this);
        this.handleLoadMore = this.handleLoadMore.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
        this.OrderBy = this.OrderBy.bind(this);
        //console.log(this.props.navigation.state.params);
        //console.log(this.props.navigation.state.params.term);

    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {

            setTimeout(function () {
                this.setState({
                    didFinishInitialAnimation: true,
                });
                this.props.searchListsRest({});

                this.setState({
                    term: this.props.navigation.state.params.term,
                    category: this.props.navigation.state.params.category,
                    min_price: this.props.navigation.state.params.min_price,
                    max_price: this.props.navigation.state.params.max_price,

                });
                this.props.getProducts({
                    page: this.state.page,
                    term: this.state.term,
                    category: this.state.category,
                    min_price: this.state.min_price,
                    max_price: this.state.max_price,
                    refresh: false,
                    order_by: this.state.order_by,
                });

            }.bind(this), 1);

        });
    }

    onPressProduct(item) {

        //console.log('product=====');
        //console.log(item);
        this.props.selectedProduct(item);
        this.props.navigation.push(NAVIGATION_PRODUCT_SCREEN);
    }

    handleLoadMore() {

        ////console.log('load more ');
        var page = this.state.page;
        page = page + 1;

        ////console.log(page);
        ////console.log(this.props.last_page);
        if (this.props.last_page >= page && !this.props.isLoadingAction) {
            this.setState({page: page}, () => {

                this.props.getProducts({

                    term: this.state.term,
                    category: this.state.category,
                    min_price: this.state.min_price,
                    max_price: this.state.max_price,
                    page: this.state.page,
                    refresh: false,
                    order_by: this.state.order_by,
                });


            });
        }


    }

    handleRefresh() {

        ////console.log('handle refresh ');
        this.props.searchListsRest({});

        this.setState({page: 1}, () => {

            this.props.getProducts({

                term: this.state.term,
                category: this.state.category,
                min_price: this.state.min_price,
                max_price: this.state.max_price,
                page: this.state.page,
                refresh: true,
                order_by: this.state.order_by,
            });

        });

    }

    OrderBy(value) {
        this.setState({order_by: value}, function () {

            this.props.searchListsRest();
            this.handleRefresh();

        }.bind(this));
    }

    render() {

        return (


            <DefaultHeader headerTitle={localize.t('search_result')}
                           didFinishInitialAnimation={this.state.didFinishInitialAnimation} {...this.props}>

                <View style={{paddingBottom: Platform.os == 'android' ? wp('15%') : wp('20%')}}>
                    <ProductListTwoColumnVertical
                        onPressItem={this.onPressProduct}

                        {...this.props}
                        home={false}
                        data={this.props.searchList}

                        handleLoadMore={this.handleLoadMore}
                        handleRefresh={this.handleRefresh}

                    />
                </View>


            </DefaultHeader>

        );
    }
}


const mapStateToProps = state => {
    return {
        last_page: state.searchList.last_page,
        searchList: state.searchList.searchList,
        isLoadingAction: state.searchList.isLoadingAction,
        status: state.searchList.status,
        errorMessage: state.searchList.ErrorMessage,

    };
};
const mapDispatchToProps = dispatch => {
    return {

        getProducts: (payload) => dispatch(SearchList(payload)),
        selectedProduct: (payload) => dispatch(openSelectedProduct(payload)),
        searchListsRest: (payload) => dispatch(SearchListRest(payload)),

    };


};

export default connect(mapStateToProps, mapDispatchToProps)

(
    SearchProductsScreen,
)
;
