import React from 'react';
import {
    TouchableWithoutFeedback,
    StyleSheet,
    InteractionManager,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View, Alert,
    TextInput, Platform,
} from 'react-native';
import {
    CartProductList,
    Touchable,
    FastImage,
    DefaultHeader,
    wp,
    hp,
    Swiper,
    AppStyle,
    colors,
    localize,
    typography, connect, Reactron,
} from './../../../all';
import NumberFormat from 'react-number-format';
import CollapsibleList from '../../elements/CollapsibleList';
import ProductListTwoRowHorizontal from '../../elements/ProductListTwoRowHorizontal';
import {NAVIGATION_CHECKOUT_SCREEN, NAVIGATION_HOME_SCREEN, NAVIGATION_PRODUCT_SCREEN} from '../../../navigation/types';
import {
    getCustomerCart,
    minusProductFromCart,
    plusProductFromCart,
    removeProductFromCart, resetCustomerCart,
} from '../../../store/cart/actions';
import {CountCartUpdate} from '../../../store/home/actions';
import AwesomeAlert from 'react-native-awesome-alerts';

class CartScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);

        this.state = {

            didFinishInitialAnimation: false,
            showAlert: false,
            type: 'default',
        };
        this.onPressProduct = this.onPressProduct.bind(this);
        this.removeItem = this.removeItem.bind(this);
        this.minusItem = this.minusItem.bind(this);
        this.plusItem = this.plusItem.bind(this);
        this.checkout = this.checkout.bind(this);
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {

            setTimeout(function () {
                this.setState({
                    didFinishInitialAnimation: true,
                });

                this.props.customerCart({});

            }.bind(this), 1);

        });
    }

    onPressProduct(item) {

        this.props.navigation.push(NAVIGATION_PRODUCT_SCREEN);
    }

    removeItem(item) {

        ////console.log('removeItem');
        this.props.removeProductFromCart(item);
        setTimeout(function () {

            this.props.resetCustomerCart();
            this.props.customerCart();
            this.props.updateCart();

        }.bind(this), 10);
    }

    plusItem(item) {

        ////console.log('plusItem');
        this.props.plusProductFromCart(item);

        setTimeout(function () {
            this.props.resetCustomerCart();

            this.props.customerCart();
            this.props.updateCart();

        }.bind(this), 10);
    }

    minusItem(item) {

        ////console.log('minusItem');
        this.props.minusProductFromCart(item);

        setTimeout(function () {
            this.props.resetCustomerCart();

            this.props.customerCart();
            this.props.updateCart();
        }.bind(this), 10);
    }

    renderTotal = () => {
        let sum = 0;
        //console.log(this.props.cart)
        if (this.props.cart !== null && this.props.cart !== undefined && this.props.cart.line_items != null && this.props.cart.line_items.length > 0) {

            this.props.cart.line_items.forEach((item) => {
                if (item.product.unit_discount_price && parseInt(item.product.unit_discount_price) > 0) {
                    sum += parseFloat(item.product.unit_discount_price.replace(',', '')) * item.quantity;

                    // console.log(item.product.unit_discount_price)

                } else {

                    sum += parseFloat(item.product.unit_price.replace(',', '')) * item.quantity;
                    // sum += item.product.unit_price * item.quantity;
                    console.log(parseFloat(item.product.unit_price.replace(',', '')));

                }
            });
        }
        // console.log(sum);
        return sum;
    };

    checkout() {

        if (global.logged) {
            if (global.cart && global.cart.line_items && global.cart.line_items.length > 0) {
                this.props.navigation.push(NAVIGATION_CHECKOUT_SCREEN);

            }
            else {
                this.setState({showAlert: true});
                this.setState({type: 'error'});

            }
        }
        else {
            this.setState({showAlert: true});
            this.setState({type: 'default'});

            // Alert.alert(localize.t('please_login_first'));
        }
    }

    render() {

        return (
            <DefaultHeader headerTitle={localize.t('cart')}
                           didFinishInitialAnimation={this.state.didFinishInitialAnimation} {...this.props}>
                <View style={{flex: 1}}>
                    <View style={[, {flex: 8}]}>

                        <CartProductList

                            data={this.props.cart !== null && this.props.cart !== undefined ? this.props.cart.line_items : []}
                            removeItem={this.removeItem}
                            plusItem={this.plusItem}
                            minusItem={this.minusItem}
                        />
                    </View>
                    <View style={[AppStyle.CenterContent, {
                        flex: 2,
                        justifyContent: 'space-between',
                    }]}>

                        <View style={[AppStyle.CenterContent, {flex: 1, flexDirection: 'row'}]}>
                            <Text style={[style.total]}>{localize.t('total')}</Text>
                            <NumberFormat value={this.renderTotal()} renderText={value => <Text
                                style={[style.price]}>{value} {this.props.cart !== null && this.props.cart !== undefined && this.props.cart.line_items != null && this.props.cart.line_items.length > 0 ? this.props.cart.line_items[0].product.currency : ''}</Text>}
                                          displayType={'text'}
                                          thousandSeparator={true} prefix={''}/>
                        </View>
                        <Touchable onPress={this.checkout} style={{backgroundColor: 'red'}}>
                            <Text style={[style.CreateOrder]}>{localize.t('place_order')}</Text>
                        </Touchable>
                    </View>
                </View>

                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={false}
                    title={''}
                    message={this.state.type === 'error' ? localize.t('please_choose_products_first') : localize.t('please_login_first')}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={true}
                    showCancelButton={true}
                    showConfirmButton={false}
                    cancelText={localize.t('ok')}
                    confirmText=""
                    confirmButtonColor={colors.GrayTransparent}
                    onCancelPressed={() => {
                        this.setState({showAlert: false});
                        setTimeout(function () {
                            this.setState({type: 'default'});

                        }.bind(this), 500);
                    }}
                    onConfirmPressed={() => {
                    }}
                />


            </DefaultHeader>


        );
    }
}


const style = StyleSheet.create({

    CreateOrder: {

        ...typography.Text20OpenSansBold,
        ...AppStyle.FontWhite,
        textAlign: 'center',
        width: wp('100%'),
        backgroundColor: colors.yellow,
        ...Platform.select({
            ios: {

                paddingBottom: hp('2%'),
                paddingTop: hp('1%'),

            },

            android: {
                paddingBottom: hp('2%'),
                paddingTop: hp('2%'),

            },
        }),
    },
    total: {
        ...typography.Text16OpenSansBold,
        ...AppStyle.FontYellow,
        textAlign: 'center',
    },
    price: {
        ...typography.Text16OpenSansBold,
        color: colors.green,
        textAlign: 'center',
        paddingStart: wp('1%'),
    },

});

const mapStateToProps = state => {
    return {
        isLoading: state.cart.isLoading,
        status: state.cart.status,
        errorMessage: state.cart.errorMessage,
        cart: state.cart.cart,
    };
};
const mapDispatchToProps = dispatch => {
    return {

        customerCart: (payload) => dispatch(getCustomerCart(payload)),
        resetCustomerCart: (payload) => dispatch(resetCustomerCart(payload)),
        removeProductFromCart: (payload) => dispatch(removeProductFromCart(payload)),
        plusProductFromCart: (payload) => dispatch(plusProductFromCart(payload)),
        minusProductFromCart: (payload) => dispatch(minusProductFromCart(payload)),
        updateCart: (payload) => dispatch(CountCartUpdate(payload)),

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartScreen);
