import React from 'react';
import {
    TouchableWithoutFeedback,
    StyleSheet,
    InteractionManager,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput,
} from 'react-native';
import {
    SearchHeader,
    FastImage,
    DefaultHeader,
    wp,
    hp,
    Swiper,
    AppStyle,
    colors,
    typography,
    localize, connect,
} from './../../../all';
import ArrowLeft from '../../../assets/icons/arrow_left.svg';
import RNPickerSelect from 'react-native-picker-select';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import CustomButton from '../../elements/CustomButton';
import {NAVIGATION_DRAWER, NAVIGATION_SEARCH_PRODUCTS_SCREEN} from '../../../navigation/types';
import InputElement from '../../elements/TextInput';
import {CustomerSignUp, resetAuthState} from '../../../store/auth/actions';

class SearchScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            term: '',
            category: '',
            min_price: '',
            max_price: '',
            didFinishInitialAnimation: false,
            nonCollidingMultiSliderValue: [0, 1000000],

        };
        this.searchProducts = this.searchProducts.bind(this);

    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {

            setTimeout(function () {
                this.setState({
                    didFinishInitialAnimation: true,
                });

            }.bind(this), 1);

        });
    }

    DrawerGoBack = () => {
        setTimeout(function () {

            this.props.navigation.goBack();

        }.bind(this), AppStyle.timeOut);

    };

    nonCollidingMultiSliderValuesChange = values => {
        this.setState({
            nonCollidingMultiSliderValue: values,
        });
        //console.log(this.state.nonCollidingMultiSliderValue);
    };

    searchProducts() {
        //console.log('gggg');
        this.props.navigation.navigate(NAVIGATION_SEARCH_PRODUCTS_SCREEN, {
            term: this.state.term,
            category: this.state.category,
            min_price: this.state.nonCollidingMultiSliderValue[0],
            max_price: this.state.nonCollidingMultiSliderValue[1],

        });
    }


    render() {

        return (


            <SearchHeader
                headerTitle={localize.t('search')}

                didFinishInitialAnimation={this.state.didFinishInitialAnimation} {...this.props}>

                <ScrollView style={{backgroundColor: colors.GrayTransparent}}>

                    <View style={[styles.viwCon]}>
                        <TextInput style={styles.input}
                                   underlineColorAndroid="transparent"
                                   placeholder="كلمة البحث..."
                                   placeholderTextColor={colors.GrayTransparent}
                                   autoCapitalize="none"
                                   onChangeText={(value) => {
                                       this.setState({term: value});
                                   }}
                        />

                    </View>
                    <View style={[styles.viwCon]}>
                        <Text
                            style={[typography.Text18OpenSansRegular, {marginBottom: wp('1%')}]}>{localize.t('category')}</Text>

                        <RNPickerSelect
                            style={AppStyle.stylesPicker}
                            placeholder={{
                                label: 'جميع التصنيفات',
                                value: null,
                                color: AppStyle.FontBlackDark,
                            }}
                            onValueChange={(value) => {this.setState({category:value})}}
                            items={global.categories}
                        />

                    </View>
                    <View style={[styles.viwCon]}>
                        <Text
                            style={[typography.Text18OpenSansRegular, {marginBottom: wp('1%')}]}>{localize.t('price')}</Text>

                        <View style={{flex: 1, alignItems: 'center'}}>
                            <MultiSlider
                                values={[
                                    this.state.nonCollidingMultiSliderValue[0],
                                    this.state.nonCollidingMultiSliderValue[1],
                                ]}
                                sliderLength={wp('80%')}
                                selectedStyle={{
                                    backgroundColor: colors.yellow,
                                }}
                                onValuesChange={this.nonCollidingMultiSliderValuesChange}
                                min={0}
                                max={1000000}
                                step={1000}
                                allowOverlap={false}
                                snapped
                                minMarkerOverlapDistance={40}
                                customMarker={() => <TouchableOpacity>
                                    <View style={[styles.marker]}/>
                                </TouchableOpacity>}

                            />
                        </View>

                        <View style={[styles.priceRange]}>
                            <Text
                                style={[typography.Text18OpenSansRegular]}>{this.state.nonCollidingMultiSliderValue[0]}</Text>
                            <Text style={[typography.Text18OpenSansRegular, {
                                paddingEnd: wp('6%'),
                                paddingStart: wp('6%'),
                            }]}>-</Text>
                            <Text
                                style={[typography.Text18OpenSansRegular]}>{this.state.nonCollidingMultiSliderValue[1]}</Text>
                        </View>

                    </View>

                    <View style={[styles.viwCon, {alignItems: 'center', justifyContent: 'center'}]}>
                        <CustomButton onPress={this.searchProducts} gradientColor={AppStyle.GradientColor}
                                      text={localize.t('search_K')}/>

                    </View>


                </ScrollView>

            </SearchHeader>


        );
    }
}

const styles = {


    viwCon: {
        backgroundColor: colors.white,
        paddingVertical: wp('4%'),
        paddingHorizontal: wp('3%'), borderRadius: 0,
        marginTop: wp('3%'),
    },
    marker: {

        // width:wp("8%"),
        // height:wp("8%"),
        padding: wp('4'),
        borderRadius: 50,
        borderWidth: 1,
        borderColor: colors.white,
        backgroundColor: colors.yellow,

    },
    ViewContainer: {

        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: wp('4%'),
        paddingTop: wp('3%'),

    }, ViewContainerSelect: {

        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: wp('4%'),
        paddingTop: wp('5%'),

    },
    input: {
        paddingHorizontal: wp('3%'),
        paddingVertical: wp('1%'),
        flex: 1,
        borderColor: colors.Gray,
        borderWidth: 1,
        borderRadius: 8,
        ...typography.Text16Light,

        textAlign: 'right',
        alignItems: 'flex-start',
    },
    Arrow: {
        paddingVertical: wp('2%'),
        paddingHorizontal: wp('3%'),
        // backgroundColor:'red',
    },
    priceRange: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        // marginStart:wp("22%"),
        // marginEnd:wp("5%"),

    },

};

const mapStateToProps = state => {
    return {

        homeCategories: state.home.homeCategories,

    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);

