import React from 'react';
import {
    TouchableWithoutFeedback,
    StyleSheet,
    InteractionManager,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput,
    Modal, Platform,
    Alert,
} from 'react-native';
import HTMLView from 'react-native-htmlview';
import {
    ImageViewer,
    localize,
    FastImage,
    DefaultHeader,
    wp,
    hp,
    Swiper,
    AppStyle,
    colors,
    typography, AwesomeAlert,
    ProductAttributes, connect, Reactron,
} from './../../../all';
import NumberFormat from 'react-number-format';
import Touchable from '../../elements/Touchable';
import {productLists, productListsRest} from '../../../store/ProductList/actions';
import {addToCart, getVariant, openSelectedProduct} from '../../../store/product/actions';
import {SUCCESS} from '../../../constants/actionsType';
import {getCustomerCart} from '../../../store/cart/actions';
import {CountCartUpdate} from '../../../store/home/actions';
import {NAVIGATION_HOME_SCREEN} from '../../../navigation/types';

class ProductDetailsScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    product = null;

    constructor(props) {
        super(props);
        this.state = {};


        this.state = {
            isLoading: false,
            ModalGallery: false,
            didFinishInitialAnimation: false,
            IndexPressed: null,
            images: [],
            attr: [],
            selectedTab: '',
            selectedVariable:
                [],
            name: '',
            description: '',
            unit_price: null,
            unit_discount_price: null,
            sku: null,
            product_id: null,
            showAlert: false,
            type: 'default',
        };

        this.updateSelectedTab = this.updateSelectedTab.bind(this);
        this.renderVariant = this.renderVariant.bind(this);
        this.selectVariant = this.selectVariant.bind(this);
        this.addToCart = this.addToCart.bind(this);
        this.product = this.props.current;
        ////console.log('this.product');
        ////console.log(this.product);

    }

    componentDidMount() {
        var images = [];
        InteractionManager.runAfterInteractions(() => {

            setTimeout(function () {
                this.setState({
                    didFinishInitialAnimation: true,
                });

                if (this.product.product_images) {

                    this.product.product_images.map((item, index) => {

                        images.push({url: item.media_url});
                    });
                }
                else {
                    images.push({url: 'http://denrakaev.com/wp-content/uploads/2015/03/no-image-800x511.png'});
                }
                this.setState({
                    images: images,
                    name: this.product.name,
                    unit_price: this.product.unit_price,
                    unit_discount_price: this.product.unit_discount_price,
                    description: this.product.description,
                    attr: this.product.product_attributes,
                    sku: this.product.sku,
                    product_id: this.product.id,
                    selectedTab: this.product.product_attributes.length > 0 ? 'Attr' + this.product.product_attributes[0].attribute_id : '',
                });

            }.bind(this), 1);

        });
    }


    updateSelectedTab(name) {

        this.setState({selectedTab: name});
    }

    selectVariant(name) {

        ////console.log(name);

        var attr = this.state.attr;
        var selectedVariable = this.state.selectedVariable;
        attr.map((e, i) => {

            e.values.map((v, vi) => {

                if (name === v) {

                    if (selectedVariable.findIndex(x => x.attribute_id === e.attribute_id) !== -1) {
                        var index = selectedVariable.findIndex(x => x.attribute_id === e.attribute_id);

                        selectedVariable[index].value = name;

                    } else {

                        selectedVariable.push({
                            attribute_id: e.attribute_id,
                            value: name,
                            attribute_name: e.attribute_name,
                        });
                    }


                }

            });

        });
        ////console.log(selectedVariable);

        this.setState({selectedVariable: selectedVariable});

        this.setState({attr: []}, () => {

            this.setState({attr: this.product.product_attributes});


        });

        // if (this.product.product_attributes.length === selectedVariable.length) {
        //     this.props.getVariant({
        //         product_id: this.state.product_id,
        //         attributes: selectedVariable,
        //     });
        // }


        // if (this.product.product_attributes.length === selectedVariable.length) {
            this.props.getVariant({
                product_id: this.state.product_id,
                attributes: selectedVariable,
            });
        // }

    }

    renderVariant() {
        if (this.props.isLoadingAction) {

            if (!this.state.isLoading) {

                //this.swiper.scrollBy(0,true)

                this.setState({isLoading: true});
            }

            // this.setState({isLoading: true})
        } else {


            if (this.state.isLoading) {

                //////console.log("======variant=====")
                //////console.log(this.props.variant)
                this.setState({isLoading: false});
                if (this.props.variant) {
                    ////console.log(this.props.variant);
                    var variant = this.props.variant;
                    var images = [];
                    if (variant.product_images) {

                        variant.product_images.map((item, index) => {

                            images.push({url: item.media_url});
                        });
                    }
                    else {
                        images.push({url: 'http://denrakaev.com/wp-content/uploads/2015/03/no-image-800x511.png'});
                    }

                    this.setState({

                        images: images,
                        name: variant.name,
                        unit_price: variant.unit_price,
                        unit_discount_price: variant.unit_discount_price,
                        description: variant.description,
                        sku: variant.sku,
                    });
                }


            }
        }

    }


    addToCart() {

        if (this.product.product_attributes.length === 0) {
            this.props.addProductToCart({
                item: this.product,
            });
            this.setState({showAlert: true});



        } else {
            if (this.product.product_attributes.length === this.state.selectedVariable.length) {

                if (this.props.variant != null) {
                    this.props.variant.product_attributes = this.state.selectedVariable;
                    this.props.addProductToCart({
                        item: this.props.variant,
                    });
                    this.setState({showAlert: true});

                    this.props.updateCart();
                }
            }
            else {
                this.setState({showAlert: true,type:'error'});

                this.props.updateCart();
                // Alert.alert(localize.t('please_choose_attr'));
            }
        }

    }

    render() {

        return (


            <DefaultHeader HeaderSecondType={'absolute'} headerTitle={''}
                           didFinishInitialAnimation={this.state.didFinishInitialAnimation} {...this.props}>

                <View style={{flex: 1}}>
                    <View style={{flex: 4, marginBottom: wp('10%')}}>
                        <ScrollView>
                            <View style={{flex: 1}}>
                                <View style={[styles.ContainerSliderImage, {flex: 1}]}>
                                    <Swiper
                                        // height={wp('80%')}
                                        dotColor={colors.CustomBlack}
                                        activeDotColor={colors.yellow}
                                        loop={true}
                                        showsButtons={false}
                                        autoplay={true}
                                        autoplayTimeout={5}
                                        autoplayDirection={true}
                                        paginationStyle={{
                                            bottom: wp('8'), left: null, right: 10,
                                        }}
                                        dot={<View
                                            style={styles.dot}/>}
                                        activeDot={<View
                                            style={[styles.dotActive]}/>}>
                                        {this.state.images.map((e, i, []) => {
                                            return (<View key={'slide' + i} style={[AppStyle.CenterContent]}>
                                                <TouchableWithoutFeedback
                                                    style={[]}
                                                    ref="touch"
                                                    onPress={(item) => {
                                                        this.setState({IndexPressed: i});

                                                        this.setState({ModalGallery: true});
                                                    }}
                                                >
                                                    <FastImage
                                                        style={[styles.ItemSliderImage]}
                                                        resizeMode={FastImage.resizeMode.contain}
                                                        source={{
                                                            uri: e.url,
                                                            priority: FastImage.priority.high,
                                                        }}
                                                    />
                                                </TouchableWithoutFeedback>
                                            </View>);
                                        })}


                                    </Swiper>

                                    <Modal visible={this.state.ModalGallery} transparent={true}>
                                        <ImageViewer
                                            index={this.state.IndexPressed}

                                            renderFooter={
                                                () =>
                                                    <TouchableOpacity style={styles.CloseContainer}
                                                                      onPress={() => {
                                                                          this.setState({ModalGallery: false});

                                                                      }}>
                                                        <Text
                                                            style={styles.Close}> {localize.t('close')} </Text>
                                                    </TouchableOpacity>}
                                            imageUrls={this.state.images}/>
                                    </Modal>
                                </View>

                                <Text style={[styles.productSku]}>{localize.t('sku')}: {this.state.sku}</Text>
                                <Text style={[styles.productName]}>{this.state.name}</Text>

                                <View style={[styles.PriceContent]}>
                                    {this.state.unit_discount_price != null && parseInt(this.state.unit_discount_price) > 0 ?
                                        <React.Fragment>
                                            <NumberFormat value={this.state.unit_discount_price} displayType={'text'}
                                                          renderText={value => <Text
                                                              style={[styles.Price]}>{value} {this.product.currency}</Text>}
                                                          thousandSeparator={true} prefix={''}/>

                                            <NumberFormat value={this.state.unit_price} renderText={value => <Text
                                                style={[styles.oldPrice]}>{value} {this.product.currency}</Text>}
                                                          displayType={'text'}
                                                          thousandSeparator={true} prefix={''}/>
                                        </React.Fragment> :
                                        <NumberFormat value={this.state.unit_price} displayType={'text'}
                                                      renderText={value => <Text
                                                          style={[styles.Price]}>{value} {this.product.currency}</Text>}
                                                      thousandSeparator={true} prefix={''}/>}

                                </View>

                                <View style={[styles.descriptionContent]}>

                                    <Text style={[styles.description]}>{localize.t('description')}</Text>

                                    <View style={{
                                        paddingTop: wp('0%'),
                                        paddingBottom: wp('2%'),
                                        alignItems: 'flex-start',
                                        textAlign: 'start',
                                        justifyContent: 'flex-start',

                                    }}>

                                        <HTMLView lineBreak={false + ''} value={this.state.description + ''}
                                                  stylesheet={stylesHTML}/>

                                    </View>


                                </View>

                                <ProductAttributes
                                    data={this.state.attr}
                                    selectedTab={this.state.selectedTab}
                                    selectedVariable={this.state.selectedVariable}
                                    selectVariant={this.selectVariant}
                                    updateSelectedTab={this.updateSelectedTab}
                                />


                            </View>
                        </ScrollView>
                    </View>
                    <View style={[styles.AddToCartContainer]}>
                        <Touchable style={{}} onPress={this.addToCart}>
                            <Text style={[styles.AddToCart]}>{localize.t('add_to_cart')}</Text>
                        </Touchable>
                    </View>
                </View>


                {this.renderVariant()}

                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={false}
                    title={this.state.type==="error"?"":"شكراً لك"}
                    message={this.state.type==="error"?localize.t('please_choose_attr'):localize.t('success_added_to_cart')}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={true}
                    showCancelButton={true}
                    showConfirmButton={false}
                    cancelText={localize.t('ok')}
                    confirmText=""
                    confirmButtonColor={colors.GrayTransparent}
                    onCancelPressed={() => {
                        this.setState({showAlert: false});
                        this.props.updateCart();

                        setTimeout(function(){
                            this.setState({type:'default'});

                        }.bind(this),500)


                    }}
                    onConfirmPressed={() => {
                    }}
                />
            </DefaultHeader>


        );
    }
}

const stylesHTML = StyleSheet.create({
    p: {
        flex: 1,
        alignSelf: 'flex-start',
        ...typography.Text12Light,

    }, a: {
        flex: 1,
        alignSelf: 'flex-start',
        ...typography.Text12Light,

    },
});

const styles = StyleSheet.create({

    ItemSliderImage: {
        width: wp('87%'),
        height: hp('49%'),
    },
    ContainerSliderImage: {

        height: hp('49%'),
        marginHorizontal: wp('5%'),
    },
    dot: {

        backgroundColor: 'transparent',
        borderColor: colors.Black,
        borderWidth: 1,
        width: 10, height: 10, borderRadius: 10,
        marginLeft: 3, marginRight: 3, marginTop: hp('0%'),
        marginBottom: -hp('0%'),

    },
    dotActive: {

        backgroundColor: colors.yellow,
        width: 10, height: 10, borderRadius: 10,
        marginLeft: 3, marginRight: 3, marginTop: hp('0%'),
        marginBottom: -hp('0%'),

    },

    CloseContainer: {
        width: wp('100%'),
        marginTop: hp('5%'), textAlign: 'center', alignItems: 'center', justifyContent: 'center',
    },
    Close: {
        ...typography.Text18OpenSansBold,
        paddingBottom: hp('1%'),
        color: 'white',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },

    productSku: {
        ...typography.Text14OpenSansRegular,
        color: colors.red,
        paddingHorizontal: wp('5%'),
        paddingVertical: wp('1%'),

    },
    productName: {
        ...typography.Text16OpenSansBold,
        color: colors.CustomBlack,

        paddingHorizontal: wp('5%'),
        paddingVertical: wp('1%'),

    },
    PriceContent: {

        justifyContent: 'space-between',
        flex: 1,
        flexDirection: 'row',
        paddingHorizontal: wp('5%'),
    },
    descriptionContent: {

        // flex:1,
        // flexDirection:'column',
        paddingHorizontal: wp('5%'),
        // backgroundColor: 'red',
        flex: 1,
        // alignSelf: 'stretch',

    },
    description: {
        ...typography.Text16OpenSansBold,
        color: colors.CustomBlack,

    },
    oldPrice: {
        ...typography.Text18OpenSansRegular,
        color: colors.Black,
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
        textDecorationColor: colors.red,

    },

    Price: {
        ...typography.Text18OpenSansRegular,
        ...AppStyle.FontGreen,


    },


    containerHtml: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 300,
        direction: 'rtl',
        textAlign: 'right',
    },


    AddToCartContainer: {
        position: 'absolute', bottom: 0, flex: 1,
    },
    AddToCart: {

        ...typography.Text20OpenSansBold,
        ...AppStyle.FontWhite,
        textAlign: 'center',
        width: wp('100%'),
        backgroundColor: colors.yellow,
        ...Platform.select({
            ios: {

                paddingBottom: hp('2%'),
                paddingTop: hp('1%'),

            },

            android: {
                paddingBottom: hp('1%'),
                paddingTop: hp('2%'),

            },
        }),
    },

});
const
    mapStateToProps = state => {
        return {
            current: state.product.current,
            isLoadingAction: state.product.isLoadingAction,
            statusVariant: state.product.statusVariant,
            ErrorMessageVariant: state.product.ErrorMessageVariant,
            variant: state.product.variant,
            cart: state.cart.cart,

        };
    };
const
    mapDispatchToProps = dispatch => {
        return {

            addProductToCart: (payload) => dispatch(addToCart(payload)),
            selectedProduct: (payload) => dispatch(openSelectedProduct(payload)),
            getVariant: (payload) => dispatch(getVariant(payload)),
            updateCart: (payload) => dispatch(CountCartUpdate(payload)),

        };
    };

export default connect(mapStateToProps, mapDispatchToProps)

(
    ProductDetailsScreen,
)
;


