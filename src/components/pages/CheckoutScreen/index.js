import React from 'react';
import {
    TouchableWithoutFeedback,
    StyleSheet,
    InteractionManager,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View, Alert,
    TextInput, Platform, KeyboardAvoidingView,
} from 'react-native';
import {connect, Logo, FastImage, DefaultHeader, wp, hp, Swiper, AppStyle, colors, typography} from './../../../all';
import localize from '../../../locales/i18n';
import FooterSocialMedia from '../../template/FooterSocialMedia';
import {NAVIGATION_DRAWER, NAVIGATION_HOME_SCREEN} from '../../../navigation/types';
import InputElement from '../../elements/TextInput';
import RNPickerSelect from 'react-native-picker-select';
import CustomButton from '../../elements/CustomButton';
import {getCustomerCart} from '../../../store/cart/actions';
import {placeCartOrder, resetPlaceOrder} from '../../../store/checkout/actions';
import {FAILURE, SUCCESS} from '../../../constants/actionsType';
import Reactron from 'reactotron-react-native';
import Loader from '../../elements/Loader';
import {CountCartUpdate} from '../../../store/home/actions';
import AwesomeAlert from 'react-native-awesome-alerts';

class CheckoutScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);
        this.state = {
            city_id: global.user.city_id,
            phone_number: global.user.phone,
            delivery_address: '',
            order_note: '',
            didFinishInitialAnimation: false,
            showAlert: false,
            first: false,

        };

        this.placeOrder = this.placeOrder.bind(this);

    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {

            setTimeout(function () {
                this.setState({
                    didFinishInitialAnimation: true,
                });
                this.props.customerCart({});
                this.props.actionResetPlaceOrder();
            }.bind(this), 1);

        });
    }

    placeOrder() {
        var cart_sumbit = this.props.cart;

        cart_sumbit.order_note = this.state.order_note;
        cart_sumbit.city_id = this.state.city_id;
        cart_sumbit.delivery_address = this.state.delivery_address;
        cart_sumbit.user_id = global.user.id;
        this.props.actionPlaceCartOrder(cart_sumbit);

    }

    renderErrorMessage() {
        if (this.props.statusPlaceHolder === FAILURE) {
            return <View style={[AppStyle.CenterContent, {flexDirection: 'column'}]}>

                {this.props.ErrorMessagePlaceHolder && this.props.ErrorMessagePlaceHolder.map((e) => {
                    return <TouchableWithoutFeedback key={e}><Text
                        style={[typography.Text12Regular, {color: colors.red}]}>{e}</Text></TouchableWithoutFeedback>;

                })}

            </View>;
        }
        else if (this.props.statusPlaceHolder === SUCCESS) {



            if (!this.state.first) {
                this.setState({first: true});
                this.setState({showAlert: true});

            }

            return null;


        }
    }


    render() {

        return (


            <DefaultHeader headerTitle={localize.t('checkout')}
                           didFinishInitialAnimation={this.state.didFinishInitialAnimation} {...this.props}>
                <View style={{flex: 1}}>
                    <ScrollView>

                        <View>
                            <KeyboardAvoidingView
                                behavior={Platform.OS === 'ios' ? 'padding' : undefined}
                                keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
                                style={[Styles.ContainerMargin]}>


                                <View style={[Styles.marginButtonTopInput, {flexDirection: 'row'}]}>

                                    <View style={[AppStyle.CenterContent]}>
                                        <Text
                                            style={[typography.Text14OpenSansRegular, AppStyle.opacity05]}>{localize.t('city')}</Text>

                                    </View>

                                    <View style={[Styles.marginSelect, {flex: 4}]}>
                                        <RNPickerSelect
                                            style={AppStyle.stylesPicker}
                                            placeholder={{
                                                label: localize.t('select_item'),
                                                value: null,
                                                color: AppStyle.FontBlackDark,
                                            }}
                                            value={this.state.city_id}
                                            onValueChange={(value) => this.setState({city_id: value})}
                                            items={global.cities}
                                        />
                                    </View>


                                </View>

                                <View style={[Styles.marginButtonTopInput]}>
                                    <InputElement onChangeText={(vale) => {
                                        this.setState({delivery_address: vale});
                                    }} text={localize.t('delivery_address')}
                                    />
                                </View>


                                <View style={[Styles.marginButtonTopInput]}>
                                    <InputElement value={this.state.phone_number} disabled={true}
                                                  text={localize.t('phone_number')}
                                    />
                                </View>


                                <View style={[Styles.marginButtonTopInput]}>
                                    <InputElement onChangeText={(vale) => {
                                        this.setState({order_note: vale});
                                    }} placeholder={''} text={localize.t('customer_note')}
                                    />
                                </View>

                                <View style={[AppStyle.CenterContent, Styles.marginButtonTop]}>
                                    <CustomButton onPress={this.placeOrder} gradientColor={AppStyle.GradientColor}
                                                  text={localize.t('place_order')}/>
                                </View>

                                {this.renderErrorMessage()}


                            </KeyboardAvoidingView>
                        </View>

                    </ScrollView>
                </View>

                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={false}
                    title={'شكراً لك'}
                    message={localize.t('success_place_order')}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={true}
                    showCancelButton={true}
                    showConfirmButton={false}
                    cancelText={localize.t('ok')}
                    confirmText=""
                    confirmButtonColor={colors.GrayTransparent}
                    onCancelPressed={() => {


                        global.storage.remove({key: 'cart'});

                        global.cart = null;

                        this.props.actionResetPlaceOrder();

                        setTimeout(function () {
                            this.props.updateCart();

                            this.setState({showAlert: false});
                            this.setState({first: false});

                            this.props.navigation.navigate(NAVIGATION_HOME_SCREEN);

                        }.bind(this), 200);

                    }}
                    onConfirmPressed={() => {
                    }}
                />
            </DefaultHeader>


        );
    }
}


const Styles = {

    ContainerMargin: {
        marginTop: hp('1%'),
        marginStart: hp('4%'),
        marginEnd: hp('4%'),
    },
    marginSecondInput: {
        marginTop: hp('4%'),

    },
    marginButtonTop: {
        marginTop: hp('4%'),
        marginBottom: hp('4%'),

    }, marginButtonTopInput: {
        marginTop: hp('3%'),

    }, marginSelect: {
        marginTop: hp('0%'),
        marginStart: hp('2%'),
        marginEnd: hp('0%'),

    },
};


const mapStateToProps = state => {
    return {

        isLoadingAction: state.checkout.isLoadingAction,
        statusPlaceHolder: state.checkout.statusPlaceHolder,
        ErrorMessagePlaceHolder: state.checkout.ErrorMessagePlaceHolder,
        cart: state.cart.cart,
    };
};
const mapDispatchToProps = dispatch => {
    return {

        customerCart: (payload) => dispatch(getCustomerCart(payload)),
        actionPlaceCartOrder: (payload) => dispatch(placeCartOrder(payload)),
        actionResetPlaceOrder: (payload) => dispatch(resetPlaceOrder(payload)),
        updateCart: (payload) => dispatch(CountCartUpdate(payload)),

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutScreen);
