import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
    KeyboardAvoidingView,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput,
    StyleSheet, Platform, TouchableWithoutFeedback,
} from 'react-native';
import {
    CustomButton,
    LinearGradient,
    localize,
    InputElement,
    AuthHeader,
    AppStyle,
    TextUnderLine,
    typography,
    RNPickerSelect, connect,axios
} from './../../../all';
import colors from '../../../constants/colors';
import {NAVIGATION_DRAWER} from '../../../navigation/types';
import {CustomerSignUp, resetAuthState} from '../../../store/auth/actions';
import {FAILURE, SUCCESS} from '../../../constants/actionsType';
import CountryPicker from 'react-native-country-picker-modal';
import {url} from '../../../api/api';

class SignUpScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    area = [];

    // Picker ;

    constructor(props) {
        super(props);
        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            password: '',
            phone: '',
            address: '',
            region: '',
            showPassword: true,
            visible: false,
            city: '',
            city_id: '',
            country: {
                cca2: 'Iraq',
                callingCode: [964],
            },
        };

        this.props.reset();

        this.openPicker = this.openPicker.bind(this);
    }

    navigateToHome = () => {

        setTimeout(function () {

            this.props.signUp({
                first_name: this.state.first_name,
                last_name: this.state.last_name,
                phone: this.state.phone,
                address: this.state.address,
                email: this.state.email,
                city_id: this.state.city_id,
                password: this.state.password,
                app_token: 'شسيشسي',
                country_code: this.state.country.callingCode[0],
            });
        }.bind(this), AppStyle.timeOut);

    };

    renderErrorMessage() {
        if (this.props.status === FAILURE) {
            return <View style={[AppStyle.CenterContent, {flexDirection: 'column'}]}>

                {this.props.ErrorMessage && this.props.ErrorMessage.map((e) => {
                    return <TouchableWithoutFeedback key={e}><Text
                        style={[typography.Text12Regular, {color: colors.red}]}>{e}</Text></TouchableWithoutFeedback>;

                })}

            </View>;
        }
        else if (this.props.status === SUCCESS) {
            this.props.reset();


            global.storage.save({
                key: 'user',
                data: {user: this.props.user, logged: true, is_active: true},
                expires: null,
            });
            global.user = this.props.user;
            global.logged = true;
            global.role = 'customer';
            global.is_active = true;

            ////console.log('global.user');
            ////console.log(global.user);

            axios.post(url+'users/auth/token',
                {
                    user_id: global.user.id,
                    os: Platform.OS,
                    token: global.token + "",
                }
                , null)
                .then(function (response) {
                    console.log("firebase save  req");
                    // console.log(response);
                    // console.log("instide req");
                    return {status: 200, response: response.data, message: ""}
                })
                .catch(function (error) {
                    console.log("firebase save  err");
                    // console.log(error.response.data);
                    return {status: 400, response: null, message: error.response.data.message}
                });

            this.props.navigation.navigate(NAVIGATION_DRAWER);

            return null;


        }
    }

    openPicker() {
        this.setState({visible: true});
    }

    render() {


        return (<AuthHeader isLoadingAction={this.props.isLoadingAction} {...this.props}
                            titleHeader={localize.t('sign_up')}>

                <KeyboardAvoidingView
                    behavior={Platform.OS === 'ios' ? 'padding' : undefined}
                    keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
                    style={[Styles.ContainerMargin]}>

                    <View>
                        <InputElement text={localize.t('first_name') + ' *'}

                                      onChangeText={(value) => {
                                          this.setState({first_name: value});
                                      }}
                        />
                    </View>
                    <View style={[Styles.marginButtonTopInput]}>
                        <InputElement text={localize.t('last_name') + ' *'}

                                      onChangeText={(value) => {
                                          this.setState({last_name: value});
                                      }}
                        />
                    </View>


                    <View style={[Styles.marginButtonTopInput, {
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                        alignItems: 'center',
                    }]}>
                        <InputElement text={localize.t('phone') + ' *'}
                                      onChangeText={(value) => {
                                          this.setState({phone: value});
                                      }}
                        />

                        <View style={{position: 'absolute', top: -1000}}>
                            <CountryPicker
                                visible={this.state.visible}
                                withCountryNameButton
                                withCallingCode
                                onSelect={(country) => {
                                    // alert(country);
                                    this.setState({visible: false});
                                    //console.log(country);
                                    this.setState({country: country});
                                }} onClose={(country) => {
                                // alert(country);
                                this.setState({visible: false});
                                // //console.log(country);
                            }}
                            />
                        </View>
                        <View>
                            <TouchableOpacity
                                onPress={this.openPicker}><Text>{this.state.country ? this.state.country.cca2 + ' (+' + this.state.country.callingCode[0] + ')' : 'test'}</Text></TouchableOpacity>
                        </View>
                    </View>

                    <View style={[Styles.marginSecondInput]}>
                        <InputElement
                            text={localize.t('password') + ' *'}
                            textLength={this.state.password.length}
                            onPressShow={() => {
                                this.setState({showPassword: !this.state.showPassword});
                            }}
                            onChangeText={(value) => {
                                this.setState({password: value});
                            }}
                            type={'password'}
                            secureTextEntry={this.state.showPassword}/>
                    </View>


                    <View style={[Styles.marginButtonTopInput]}>
                        <InputElement text={localize.t('email_address')}
                                      onChangeText={(value) => {
                                          this.setState({email: value});
                                      }}
                        />

                    </View>

                    <View style={[Styles.marginButtonTopInput]}>
                        <InputElement placeholder={localize.t('area_placeholder')} text={localize.t('area')}
                                      onChangeText={(value) => {
                                          this.setState({address: value});
                                      }}
                        />
                    </View>

                    <View style={[Styles.marginButtonTopInput, {flexDirection: 'row'}]}>

                        <View style={[AppStyle.CenterContent]}>
                            <Text
                                style={[typography.Text14OpenSansRegular, AppStyle.opacity05]}>{localize.t('city')+" *"}</Text>

                        </View>

                        <View style={[Styles.marginSelect, {flex: 4}]}>
                            <RNPickerSelect
                                style={AppStyle.stylesPicker}
                                placeholder={{
                                    label: localize.t('select_item'),
                                    value: null,
                                    // color: AppStyle.FontBlackDark,
                                }}
                                onValueChange={(value) => this.setState({city_id: value})}
                                items={global.cities ? global.cities : []}
                            />
                        </View>


                    </View>

                    {this.renderErrorMessage()}

                    <View style={[AppStyle.CenterContent, Styles.marginButtonTop]}>
                        <CustomButton onPress={this.navigateToHome} gradientColor={AppStyle.GradientColor}
                                      text={localize.t('sign_up')}/>
                    </View>

                </KeyboardAvoidingView>


            </AuthHeader>


        );
    }
}

const Styles = {

    ContainerMargin: {
        marginTop: hp('5%'),
        marginStart: hp('4%'),
        marginEnd: hp('4%'),
    },
    marginSecondInput: {
        marginTop: hp('4%'),

    },
    marginButtonTop: {
        marginTop: hp('4%'),
        marginBottom: hp('4%'),

    }, marginButtonTopInput: {
        marginTop: hp('3%'),

    }, marginSelect: {
        marginTop: hp('0%'),
        marginStart: hp('2%'),
        marginEnd: hp('0%'),

    },
};

const mapStateToProps = state => {
    return {
        isLoadingAction: state.auth.isLoadingAction,
        status: state.auth.status,
        ErrorMessage: state.auth.ErrorMessage,
        user: state.auth.user,
    };
};
const mapDispatchToProps = dispatch => {
    return {

        signUp: (payload) => dispatch(CustomerSignUp(payload)),
        reset: (payload) => dispatch(resetAuthState(payload)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);
