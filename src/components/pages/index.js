// Pages
import HomeScreen from './HomeScreen';
import HomeScreen2 from './HomeScreen2';
import SplashScreen from './SplashScreen';
import LoginScreen from './LoginScreen';
import SignUpScreen from './SignUpScreen';
import WelcomeScreen from './WelcomeScreen';
import SearchScreen from './SearchScreen';
import OffersScreen from './OffersScreen';
import CategoryScreen from './CategoryScreen';
import CartScreen from './CartScreen';
import ProductDetailsScreen from './ProductDetailsScreen';
import CategoryProductsScreen from './CategoryProductsScreen';
import ComplainsScreen from './ComplainsScreen';
import ProfileScreen from './ProfileScreen';
import EditProfileScreen from './EditProfileScreen';
import MyOrdersScreen from './MyOrdersScreen';
import MyOrderDetailsScreen from './MyOrderDetailsScreen';
import AboutScreen from './AboutScreen';
import SearchProductsScreen from './SearchProductsScreen';

export {
    SplashScreen,
    LoginScreen,
    HomeScreen,
    WelcomeScreen,
    SignUpScreen,
    HomeScreen2,
    SearchScreen,
    OffersScreen,
    CategoryScreen,
    CartScreen,
    ProductDetailsScreen,
    CategoryProductsScreen,
    ComplainsScreen,
    ProfileScreen,
    EditProfileScreen,
    MyOrdersScreen,
    MyOrderDetailsScreen,
    AboutScreen,SearchProductsScreen
};



