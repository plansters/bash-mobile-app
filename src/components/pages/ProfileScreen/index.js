import React from 'react';
import {
    TouchableWithoutFeedback,
    StyleSheet,
    InteractionManager,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput,
} from 'react-native';
import {Logo, FastImage, DefaultHeader, wp, hp, Swiper, AppStyle, colors, typography} from './../../../all';
import localize from '../../../locales/i18n';
import FooterSocialMedia from '../../template/FooterSocialMedia';
import {
    NAVIGATION_EDIT_PROFILE_SCREEN,
    NAVIGATION_HOME_SCREEN,
    NAVIGATION_MYORDERS_SCREEN,
} from '../../../navigation/types';
import CodePush from 'react-native-code-push';

class ProfileScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);
        this.state = {};

        this.state = {
            didFinishInitialAnimation: false,
        };
        this.navigateToEditProfile = this.navigateToEditProfile.bind(this);
    }

    navigateToEditProfile = () => {

        setTimeout(function () {

            this.props.navigation.navigate(NAVIGATION_EDIT_PROFILE_SCREEN);

        }.bind(this), AppStyle.timeOut);

    };
    navigateToMyOrders = () => {

        setTimeout(function () {

            this.props.navigation.navigate(NAVIGATION_MYORDERS_SCREEN);

        }.bind(this), AppStyle.timeOut);

    };
    Logout = () => {

        setTimeout(function () {


            global.storage.remove({
                key: 'user',
            });


            CodePush.restartApp();

        }.bind(this), AppStyle.timeOut);

    };

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {

            setTimeout(function () {
                this.setState({
                    didFinishInitialAnimation: true,
                });

            }.bind(this), 1);

        });
    }

    render() {

        return (


            <DefaultHeader headerTitle={localize.t('profile')}
                           didFinishInitialAnimation={this.state.didFinishInitialAnimation} {...this.props}>
                <View style={{flex: 1, marginTop: hp('3%')}}>
                    <ScrollView>

                        <View style={[AppStyle.CenterContent]}>
                            {/*<Logo width={hp('15%')} height={hp('15%')}/>*/}
                            <Image
                                style={{
                                    width:hp("30%"),
                                    height:hp("15%"),
                                    marginBottom:wp("3%"),
                                }}
                                source={require('./../../../../src/assets/images/Logo.png')}
                            />
                            <Text style={[typography.Text14OpenSansRegular, {marginTop: hp('2%')}]}>{global.user.first_name+" "+global.user.last_name}</Text>
                            <Text style={[typography.Text14OpenSansRegular]}> {global.user.phone} </Text>
                        </View>

                        <View style={[AppStyle.CenterContent, {marginTop: hp('5%')}]}>
                            <TouchableOpacity underlayColor='rgba(73,182,77,1,0.9)' onPress={this.navigateToEditProfile}
                                              style={[AppStyle.CenterContent, AppStyle.itemButton]}><Text style={[,
                                typography.Text14OpenSansBold, AppStyle.CenterText,
                                , AppStyle.FontBlack]}>{localize.t('edit_profile')}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity underlayColor='rgba(73,182,77,1,0.9)'
                                              onPress={this.navigateToMyOrders}
                                              style={[AppStyle.CenterContent, AppStyle.itemButton]}><Text style={[,
                                typography.Text14OpenSansBold, AppStyle.CenterText,
                                , AppStyle.FontBlack]}>{localize.t('my_last_orders')}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity underlayColor='rgba(73,182,77,1,0.9)' onPress={this.Logout}
                                              style={[AppStyle.CenterContent, AppStyle.itemButton]}><Text style={[,
                                typography.Text14OpenSansBold, AppStyle.CenterText,
                                , AppStyle.FontBlack]}>{localize.t('logout')}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[AppStyle.CenterContent, {marginTop: hp('5%')}]}>
                            <FooterSocialMedia/>
                        </View>


                    </ScrollView>
                </View>

            </DefaultHeader>


        );
    }
}


export default ProfileScreen;
