import React, {useState, useEffect} from 'react';
import {
    Text,
    TouchableOpacity,
    View,
    I18nManager, AsyncStorage, Image,
} from 'react-native';
import {colors, typography, Logo, localize, hp, wp, CodePush, ApplicationLayout, AppStyle} from './../../../all';

class ChooseLanguage extends React.Component {

    static navigationOptions = {title: 'Welcome', header: null};

    constructor(props) {
        super(props);
    }


    render() {

        return (

            <ApplicationLayout isLoading={false}>
                <View style={[AppStyle.CenterContent, {flex: 1}]}>
                    {/*<Logo width={wp('60%')} height={wp('60%')}/>*/}
                    <Image
                        style={{
                            width:hp("34%"),
                            height:hp("17%"),
                        }}
                        source={require('./../../../../src/assets/images/Logo.png')}
                    />
                </View>
                <View style={[AppStyle.CenterContentRow, {flex: 1, paddingHorizontal: wp('8%')}]}>

                    <View>

                        <TouchableOpacity onPress={() => {

                            AsyncStorage.setItem('lang', 'ar')
                                .then(() => {
                                    I18nManager.forceRTL(true);
                                    CodePush.restartApp();
                                });

                        }} style={[AppStyle.CenterContent, styles.itemColor]}><Text style={[,
                            typography.Text14Light, AppStyle.FontBlack, AppStyle.CenterText,
                        ]}>العربية</Text>
                        </TouchableOpacity>

                    </View>

                    <View>

                        <TouchableOpacity onPress={() => {

                            AsyncStorage.setItem('lang', 'en')
                                .then(() => {
                                    I18nManager.forceRTL(false);
                                    CodePush.restartApp();


                                });

                        }} style={[AppStyle.CenterContent, styles.itemColor, styles.selectedColor]}><Text style={[,
                            typography.Text14Light, AppStyle.FontBlack, AppStyle.CenterText,
                        ]}>English</Text>
                        </TouchableOpacity>

                    </View>
                </View>

            </ApplicationLayout>

        );
    }
}

const styles = {


    itemColor: {
        alignItems: 'center', justifyContent: 'center',
        marginEnd: hp('1%'),
        marginTop: hp('1%'),
        marginBottom: hp('1%'),
        paddingStart: hp('6%'),
        paddingEnd: hp('6%'),
        paddingTop: hp('1.3%'),
        paddingBottom: hp('1.3%'),
        borderRadius: 8,
        backgroundColor: colors.white,
        borderWidth: 1,
        borderColor: colors.CustomBlack,
    }
    ,
    selectedColor: {
        backgroundColor: colors.yellow,
        borderWidth: 0,
    },
};


export default ChooseLanguage;
