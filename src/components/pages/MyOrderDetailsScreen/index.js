import React from 'react';
import {
    TouchableWithoutFeedback,
    StyleSheet,
    InteractionManager,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput, Platform, KeyboardAvoidingView, Alert,
} from 'react-native';
import {
    MyOrderList,
    Touchable,
    FastImage,
    DefaultHeader,
    wp,
    hp,
    Swiper,
    AppStyle,
    colors,
    localize,
    typography, connect,
} from './../../../all';
import {NAVIGATION_HOME_SCREEN, NAVIGATION_PRODUCT_SCREEN} from '../../../navigation/types';
import CartProductList from '../../elements/CartProductList';
import NumberFormat from 'react-number-format';
import {CustomerLogin} from '../../../store/login/actions';
import {CustomerCancelOrder, CustomerCancelOrderReset} from '../../../store/order/actions';
import {FAILURE, SUCCESS} from '../../../constants/actionsType';

class MyOrderDetailsScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};
    order = null;

    constructor(props) {
        super(props);

        this.state = {
            didFinishInitialAnimation: false,
        };

        this.cancelOrder = this.cancelOrder.bind(this);
        this.renderErrorMessage = this.renderErrorMessage.bind(this);
        this.order = this.props.current;
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {

            setTimeout(function () {
                this.setState({
                    didFinishInitialAnimation: true,
                });
                this.props.CancelOrderReset();


            }.bind(this), 1);

        });
    }

    cancelOrder() {


        this.props.CancelOrder({
            order_id: this.order.id,
        });
    }


    renderErrorMessage() {
        if (this.props.statusCancel === FAILURE) {
            return <View style={[AppStyle.CenterContent, {flexDirection: 'column'}]}>

                {this.props.ErrorMessageCancel && this.props.ErrorMessageCancel.map((e) => {
                    return <TouchableWithoutFeedback key={e}><Text
                        style={[typography.Text12Regular, {color: colors.red}]}>{e}</Text></TouchableWithoutFeedback>;

                })}

            </View>;
        }
        else if (this.props.statusCancel === SUCCESS) {


            Alert.alert(
                'نجاح',
                localize.t('order_cancel_success'), // <- this part is optional, you can pass an empty string
                [
                    {
                        text: 'OK', onPress: () => {
                            this.props.CancelOrderReset();

                            setTimeout(function () {

                                this.props.navigation.navigate(NAVIGATION_HOME_SCREEN);

                            }.bind(this), 200);
                        },
                    },
                ],
                {cancelable: false},
            );

            return null;


        }
    }

    render() {

        return (
            <DefaultHeader headerTitle={localize.t('order_number') + '# ' + this.order.id}
                           didFinishInitialAnimation={this.state.didFinishInitialAnimation} {...this.props}>
                <View style={{flex: 1}}>
                    <View style={[, {flex: 8}]}>

                        <CartProductList

                            type={'order_details'}
                            data={this.order && this.order.products && this.order.products.length > 0 ? this.order.products : []}
                        />
                    </View>
                    <View style={[AppStyle.CenterContent, {
                        flex: 2,
                        justifyContent: 'space-between',
                    }]}>

                        <View style={[AppStyle.CenterContent, {flex: 1, flexDirection: 'row'}]}>
                            <Text style={[style.total]}>{localize.t('total')}</Text>
                            <NumberFormat value={this.order.total_price} renderText={value => <Text
                                style={[style.price]}>{value} {this.order.currency}</Text>}
                                          displayType={'text'}
                                          thousandSeparator={true} prefix={''}/>
                        </View>
                        {this.renderErrorMessage()}
                        {this.order.allowed_cancel ?
                            <Touchable onPress={this.cancelOrder} style={{backgroundColor: 'red'}}>
                                <Text style={[style.CreateOrder]}>{localize.t('cancel_order')}</Text>
                            </Touchable> : <React.Fragment></React.Fragment>}


                    </View>
                </View>


            </DefaultHeader>


        );
    }
}


const style = StyleSheet.create({

    CreateOrder: {

        ...typography.Text20OpenSansBold,
        ...AppStyle.FontWhite,
        textAlign: 'center',
        width: wp('100%'),
        backgroundColor: colors.yellow,
        ...Platform.select({
            ios: {

                paddingBottom: hp('2%'),
                paddingTop: hp('1%'),

            },

            android: {
                paddingBottom: hp('2%'),
                paddingTop: hp('2%'),

            },
        }),
    },
    total: {
        ...typography.Text16OpenSansBold,
        ...AppStyle.FontYellow,
        textAlign: 'center',
    },
    price: {
        ...typography.Text16OpenSansBold,
        color: colors.green,
        textAlign: 'center',
        paddingStart: wp('1%'),
    },

});
const mapStateToProps = state => {
    return {
        current: state.order.current,
        isLoadingAction: state.order.isLoadingAction,
        ErrorMessageCancel: state.order.ErrorMessageCancel,
        statusCancel: state.order.statusCancel,
    };
};
const mapDispatchToProps = dispatch => {
    return {

        CancelOrder: (payload) => dispatch(CustomerCancelOrder(payload)),
        CancelOrderReset: (payload) => dispatch(CustomerCancelOrderReset(payload)),

    };
};

export default connect(mapStateToProps, mapDispatchToProps)

(
    MyOrderDetailsScreen,
)
;

