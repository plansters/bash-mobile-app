import React from 'react';
import {
    TouchableWithoutFeedback,
    StyleSheet,
    InteractionManager,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput, Platform, KeyboardAvoidingView, Alert,
} from 'react-native';
import {
    connect,
    Logo,
    FastImage,
    DefaultHeader,
    wp,
    hp,
    Swiper,
    AppStyle,
    colors,
    typography,
    Reactron,
} from './../../../all';
import localize from '../../../locales/i18n';
import FooterSocialMedia from '../../template/FooterSocialMedia';
import {NAVIGATION_HOME_SCREEN} from '../../../navigation/types';
import InputElement from '../../elements/TextInput';
import RNPickerSelect from 'react-native-picker-select';
import CustomButton from '../../elements/CustomButton';
import {CustomerAddComplaint, CustomerComplaintReset} from '../../../store/complaint/actions';
import {editProfile, editProfileReset} from '../../../store/profile/actions';
import {FAILURE, SUCCESS} from '../../../constants/actionsType';
import AwesomeAlert from 'react-native-awesome-alerts';

class EditProfileScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);
        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            password: '',
            phone: '',
            address: '',
            region: '',
            showPassword: true,
            city: '',
            city_id: '',
            didFinishInitialAnimation: false,
            showAlert: false,
            first: false,

        };

        this.AUserEditProfile = this.AUserEditProfile.bind(this);
        this.renderErrorMessage = this.renderErrorMessage.bind(this);

    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {

            setTimeout(function () {
                this.setState({
                    didFinishInitialAnimation: true,
                    first_name: global.user.first_name,
                    last_name: global.user.last_name,
                    email: global.user.email,
                    city_id: global.user.city_id,
                    address: global.user.address,
                });

                this.props.UserEditProfileReset();

            }.bind(this), 1);

        });
    }

    AUserEditProfile() {

        this.props.UserEditProfile({
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            email: this.state.email,
            city_id: this.state.city_id,
            address: this.state.address,
        });
    }

    renderErrorMessage() {
        if (this.props.statusEditProfile === FAILURE) {
            return <View style={[AppStyle.CenterContent, {flexDirection: 'column'}]}>

                {this.props.ErrorMessageEditProfile && this.props.ErrorMessageEditProfile.map((e) => {
                    return <TouchableWithoutFeedback key={e}><Text
                        style={[typography.Text12Regular, {color: colors.red}]}>{e}</Text></TouchableWithoutFeedback>;

                })}

            </View>;
        }
        else if (this.props.statusEditProfile === SUCCESS) {


            global.user = null;

            global.storage.save({
                key: 'user', data: {

                    user: this.props.user,
                    logged: true,
                    is_active: true,
                },
            });

            global.user = this.props.user;


            if (!this.state.first) {
                this.setState({first: true});
                this.setState({showAlert: true});

            }
            // Alert.alert(
            //     'نجاح',
            //     localize.t('edit_profile_success'), // <- this part is optional, you can pass an empty string
            //     [
            //         {
            //             text: 'OK', onPress: () => {
            //                 this.props.UserEditProfileReset();
            //
            //             },
            //         },
            //     ],
            //     {cancelable: false},
            // );

            return null;


        }
    }


    render() {

        return (


            <DefaultHeader headerTitle={localize.t('edit_profile')}
                           didFinishInitialAnimation={this.state.didFinishInitialAnimation} {...this.props}>
                <View style={{flex: 1}}>
                    <ScrollView>

                        <View>
                            <KeyboardAvoidingView
                                behavior={Platform.OS === 'ios' ? 'padding' : undefined}
                                keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
                                style={[Styles.ContainerMargin]}>

                                <View>
                                    <InputElement text={localize.t('first_name')}

                                                  onChangeText={(value) => {
                                                      this.setState({first_name: value});
                                                  }}
                                                  value={this.state.first_name}
                                    />
                                </View>
                                <View style={[Styles.marginButtonTopInput]}>
                                    <InputElement text={localize.t('last_name')}

                                                  onChangeText={(value) => {
                                                      this.setState({last_name: value});
                                                  }}
                                                  value={this.state.last_name}

                                    />
                                </View>


                                <View style={[Styles.marginButtonTopInput]}>
                                    <InputElement text={localize.t('email_address')}
                                                  onChangeText={(value) => {
                                                      this.setState({email: value});
                                                  }}
                                                  value={this.state.email}

                                    />
                                </View>

                                <View style={[Styles.marginButtonTopInput]}>
                                    <InputElement placeholder={localize.t('area_placeholder')} text={localize.t('area')}
                                                  onChangeText={(value) => {
                                                      this.setState({address: value});
                                                  }}
                                                  value={this.state.address}

                                    />
                                </View>

                                <View style={[Styles.marginButtonTopInput, {flexDirection: 'row'}]}>

                                    <View style={[AppStyle.CenterContent]}>
                                        <Text
                                            style={[typography.Text14OpenSansRegular, AppStyle.opacity05]}>{localize.t('city')}</Text>

                                    </View>

                                    <View style={[Styles.marginSelect, {flex: 4}]}>
                                        <RNPickerSelect
                                            style={AppStyle.stylesPicker}
                                            placeholder={{
                                                label: localize.t('select_item'),
                                                value: null,
                                                color: AppStyle.FontBlackDark,
                                            }}
                                            value={this.state.city_id}

                                            onValueChange={(value) => this.setState({city_id: value})}
                                            items={global.cities}
                                        />
                                    </View>


                                </View>

                                {this.renderErrorMessage()}
                                <View style={[AppStyle.CenterContent, Styles.marginButtonTop]}>
                                    <CustomButton onPress={this.AUserEditProfile} gradientColor={AppStyle.GradientColor}
                                                  text={localize.t('save_edit')}/>
                                </View>

                            </KeyboardAvoidingView>
                        </View>

                    </ScrollView>
                </View>


                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={false}
                    title={'نجاح'}
                    message={localize.t('edit_profile_success')}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={true}
                    showCancelButton={true}
                    showConfirmButton={false}
                    cancelText={localize.t('ok')}
                    confirmText=""
                    confirmButtonColor={colors.GrayTransparent}
                    onCancelPressed={() => {
                        this.setState({showAlert: false});
                        this.props.UserEditProfileReset();
                        this.setState({first: false});

                    }}
                    onConfirmPressed={() => {
                    }}
                />

            </DefaultHeader>


        );
    }
}


const Styles = {

    ContainerMargin: {
        marginTop: hp('1%'),
        marginStart: hp('4%'),
        marginEnd: hp('4%'),
    },
    marginSecondInput: {
        marginTop: hp('4%'),

    },
    marginButtonTop: {
        marginTop: hp('4%'),
        marginBottom: hp('4%'),

    }, marginButtonTopInput: {
        marginTop: hp('3%'),

    }, marginSelect: {
        marginTop: hp('0%'),
        marginStart: hp('2%'),
        marginEnd: hp('0%'),

    },
};


const mapStateToProps = state => {
    return {
        isLoadingAction: state.profile.isLoadingAction,
        ErrorMessageEditProfile: state.profile.ErrorMessageEditProfile,
        statusEditProfile: state.profile.statusEditProfile,
        user: state.profile.user,

    }
        ;
};
const mapDispatchToProps = dispatch => {
    return {

        UserEditProfile: (payload) => dispatch(editProfile(payload)),
        UserEditProfileReset: (payload) => dispatch(editProfileReset(payload)),

    };
};

export default connect(mapStateToProps, mapDispatchToProps)

(
    EditProfileScreen,
)
;
