import React, {useState, useEffect} from 'react';
import {
    Text,
    TouchableOpacity,
    TouchableHighlight,
    TouchableWithoutFeedback,
    View,
    Button,
    ScrollView, Image,
} from 'react-native';
import {AppStyle, colors, typography, Logo, localize, hp, wp, CodePush, ApplicationLayout} from './../../../all';
import {
    NAVIGATION_CATEGORY_PRODUCTS_SCREEN,
    NAVIGATION_DRAWER,
    NAVIGATION_HOME_SCREEN,
    NAVIGATION_LOGIN_SCREEN,
    NAVIGATION_SIGNUP_SCREEN,
} from '../../../navigation/types';

class WelcomeScreen extends React.Component {

    static navigationOptions = {title: 'Welcome', header: null};

    constructor(props) {
        super(props);
        this.navigateToHome = this.navigateToHome.bind(this);
    }

    navigateToHome = () => {
        setTimeout(function () {
            this.props.navigation.navigate(NAVIGATION_HOME_SCREEN);
            // this.props.navigation.navigate(NAVIGATION_CATEGORY_PRODUCTS_SCREEN);

        }.bind(this), AppStyle.timeOut);
    };
    navigateToLogin = () => {
        setTimeout(function () {
            this.props.navigation.navigate(NAVIGATION_LOGIN_SCREEN);
            // this.props.navigation.navigate(NAVIGATION_CATEGORY_PRODUCTS_SCREEN);

        }.bind(this), AppStyle.timeOut);
    };
    navigateToSignUp = () => {
        setTimeout(function () {
            this.props.navigation.navigate(NAVIGATION_SIGNUP_SCREEN);
            // this.props.navigation.navigate(NAVIGATION_CATEGORY_PRODUCTS_SCREEN);

        }.bind(this), AppStyle.timeOut);
    };

    render() {

        return (

            <ApplicationLayout isLoading={false}>
                <ScrollView style={{flex: 1}}>
                    <View style={{height: hp('100%')}}>
                        <View style={[AppStyle.CenterBottomContent, {flex: 2}]}>
                            <Text style={[typography.Text22Light, {}]}>{localize.t('welcome')}</Text>
                        </View>

                        <View style={[AppStyle.CenterContent, {flex: 4}]}>
                            {/*<Logo style={{}} width={hp('30%')} height={hp('30%')}/>*/}

                            <Image
                                style={{
                                    width:hp("34%"),
                                    height:hp("17%"),
                                }}
                                source={require('./../../../../src/assets/images/Logo.png')}
                            />
                        </View>

                        <View style={[AppStyle.CenterTopContent, {
                            flex: 3,
                            paddingStart: wp('8%'),
                            paddingEnd: wp('8%'),


                        }]}>

                            <View>

                                <TouchableOpacity underlayColor='rgba(73,182,77,1,0.9)' onPress={this.navigateToLogin}
                                                  style={[AppStyle.CenterContent, AppStyle.itemButton]}><Text style={[,
                                    typography.Text14OpenSansBold, AppStyle.CenterText,
                                    , AppStyle.FontBlack]}>{localize.t('login')}</Text>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity underlayColor='rgba(73,182,77,1,0.9)' onPress={this.navigateToSignUp}
                                                  style={[AppStyle.CenterContent, AppStyle.itemButton]}><Text style={[,
                                    typography.Text14OpenSansBold, AppStyle.CenterText,
                                    , AppStyle.FontBlack]}>{localize.t('sign_up')}</Text>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity underlayColor='rgba(73,182,77,1,0.9)' onPress={this.navigateToHome}
                                                  style={[AppStyle.CenterContent, AppStyle.itemButton]}><Text style={[,
                                    typography.Text14OpenSansBold, AppStyle.CenterText,
                                    , AppStyle.FontBlack]}>{localize.t('login_as_visitor')}</Text>
                                </TouchableOpacity>
                            </View>


                        </View>

                    </View>


                </ScrollView>

            </ApplicationLayout>

        );
    }
}


export default WelcomeScreen;
