import React from 'react';
import {
    TouchableWithoutFeedback,
    StyleSheet,
    InteractionManager,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput,
    FlatList, Platform, BackHandler,
} from 'react-native';
import {
    LinearGradient,
    CategoryList,
    FastImage,
    wp,
    hp,
    Swiper,
    AppStyle,
    colors,
    localize, typography,
    ProductListTwoRowHorizontal, CustomButton,
    ButtonTransparent, ButtonStroked,
    DefaultHeader, connect, Loader,
} from './../../../all';
import {
    NAVIGATION_CATEGORY_PRODUCTS_SCREEN, NAVIGATION_COMPLAINS_SCREEN,
    NAVIGATION_OFFER_SCREEN,
    NAVIGATION_PRODUCT_SCREEN,
} from '../../../navigation/types';

import {Categories, openSelectedCategory} from '../../../store/category/actions';
import {CountCartUpdate, HomeCategories, HomeOffers, HomeSliders} from '../../../store/home/actions';
import {openSelectedProduct} from '../../../store/product/actions';
import {SUCCESS} from '../../../constants/actionsType';

class HomeScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {};


        ////console.log(Platform.OS);

        this.state = {
            didFinishInitialAnimation: false,
        };

        this.onPressProduct = this.onPressProduct.bind(this);
        this.onPressCategory = this.onPressCategory.bind(this);
        this.RenderHomeOffers = this.RenderHomeOffers.bind(this);
        this.renderHomeCategories = this.renderHomeCategories.bind(this);
        this.openOffers = this.openOffers.bind(this);
        this.openComplain = this.openComplain.bind(this);
        this.openSlide = this.openSlide.bind(this);


    }


    componentWillUnmount() {
        //BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        alert('Back button is pressed');
        return true;
    }

    componentDidMount() {
        // BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

        InteractionManager.runAfterInteractions(() => {

            setTimeout(function () {
                this.setState({
                    didFinishInitialAnimation: true,
                });

                this.props.getHomeSliders();
                this.props.getHomeCategories();
                this.props.getHomeOffers();
                this.props.updateCart();
            }.bind(this), 1);

        });
    }

    onPressProduct(item) {

        this.props.selectedProduct(item);

        this.props.navigation.navigate(NAVIGATION_PRODUCT_SCREEN);
    }

    onPressCategory(item) {

        this.props.selectedCategory({id: item.id, name: item.name});
        this.props.navigation.navigate(NAVIGATION_CATEGORY_PRODUCTS_SCREEN);


    }

    renderItem({item}) {
        const {selected} = this.state;

        return (
            <Category
                id={item.id}
                onPress={this.onPressItem}
                selected={!!selected.get(item.id)}
                title={item.title}
            />
        );
    }

    openSlide(item) {

        if (item.type === 'category') {
            this.onPressCategory(item.category);

        } else if (item.type === 'offers') {
            this.props.navigation.navigate(NAVIGATION_OFFER_SCREEN);

        } else if (item.type === 'product') {

            this.onPressProduct(item.product);
        }
    }

    renderHomeSlider() {
        if (this.props.isLoadingHomeSliders) {
            return <Loader didFinishInitialAnimation={!this.props.isLoadingHomeSliders}/>;
        }
        else {
            return <View style={[styles.ContainerSliderImage, {flex: 1, overflow:'hidden'}]}>
                <Swiper
                    height={wp('50%')}
                    dotColor={colors.CustomBlack}
                    activeDotColor={colors.yellow}
                    loop={true}
                    showsButtons={false}
                    autoplay={true}
                    autoplayTimeout={5}
                    autoplayDirection={true}
                    paginationStyle={{
                        bottom: wp('8'), left: null, right: 10,
                    }}
                    dot={<View
                        style={styles.dot}/>}
                    activeDot={<View
                        style={[styles.dotActive]}/>}>
                    {this.props.homeSliders.map((e, i) => {
                        return (<View key={'slide' + e.id} style={[AppStyle.CenterContent]}>
                            <TouchableWithoutFeedback onPress={this.openSlide.bind(this, e)} style={[]}>
                                <FastImage
                                    style={[styles.ItemSliderImage]}
                                    resizeMode={FastImage.resizeMode.contain}
                                    source={{uri: e.image_url}}
                                />
                            </TouchableWithoutFeedback>
                        </View>);
                    })}


                </Swiper></View>;
        }
    }

    renderHomeCategories() {
        if (this.props.isLoadingHomeCategories) {
            return <Loader didFinishInitialAnimation={!this.props.isLoadingHomeCategories}/>;
        }
        else {
            return <CategoryList
                onPressItem={this.onPressCategory}
                {...this.props}
                data={this.props.homeCategories}
            />;
        }
    }


    RenderHomeOffers() {
        if (this.props.isLoadingHomeOffers) {
            return <Loader didFinishInitialAnimation={!this.props.isLoadingHomeOffers}/>;
        }
        else {
            return <React.Fragment>
                <ProductListTwoRowHorizontal
                    onPressItem={this.onPressProduct}
                    home={true}
                    {...this.props}
                    data={this.props.homeOffers}
                />
                <View style={[AppStyle.CenterContent, {marginBottom: wp('8%')}]}>
                    <ButtonTransparent onPress={this.openOffers} gradientColor={['#000', '#000']} type={'simple'}
                                       text={localize.t('see_more')}/>
                </View>
            </React.Fragment>;
        }
    }

    openOffers() {

        this.props.navigation.push(NAVIGATION_OFFER_SCREEN);

    }

    openComplain() {

        this.props.navigation.push(NAVIGATION_COMPLAINS_SCREEN);

    }


    render() {

        return (


            <DefaultHeader type={'home'}
                           didFinishInitialAnimation={this.state.didFinishInitialAnimation} {...this.props}>
                <ScrollView>
                    <View style={{flex: 1}}>


                        {this.renderHomeSlider()}


                        <View
                            style={[styles.categoryContainerStyle]}>

                            {this.renderHomeCategories()}


                        </View>

                        <LinearGradient
                            colors={AppStyle.GradientColorDark}
                            style={[styles.offersContainerStyle]}>

                            <Text
                                style={[typography.Text18OpenSansBold, AppStyle.FontYellow, AppStyle.CenterTopContent, AppStyle.CenterText]}>{localize.t('offers')}</Text>


                            {this.RenderHomeOffers()}

                        </LinearGradient>
                        <View style={[styles.reportStyle]}>

                            <Text style={[typography.Text12Regular]}>{localize.t('are_u_have_report')}</Text>

                            <ButtonStroked onPress={this.openComplain} backgroundColor={colors.yellow} type={'simple'}
                                           text={localize.t('contact_with_us')}/>
                        </View>

                    </View>
                </ScrollView>

            </DefaultHeader>


        );
    }
}

const styles = StyleSheet.create({

    ItemSliderImage: {
        height: 220 * (wp(87) / 373),
        width: wp(100),

    },
    ContainerSliderImage: {

        marginHorizontal: wp('5%'),
        marginTop: wp('5%'),
        borderWidth: 1,
        borderColor: colors.Black,
        borderRadius: 3,
    },
    dot: {

        backgroundColor: 'transparent',
        borderColor: colors.Black,
        borderWidth: 1,
        width: 10, height: 10, borderRadius: 10,
        marginLeft: 3, marginRight: 3, marginTop: hp('0%'),
        marginBottom: -hp('4%'),

    },
    dotActive: {

        backgroundColor: colors.yellow,
        width: 10, height: 10, borderRadius: 10,
        marginLeft: 3, marginRight: 3, marginTop: hp('0%'),
        marginBottom: -hp('4%'),

    },

    categoryContainerStyle: {
        flex: 1,
        marginHorizontal: wp('5%'),
        marginTop: wp('5%'),
        marginBottom: wp('8%'),
    },
    offersContainerStyle: {
        flex: 1,
        marginTop: wp('5%'),
        // marginBottom: wp('8%'),
        paddingTop: wp('5%'),
    },
    reportStyle: {
        alignItems: 'center',
        paddingHorizontal: wp('4%'),
        justifyContent: 'space-between',
        flexDirection: 'row',
        // backgroundColor: 'red',
        paddingBottom: wp('3%'),
        paddingTop: wp('1%'),
    },
});

const mapStateToProps = state => {
    return {
        isLoadingHomeCategories: state.home.isLoadingHomeCategories,
        statusHomeCategories: state.home.statusHomeCategories,
        homeCategories: state.home.homeCategories,
        ErrorMessage: state.home.ErrorMessage,

        isLoadingHomeOffers: state.home.isLoadingHomeOffers,
        statusHomeOffers: state.home.statusHomeOffers,
        homeOffers: state.home.homeOffers,

        isLoadingHomeSliders: state.home.isLoadingHomeSliders,
        statusHomeSliders: state.home.statusHomeSliders,
        homeSliders: state.home.homeSliders,
    };
};
const mapDispatchToProps = dispatch => {
    return {

        selectedCategory: (payload) => dispatch(openSelectedCategory(payload)),
        getHomeCategories: (payload) => dispatch(HomeCategories(payload)),
        getHomeOffers: (payload) => dispatch(HomeOffers(payload)),
        getHomeSliders: (payload) => dispatch(HomeSliders(payload)),
        selectedProduct: (payload) => dispatch(openSelectedProduct(payload)),
        updateCart: (payload) => dispatch(CountCartUpdate(payload)),

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
