import React from 'react';
import {
    TouchableWithoutFeedback,
    StyleSheet,
    InteractionManager,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput,
    Button, FlatList,
} from 'react-native';
import {
    connect,
    CollapsibleList,
    Collapsible,
    FastImage,
    DefaultHeader,
    wp,
    hp,
    Swiper,
    AppStyle,
    colors,
} from './../../../all';
import localize from '../../../locales/i18n';
import {Categories, openSelectedCategory} from '../../../store/category/actions';
import ProductListTwoRowHorizontal from '../../elements/ProductListTwoRowHorizontal';
import {NAVIGATION_CATEGORY_PRODUCTS_SCREEN} from '../../../navigation/types';

class CategoryScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);
        this.state = {};

        this.state = {
            didFinishInitialAnimation: false,
            isCollapsed: true,

        };
        this.onPressCategory = this.onPressCategory.bind(this)
    }

    componentDidMount() {

        InteractionManager.runAfterInteractions(() => {

            setTimeout(function () {
                this.setState({
                    didFinishInitialAnimation: true,
                });

                this.props.getCategories({});
            }.bind(this), 1);

        });
    }

    onPressCategory(item) {

        this.props.selectedCategory({id:item.id,name:item.name})

        this.props.navigation.navigate(NAVIGATION_CATEGORY_PRODUCTS_SCREEN);
        // //console.log('category===== oepned');


    }
    render() {

        return (


            <DefaultHeader  isLoadingAction={this.props.isLoadingAction} headerTitle={localize.t('categories')}
                           didFinishInitialAnimation={this.state.didFinishInitialAnimation} {...this.props}>

                <View style={[style.paddingPage]}>
                    <CollapsibleList
                        onPressItem={this.onPressCategory}
                        data={this.props.categories}
                    />
                </View>

            </DefaultHeader>


        );
    }
}


const style = StyleSheet.create({
    paddingPage: {
        paddingHorizontal: wp('3%'),
        marginBottom:wp("25%"),
    },
});
const mapStateToProps = state => {
    return {
        isLoadingAction: state.category.isLoadingAction,
        status: state.category.status,
        categories: state.category.categories,
    };
};
const mapDispatchToProps = dispatch => {
    return {

        selectedCategory: (payload) => dispatch(openSelectedCategory(payload)),
        getCategories: (payload) => dispatch(Categories(payload)),

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryScreen);
