import React from 'react';
import {
    TouchableWithoutFeedback,
    StyleSheet,
    InteractionManager,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput,
    Button, FlatList,
} from 'react-native';
import {
    Logo,
    CollapsibleList,
    Collapsible,
    FastImage,
    DefaultHeader,
    wp,
    hp,
    Swiper,
    AppStyle,
    colors, typography,
} from './../../../all';
import localize from '../../../locales/i18n';
import Touchable from '../../elements/Touchable';

class AboutScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);
        this.state = {};

        this.state = {
            didFinishInitialAnimation: false,

        };
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {

            setTimeout(function () {
                this.setState({
                    didFinishInitialAnimation: true,
                });

            }.bind(this), 1);

        });
    }

    render() {

        return (


            <DefaultHeader headerTitle={localize.t('about_us')}
                           didFinishInitialAnimation={this.state.didFinishInitialAnimation} {...this.props}>

                <ScrollView>
                    <View style={[style.paddingPage]}>

                        {/*<Logo width={wp('35%')} height={wp('35%')}/>*/}
                        <Image
                            style={{
                                width:hp("38%"),
                                height:hp("20%"),
                                marginBottom:wp("3%"),
                            }}
                            source={require('./../../../../src/assets/images/Logo.png')}
                        />
                        <Text style={[typography.Text18Light,{textAlign:'center'}]}>

                            شركه باش بغداد هي من الشركات الرائده في تجاره الالبسه الجاهزه تأسست عام 2012  عن تراكم خبرات اكثر من 30 عاماً بصناعه وتجاره الألبسه الرجاليه حيث نقوم بتوفير المنتجات من عده مصادر (تركيا وتايلند والصين) بأعلى درجات المهنيه والحرفيه في مصانعنا هناك.... ومنذ التأسيس وجدنا ان السوق العراقي بحاجه ماسه لنوعيات ممتازه وفاخره بأسعار منخفضه تناسب دخل المواطن العراقي ، باشرنا بالعمل على حاجة السوق بكل صدق وكان ذالك سر نجاحنا وتوسعنا بفترة زمنية قليلة, شعارنا الأفضل والأفخم بأقل الأرباح

                            </Text>
                    </View>
                </ScrollView>


            </DefaultHeader>


        );
    }
}


const style = StyleSheet.create({
    paddingPage: {
        paddingHorizontal: wp('3%'),
        justifyContent: 'center',
        alignItems: 'center',
    },
});
export default AboutScreen;
