import React from 'react';
import {
    TouchableWithoutFeedback,
    StyleSheet,
    InteractionManager,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput,
} from 'react-native';
import {FastImage, DefaultHeader, wp, hp, Swiper, AppStyle, colors} from './../../../all';

class HomeScreen2 extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);
        this.state = {};

        this.state = {
            didFinishInitialAnimation: false,
        };
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {

            setTimeout(function () {
                this.setState({
                    didFinishInitialAnimation: true,
                });

            }.bind(this), 1);

        });
    }

    render() {

        return (


            <DefaultHeader
                           didFinishInitialAnimation={this.state.didFinishInitialAnimation} {...this.props}>

                <TouchableOpacity onPress={() => {

                    this.props.navigation.toggleDrawer();

                }}>
                    <Text>Drwer 2</Text>
                </TouchableOpacity>

            </DefaultHeader>


        );
    }
}


export default HomeScreen2;
