import React from 'react';
import {
    TouchableWithoutFeedback,
    StyleSheet,
    InteractionManager,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput, Platform,
} from 'react-native';
import {
    ProductListTwoColumnVertical,
    FastImage,
    DefaultHeader,
    wp,
    hp,
    Swiper,
    AppStyle,
    colors,
    localize, typography, connect, Reactron, Loader,
} from './../../../all';
import RNPickerSelect from 'react-native-picker-select';
import CartProductList from '../../elements/CartProductList';
import {NAVIGATION_PRODUCT_SCREEN} from '../../../navigation/types';
import {
    productLists,
    productListsRest,
} from '../../../store/ProductList/actions';
import {openSelectedProduct} from '../../../store/product/actions';
import {SubCategoriesRest, SubCategoryLists} from '../../../store/category/actions';
import CategorySubList from '../../elements/CategorySubList';

class CategoryProductsScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    category = {
        'name': '',
        'id': '',
    };

    constructor(props) {
        super(props);
        this.state = {

            order_by: '',
            page: 1,
            didFinishInitialAnimation: false,
        };
        this.onPressProduct = this.onPressProduct.bind(this);
        this.handleLoadMore = this.handleLoadMore.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
        this.OrderBy = this.OrderBy.bind(this);
        this.onPressItemCategory = this.onPressItemCategory.bind(this);


        this.category = this.props.currentCategory;


    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {

            setTimeout(function () {
                this.setState({
                    didFinishInitialAnimation: true,
                });

                this.props.productListsRest({});
                // this.props.subCategoriesRest({});

                if (this.props.currentCategory) {

                    this.props.getProducts({
                        category: this.props.currentCategory.id,
                        page: this.state.page,
                        refresh: false,
                        order_by: this.state.order_by,
                    });

                    this.props.subCategories({category: this.props.currentCategory.id});

                }

            }.bind(this), 1);

        });
    }

    onPressProduct(item) {

        this.props.selectedProduct(item);
        this.props.navigation.push(NAVIGATION_PRODUCT_SCREEN);
    }

    handleLoadMore() {

        var page = this.state.page;
        page = page + 1;

        if (this.props.last_page >= page && !this.props.isLoadingAction) {
            this.setState({page: page}, () => {

                if (this.category) {
                    this.props.getProducts({
                        category: this.category.id,
                        page: this.state.page,
                        refresh: false,
                        order_by: this.state.order_by,
                    });

                }

            });
        }


    }

    handleRefresh() {

        this.props.productListsRest({});

        this.setState({page: 1}, () => {

            if (this.category) {
                this.props.getProducts({
                    category: this.category.id,
                    page: this.state.page,
                    refresh: true,
                    order_by: this.state.order_by,
                });

            }

        });

    }

    OrderBy(value) {
        this.setState({order_by: value}, function () {

            this.props.productListsRest();
            this.handleRefresh();

        }.bind(this));
    }

    onPressItemCategory(item) {


        this.props.productListsRest({});

        this.setState({page: 1}, () => {

            if (this.category) {
                this.props.getProducts({
                    category: item.id,
                    page: this.state.page,
                    refresh: true,
                    order_by: this.state.order_by,
                });

            }

        });

    }

    render() {

        return (
            <DefaultHeader headerTitle={this.category.name}
                           didFinishInitialAnimation={this.state.didFinishInitialAnimation} {...this.props}>

                <View style={{padding: wp(2)}}>
                    <CategorySubList
                        data={this.props.subCategoriesList}
                        onPressItem={this.onPressItemCategory}
                    />

                </View>
                <View style={[styles.FilterContainer]}>

                    <Text style={[typography.Text16penSansRegular]}>{localize.t('order_by')}</Text>

                    <View style={{flex: 0.7}}>
                        <RNPickerSelect
                            style={AppStyle.stylesPicker}
                            placeholder={{
                                label: localize.t('select_item'),
                                value: null,
                                ...AppStyle.FontBlackDark,
                            }}
                            onValueChange={(value, index) => {
                                this.OrderBy(value);
                            }}
                            items={[
                                {label: 'سعر منخفض إلى مرتفع', value: 'order_by=unit_price&order=asc'},
                                {label: 'سعر مرتفع إلى منخفض', value: 'order_by=unit_price&order=desc'},
                                {label: 'الترتيب الأبجدي', value: 'order_by=name'},
                                {label: 'الأحدث', value: 'order_by=created_at'},
                            ]}
                        />
                    </View>


                </View>
                <View style={{paddingBottom: Platform.os == 'android' ? wp('25%') : wp('30%')}}>
                    <ProductListTwoColumnVertical
                        onPressItem={this.onPressProduct}
                        {...this.props}
                        home={false}
                        data={this.props.productList}
                        handleLoadMore={this.handleLoadMore}
                        handleRefresh={this.handleRefresh}

                    />
                </View>


            </DefaultHeader>

        );
    }
}

const styles = {
    FilterContainer: {
        paddingHorizontal: wp('6%'),
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: wp('2%'),
    },
};


const mapStateToProps = state => {
    return {
        currentCategory: state.category.currentCategory,
        last_page: state.productList.last_page,
        productList: state.productList.productList,
        isLoadingAction: state.productList.isLoadingAction,
        status: state.productList.status,
        errorMessage: state.productList.ErrorMessage,
        subCategoriesList: state.category.subCategoryList,

    };
};
const mapDispatchToProps = dispatch => {
    return {

        getProducts: (payload) => dispatch(productLists(payload)),
        selectedProduct: (payload) => dispatch(openSelectedProduct(payload)),
        productListsRest: (payload) => dispatch(productListsRest(payload)),
        subCategories: (payload) => dispatch(SubCategoryLists(payload)),
        subCategoriesRest: (payload) => dispatch(SubCategoriesRest(payload)),

    };


};

export default connect(mapStateToProps, mapDispatchToProps)

(
    CategoryProductsScreen,
)
;
