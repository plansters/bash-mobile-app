import React from 'react';
import {
    TouchableWithoutFeedback,
    StyleSheet,
    InteractionManager,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    View,
    TextInput, Platform,
} from 'react-native';
import {
    MyOrderList,
    Touchable,
    FastImage,
    DefaultHeader,
    wp,
    hp,
    Swiper,
    AppStyle,
    colors,
    localize,
    typography, connect,
} from './../../../all';
import {NAVIGATION_MYORDER_DETAILS_SCREEN, NAVIGATION_PRODUCT_SCREEN} from '../../../navigation/types';
import {openSelectedOrder, orderLists, orderListsRest} from '../../../store/order/actions';
import ProductListTwoColumnVertical from '../../elements/ProductListTwoColumnVertical';
import Reactron from 'reactotron-react-native';

class MyOrdersScreen extends React.Component {

    // static navigationOptions = { title: 'Welcome', header: null };
    static navigationOptions = {title: 'Home', header: null};

    constructor(props) {
        super(props);

        this.state = {
            order_by: '',
            page: 1,
            didFinishInitialAnimation: false,
        };
        this.onPressOrder = this.onPressOrder.bind(this);
        this.handleLoadMore = this.handleLoadMore.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {

            setTimeout(function () {
                this.setState({
                    didFinishInitialAnimation: true,
                });

                this.props.resetOrderList({});


                this.props.getOrders({
                    page: this.state.page,
                    refresh: false,
                    order_by: this.state.order_by,
                });


            }.bind(this), 1);

        });
    }

    onPressOrder(item) {

        //console.log('order=====');
        ////console.log(item);
        this.props.selectedOrder(item);
        this.props.navigation.push(NAVIGATION_MYORDER_DETAILS_SCREEN);
    }

    handleLoadMore() {

        ////console.log('load more ');
        var page = this.state.page;
        page = page + 1;

        ////console.log(page);
        ////console.log(this.props.last_page);
        if (this.props.last_page >= page && !this.props.isLoadingAction) {
            this.setState({page: page}, () => {

                this.props.getOrders({
                    page: this.state.page,
                    refresh: false,
                    order_by: this.state.order_by,
                });


            });
        }


    }

    handleRefresh() {

        ////console.log('handle refresh ');
        this.props.resetOrderList({});

        this.setState({page: 1}, () => {

            this.props.getOrders({
                page: this.state.page,
                refresh: true,
                order_by: this.state.order_by,
            });


        });

    }


    render() {

        return (
            <DefaultHeader headerTitle={localize.t('orders')}
                           didFinishInitialAnimation={this.state.didFinishInitialAnimation} {...this.props}>
                <View style={{flex: 1}}>
                    <View style={[, {marginBottom: Platform.os == 'android' ? wp('10%') : wp('10%')}]}>

                        <MyOrderList
                            {...this.props}
                            onPressItem={this.onPressOrder}
                            data={this.props.orderList}
                            handleLoadMore={this.handleLoadMore}
                            handleRefresh={this.handleRefresh}
                        />
                    </View>
                </View>

            </DefaultHeader>


        );
    }
}


const style = StyleSheet.create({

    CreateOrder: {

        ...typography.Text20OpenSansBold,
        ...AppStyle.FontWhite,
        textAlign: 'center',
        width: wp('100%'),
        backgroundColor: colors.yellow,
        ...Platform.select({
            ios: {

                paddingBottom: hp('2%'),
                paddingTop: hp('1%'),

            },

            android: {
                paddingBottom: hp('2%'),
                paddingTop: hp('2%'),

            },
        }),
    },
    total: {
        ...typography.Text16OpenSansBold,
        ...AppStyle.FontYellow,
        textAlign: 'center',
    },
    price: {
        ...typography.Text16OpenSansBold,
        color: colors.green,
        textAlign: 'center',
        paddingStart: wp('1%'),
    },

});

const mapStateToProps = state => {
    return {

        orderList: state.order.orderList,
        isLoadingAction: state.order.isLoadingAction,
        status: state.order.status,
        errorMessage: state.order.errorMessage,
        last_page: state.order.last_page,
    };
};
const mapDispatchToProps = dispatch => {
    return {

        resetOrderList: (payload) => dispatch(orderListsRest(payload)),
        getOrders: (payload) => dispatch(orderLists(payload)),
        selectedOrder: (payload) => dispatch(openSelectedOrder(payload)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)

(
    MyOrdersScreen,
)
;
