import React from 'react';
import {
    TouchableNativeFeedback,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    TouchableOpacity,
    View,
    Platform,
} from 'react-native';
import {
    Touchable,
    LinearGradient,
    colors,
    typography,
    AppStyle,
    wp,
    hp,
    localize,
    EyeUnActive,
    EyeActive,
} from './../../../all';

const CustomButton = ({
                          onPress,
                          text,
                          style,
                          textStyle,
                          ifHome,
                          gradientColor,
                      }) => (

    <View style={[styles.ButtonStyle]}>

        <LinearGradient style={[styles.ButtonStyleLinear]} colors={gradientColor}>

            <Touchable onPress={onPress}
                       style={[AppStyle.CenterContent, styles.itemButtonBig]}><Text
                style={[,
                    typography.Text16Light, AppStyle.CenterText,
                    , AppStyle.FontWhite]}>{text}</Text>
            </Touchable>
        </LinearGradient>
    </View>


);

const styles = {
    ButtonStyle: {
        width: wp('80%'),
        borderRadius: 8,
        borderWidth: 1,
        borderColor: 'transparent',
        shadowColor: colors.Gray,
        shadowOpacity: 1,
        shadowRadius: 3,
        shadowOffset: {
            height: 4,
            width: 2,
        },
    }, ButtonStyleSimple: {},
    ButtonStyleLinear: {
        borderRadius: 8,
        paddingTop: hp('1.5%'),
        paddingBottom: hp('1.5%'),
    },
    itemButtonBig: {
        alignItems: 'center', justifyContent: 'center',
        marginTop: hp('1%'),
        marginBottom: hp('1%'),
        paddingTop: hp('5%'),
        paddingBottom: hp('5%'),

    },
    itemButton: {
        marginTop: hp('1%'),
        marginBottom: hp('1%'),
        paddingHorizontal: wp('8%'),
        paddingVertical: wp('0.5%'),
        borderRadius: 8,
        borderWidth: 2,
        // borderColor: colors.white,


    },
};
export default CustomButton;
