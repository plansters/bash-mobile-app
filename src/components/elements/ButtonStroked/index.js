import React from 'react';
import {
    Platform,
    TouchableNativeFeedback,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    TouchableOpacity,
    View,
} from 'react-native';
import {
    Touchable,
    LinearGradient,
    colors,
    typography,
    AppStyle,
    wp,
    hp,
    localize,
    EyeUnActive,
    EyeActive,
} from './../../../all';
const ButtonStroked = ({
                           onPress,
                           text,
                           style,
                           textStyle,
                           ifHome,
                           gradientColor,
                           backgroundColor,
                       }) => (

    <View  >

        <Touchable  onPress={onPress}>
            <View style={[{flex: 1, backgroundColor: backgroundColor}, AppStyle.CenterContent, styles.itemButton]}>
                <Text
                    style={[,
                        typography.Text16Light, AppStyle.CenterText,
                        , AppStyle.FontWhite, {}]}>{text}</Text>

            </View>
        </Touchable>

    </View>


);

const styles = {

    itemButton: {
        marginTop: hp('1%'),
        marginBottom: hp('1%'),
        paddingHorizontal: wp('8%'),
        paddingVertical: wp('0.5%'),
        borderRadius: 8,
        borderWidth: 2,
        borderColor: 'transparent',
    },
};
export default ButtonStroked;
