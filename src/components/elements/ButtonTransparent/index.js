import React from 'react';
import {
    Platform,
    TouchableNativeFeedback,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    TouchableOpacity,
    View,
} from 'react-native';
import {
    Touchable,
    LinearGradient,
    colors,
    typography,
    AppStyle,
    wp,
    hp,
    localize,
    EyeUnActive,
    EyeActive,
} from './../../../all';

const ButtonTransparent = ({
                               onPress,
                               text,
                               style,
                               textStyle,
                               ifHome,
                               gradientColor,

                           }) => (

    <View style={[]}>

        <Touchable onPress={onPress}><Text
            style={[, AppStyle.CenterContent, styles.itemButton,
                typography.Text16Light, AppStyle.CenterText,
                , AppStyle.FontWhite]}>{text}</Text>
        </Touchable>
    </View>


);

const styles = {

    itemButton: {
        marginTop: hp('1%'),
        marginBottom: hp('1%'),
        paddingHorizontal: wp('8%'),
        paddingVertical: wp('0.5%'),
        borderRadius: 8,
        borderWidth: 2,
        borderColor: colors.white,


    },
};
export default ButtonTransparent;
