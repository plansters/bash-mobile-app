import React from 'react';
import {
    StyleSheet,
    FlatList,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native';
import {MyOrderItem, hp, wp, typography, colors} from './../../../all';
import localize from '../../../locales/i18n';

class MyOrderList extends React.Component {
    constructor(props) {
        super(props);

        this.onPressItem = this.onPressItem.bind(this);
        this.renderItem = this.renderItem.bind(this);

        ////console.log(this.props);
    }


    onPressItem(item) {

        this.props.onPressItem(item);
    }


    renderItem({item}) {

        return (
            <MyOrderItem
                {...this.props}
                item={item}
                onPressItem={this.onPressItem}
            />
        );
    }


    _listEmptyComponent = () => {
        return (<Text style={[typography.Text16Light, {
            textAlign: 'center',
            padding: wp('3%'),
        }]}>{localize.t('empty_result')}</Text>);
    };

    render() {
        const {data} = this.props;
        //console.log(data);
        return (
            <FlatList
                ListEmptyComponent={data != null && data.length == 0 && !this.props.isLoadingAction ? this._listEmptyComponent : null}
                data={data}
                extraData={this.state}
                keyExtractor={item => item.id + ''}
                renderItem={this.renderItem}
                onEndReachedThreshold={0.001}
                onEndReached={this.props.handleLoadMore}
                refreshing={this.props.isLoadingAction}
                onRefresh={this.props.handleRefresh}
            />
        );
    }
}

const style = StyleSheet.create({});
export default MyOrderList;
