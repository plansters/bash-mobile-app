import React from 'react';
import {
    ImageBackground,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
    StyleSheet,

} from 'react-native';
import {FastImage, hp, wp, typography, colors, AppStyle} from './../../../all';
import NumberFormat from 'react-number-format';

class Product extends React.Component {
    constructor(props) {
        super(props);

        this.handleOnPress = this.handleOnPress.bind(this);
    }


    handleOnPress() {
        const {item, home, onPressItem} = this.props;
        //console.log('federder');
        setTimeout(function () {
            this.props.onPressItem(item);

        }.bind(this), AppStyle.timeOut);


    }

    render() {
        const {item, home} = this.props;

        ////console.log('home' + home);
        return (

            <TouchableOpacity onPress={this.handleOnPress} style={[style.itemTouch]}>

                <View style={[home ? style.itemHome : style.item]}>
                    <FastImage
                        style={[home?style.ItemImageHome:style.ItemImage]}
                        resizeMode={FastImage.resizeMode.contain}
                        source={{
                            uri: Array.isArray(item.product_images) && item.product_images.length > 0 ? item.product_images[0].media_url : 'https://static.thenounproject.com/png/220984-200.png',
                            priority: FastImage.priority.high,
                        }}/>
                    <View>
                        <View style={[style.ItemPricesContainer]}>

                            {item.unit_discount_price != null && parseInt(item.unit_discount_price) > 0 ?

                                <React.Fragment>
                                    <NumberFormat value={item.unit_discount_price} displayType={'text'}
                                                  renderText={value => <Text
                                                      style={[style.Price]}>{value} {item.currency}</Text>}
                                                  thousandSeparator={true} prefix={''}/>

                                    <NumberFormat value={item.unit_price} renderText={value => <Text
                                        style={[home ? style.oldPriceHome : style.oldPrice]}> {value} {item.currency}</Text>}
                                                  displayType={'text'}
                                                  thousandSeparator={true} prefix={''}/>
                                </React.Fragment>
                                :
                                <React.Fragment>
                                    <NumberFormat value={item.unit_price} displayType={'text'}
                                                  renderText={value => <Text
                                                      style={[style.Price]}> {value} {item.currency}</Text>}
                                                  thousandSeparator={true} prefix={''}/>
                                </React.Fragment>}

                        </View>
                    </View>

                    <Text
                        style={[typography.Text12Regular, home ? AppStyle.FontWhite : AppStyle.FontBlack, style.Title]}>{item.name}</Text>


                </View>

            </TouchableOpacity>
        );
    }
}


const style = StyleSheet.create({
    itemTouch: {},


    itemHome: {
        width: wp('50%'),
        marginTop: wp('5%'),
        marginBottom: wp('5%'),
        // backgroundColor: 'red',
        // borderRadius: 7,
        marginStart: wp('4%'),
        alignItems: 'center',


    },
    item: {
        width: wp('43%'),
        marginTop: wp('5%'),
        marginBottom: wp('5%'),
        // backgroundColor: 'red',
        // borderRadius: 7,
        marginStart: wp('4%'),
        alignItems: 'center',


    },
    ItemImage: {
        height: 190,
        width: wp('40%'),
        resizeMode: 'repeat',
        borderRadius: 5,

    },
    ItemImageHome: {
        height: 190,
        width: wp('50%'),
        resizeMode: 'repeat',
        borderRadius: 5,

    },
    ItemPricesContainer: {
        paddingHorizontal: wp('1%'),
        alignItems:'center',
        justifyContent: 'center', flexDirection: 'column',
    },
    oldPriceHome: {
        ...typography.Text14Light,
        color: colors.white,
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
        textDecorationColor: colors.red,

    },
    oldPrice: {
        ...typography.Text14Light,
        color: colors.Black,
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
        textDecorationColor: colors.red,

    },
    Price: {
        ...typography.Text14Light,
        ...AppStyle.FontGreen,

    },
    Title: {
        paddingStart: wp('1%'),
        paddingEnd: wp('1%'),
        // backgroundColor: 'red',
    },
});

export default Product;
