import React from 'react';
import {
    StyleSheet,
    FlatList,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native';
import {CartProductItem, hp, wp, typography, colors, AppStyle, localize} from './../../../all';

class CartProductList extends React.Component {
    constructor(props) {
        super(props);

        this.onPressItem = this.onPressItem.bind(this);
        this.renderItem = this.renderItem.bind(this);

        ////console.log(this.props);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }

    onPressItem(id) {

    }


    renderItem({item}) {

        return (
            <CartProductItem
                {...this.props}
                item={item}
                onPress={this.onPressItem}
                removeItem={this.props.removeItem}
                plusItem={this.props.plusItem}
                minusItem={this.props.minusItem}
            />
        );
    }


    render() {
        const {data,type} = this.props;
        //console.log(data);
        ////console.log("Cart Products")
        return (
            <FlatList
                type={type}
                data={data}
                extraData={this.state}
                keyExtractor={item => item.id}
                renderItem={this.renderItem}
                ListEmptyComponent={<View style={[AppStyle.CenterContent, {
                    flex: 1,
                    marginTop: wp("4%"),
                }]}><Text style={[typography.Text14OpenSansRegular]}>{localize.t("empty")}</Text></View>}
            />
        );
    }
}

const style = StyleSheet.create({});
export default CartProductList;
