import React from 'react';
import {
    StyleSheet,
    FlatList,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native';
import {CategoryForCollapse, Category, hp, wp, typography, colors} from './../../../all';
import CollapsibleItem from '../CollapsibleItem';

class CollapsibleList extends React.Component {
    constructor(props) {
        super(props);

        this.onPressItem = this.onPressItem.bind(this);
        this.renderItem = this.renderItem.bind(this);

        ////console.log(this.props);
    }


    onPressItem(item) {

        const {onPressItem} = this.props;

        this.props.onPressItem(item);
    }


    renderItem({item}) {

        // ////console.log("item");
        // ////console.log(item);

        if(item.sub_categories && item.sub_categories.length>0)
        {
            return (
                <CollapsibleItem
                    {...this.props}
                    item={item}
                    onPressItem={this.onPressItem}
                />
            );
        }else {
           return null
        }



    }


    render() {
        const {data} = this.props;

        // ////console.log("itemAll");
        // ////console.log(data);
        return (
            <FlatList
                data={data}
                extraData={this.state}
                keyExtractor={item => item.id}
                renderItem={this.renderItem}
            />
        );
    }
}

const style = StyleSheet.create({});
export default CollapsibleList;
