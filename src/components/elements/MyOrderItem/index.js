import React from 'react';
import {
    ImageBackground,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
    StyleSheet, Platform,
} from 'react-native';
import {
    OrderIcon,
    Touchable,
    FastImage,
    hp,
    wp,
    typography,
    colors,
    AppStyle,
    localize,
    ButtonStroked,
} from './../../../all';
import NumberFormat from 'react-number-format';

class MyOrderItem extends React.Component {
    constructor(props) {
        super(props);

        this.handleOnPress = this.handleOnPress.bind(this);
    }

    handleOnPress() {
        const {item, onPressItem} = this.props;

        setTimeout(function () {
            this.props.onPressItem(item);

        }.bind(this), AppStyle.timeOut);

    }

    render() {
        const {item, home} = this.props;

        ////console.log('home' + home);
        return (

            <TouchableWithoutFeedback onPress={this.handleOnPress}>

                <View style={[style.ItemContainer]}>

                    <View style={{flexDirection: 'row', flex: 1}}>
                        <View style={[style.itemDescription]}>
                            <Text style={[style.productName]}>{localize.t('order_number')} <Text
                                style={[style.productPrice]}>{item.id}</Text></Text>
                            <Text style={[style.productName]}>{localize.t('status')} <Text
                                style={[style.productPrice]}>{item.translated_status}</Text></Text>
                            <Text style={[style.productName]}>{localize.t('total_order_one')} <Text
                                style={[style.productPrice]}>{item.total_price} {item.currency}</Text></Text>
                            <Text style={[style.productName]}>{localize.t('order_date')} <Text
                                style={[style.productPrice]}>{item.created_at}</Text></Text>

                        </View>


                        <View style={[AppStyle.CenterRightContent, {marginEnd: wp('4%')}]}>
                            <OrderIcon/>
                        </View>

                    </View>

                    <View style={{marginEnd: wp('4%')}}>
                        <ButtonStroked onPress={this.handleOnPress} backgroundColor={colors.yellowTransparent}
                                       text={localize.t('order_details')}/></View>

                </View>

            </TouchableWithoutFeedback>
        );
    }
}


const style = StyleSheet.create({
    ItemContainer: {
        borderWidth: 1,
        borderColor: colors.GrayBorder,
        borderRadius: 4,
        flexDirection: 'column',
        marginHorizontal: wp('4%'),
        marginVertical: wp('2%'),
        //
        // backgroundColor: 'green',

        ...Platform.select({
            ios: {
                shadowColor: colors.GrayBorder,
                shadowOpacity: 1,
                shadowRadius: 3,
                shadowOffset: {
                    height: 3,
                    width: 1,
                },
                paddingTop: hp('1.5%'),
                paddingBottom: hp('1.5%'),
                paddingStart: hp('1.5%'),
            },

            android: {
                shadowColor: colors.GrayBorder,
                paddingTop: hp('1.5%'),
                paddingBottom: hp('1.5%'),
                paddingStart: hp('1.5%'),
            },
        }),
    },
    ItemImage: {
        height: 115,
        width: 115,
    },
    itemDescription: {
        justifyContent: 'space-around',
        alignItems: 'flex-start',
        flex: 1,
        marginHorizontal: wp('2%'),
    },
    productName: {
        ...typography.Text14OpenSansRegular,
    }, productPrice: {
        ...AppStyle.FontGreen,
        ...typography.Text16penSansRegular,
    },
});

export default MyOrderItem;
