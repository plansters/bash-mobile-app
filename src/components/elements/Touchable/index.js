import React from 'react';
import {
    Platform,
    TouchableNativeFeedback,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    TouchableOpacity,
    View,
} from 'react-native';
import {LinearGradient, colors, typography, AppStyle, wp, hp, localize, EyeUnActive, EyeActive} from './../../../all';

const Touchable = (props) => {
    return Platform.OS === 'ios'
        ? <TouchableWithoutFeedback onPress={props.onPress}>{props.children}</TouchableWithoutFeedback>
        : <TouchableNativeFeedback onPress={props.onPress}>{props.children}</TouchableNativeFeedback>
}
export default Touchable;
