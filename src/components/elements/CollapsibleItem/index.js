import React from 'react';
import {
    StyleSheet,
    FlatList,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,

} from 'react-native';
import {
    ArrowBottom,
    ArrowTop,
    Collapsible,
    CategoryForCollapse,
    Category,
    hp,
    wp,
    typography,
    colors,
    AppStyle, FastImage,
} from './../../../all';

class CollapsibleItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isCollapsed: true,
        };
        this.onPressItem = this.onPressItem.bind(this);

        ////console.log(this.props);
    }


    onPressItem(item) {

        this.props.onPressItem(item)
    }


    render() {
        const {item} = this.props;

        ////console.log('item.sub_categories');
        ////console.log(item.sub_categories);
        return (
            <View style={[style.CollapseItemContainer]}>

                <TouchableWithoutFeedback onPress={() => {
                    this.setState({isCollapsed: !this.state.isCollapsed});
                }} style={[, {flex: 1}]}>
                    <View
                        style={[style.CollapseItem, {backgroundColor: this.state.isCollapsed ? colors.Black : colors.yellow}]}>
                        <Text style={[style.itemText, {flex: 1}]}>{item.name}</Text>

                        <View style={[style.arrow]}>
                            <FastImage
                                style={[style.ItemImage]}
                                resizeMode={FastImage.resizeMode.contain}
                                source={this.state.isCollapsed ? require('./../../../assets/icons/arrow_bottom.png') : require('./../../../assets/icons/arrow_top.png')}
                            />
                        </View>
                    </View>

                </TouchableWithoutFeedback>

                <Collapsible collapsed={this.state.isCollapsed} align="start">

                    <View style={[style.container, {flexDirection: 'row'}]}>

                        <View style={[style.row]}>

                            {item.sub_categories.map(function (e) {

                                if (e.sub_categories && e.sub_categories.length > 0) {

                                    return (

                                        <View style={{borderBottomWidth:1,borderColor:colors.LoaderTransparent,marginBottom:wp("3%"),}}>
                                            <Text
                                                style={[typography.Text16OpenSansBold, AppStyle.FontBlack, {marginBottom: 9}]}>{e.name}</Text>
                                            <View style={[style.row2,{marginBottom:wp("3%")}]}>
                                                {e.sub_categories.map(function (e2) {
                                                    return <CategoryForCollapse
                                                        {...this.props}
                                                        item={e2}
                                                        onPressItem={this.onPressItem}
                                                    />;
                                                }.bind(this))}

                                            </View>
                                        </View>
                                    );
                                }
                                else {
                                    return <CategoryForCollapse
                                        {...this.props}
                                        item={e}
                                        onPressItem={this.onPressItem}
                                    />;
                                }

                                // return <Text>{e.name}</Text>
                            }.bind(this))}


                        </View>

                    </View>


                </Collapsible>

            </View>

        );
    }
}

const style = StyleSheet.create({

    CollapseItemContainer: {
        marginVertical: wp('1.5%'),
        ...AppStyle.CenterContent,
    },
    CollapseItem: {
        flex: 1,
        width: wp('90%'),
        ...AppStyle.CenterContent,
        paddingVertical: wp('2%'),
        // backgroundColor: ,
        borderRadius: 8,
        marginBottom: wp('2%'),

    },
    container: {
        // paddingTop: wp("1%"),
        // paddingStart: wp("1%"),
        // paddingEnd: wp("1%"),
        // paddingBottom: wp("1%"),
        // backgroundColor: '#fff',
        // marginStart: wp("4%"),
        // marginEnd: wp("4%"),
    },
    row: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        width: wp('90%'),
        paddingHorizontal: wp('1%'),
    },
    row2: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        width: wp('90%'),
    },
    arrow: {
        position: 'absolute',
        left: wp('3%'),
        top: wp('4%'),

    },
    itemText: {
        ...typography.Text20OpenSansBold,
        ...AppStyle.FontWhite,
    },

    ItemImage: {
        height: 20,
        width: 20,
        resizeMode: 'repeat',
        borderRadius: 5,
    },
});
export default CollapsibleItem;
