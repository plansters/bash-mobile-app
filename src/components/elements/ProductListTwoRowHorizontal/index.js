import React from 'react';
import {
    StyleSheet,
    FlatList,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native';
import {Category, hp, wp, typography, colors, Product} from './../../../all';

class ProductListTwoRowHorizontal extends React.Component {
    constructor(props) {
        super(props);

        this.onPressItem = this.onPressItem.bind(this);
        this.renderItem = this.renderItem.bind(this);

        ////console.log(this.props);
    }


    onPressItem(item) {
        const {onPressItem} = this.props;

        this.props.onPressItem(item);
    }


    renderItem({item}) {

        //console.log('0000000');
        //console.log(this.props);
        return (
            <Product
                {...this.props}
                item={item}
                onPressItem={this.onPressItem}
            />
        );
    }


    render() {
        const {data, home} = this.props;

        var first_half = [];
        var second_half = [];

        data.map(function (e, i) {

            if (i < Math.ceil(data.length / 2)) {
                first_half.push(e);

            }
            else {
                second_half.push(e);

            }
            ////console.log(e);
        });

        return (
            <View>

                <FlatList
                    home={home}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={first_half}
                    extraData={this.state}
                    keyExtractor={item => item.id + 'pr'}
                    renderItem={this.renderItem}

                />
                <FlatList
                    home={home}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={second_half}
                    extraData={this.state}
                    keyExtractor={item => item.id + 'pr'}
                    renderItem={this.renderItem}

                />
            </View>

        );
    }
}

const style = StyleSheet.create({});
export default ProductListTwoRowHorizontal;
