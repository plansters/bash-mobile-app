import React from 'react';
import {
    ImageBackground,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
    StyleSheet,
} from 'react-native';
import {hp, wp, typography, colors, AppStyle, Reactron} from './../../../all';
import ProductAttributesVariable from '../ProductAttributesVariable';

class ProductAttributesListVariable extends React.Component {
    constructor(props) {
        super(props);

        this.handleOnPress = this.handleOnPress.bind(this);
    }


    handleOnPress() {
        const {onPress} = this.props;
        ////console.log(this.props);
    }

    renderItems() {
        const {item, variables, selectedVariable} = this.props;
        return variables.map((variable, variableIndex) => {

            var variab = selectedVariable.find(x => x.value === variable);


            if (variab !== undefined) {

                return <ProductAttributesVariable key={variableIndex} selectVariant={this.props.selectVariant}
                                                  selected={true}
                                                  item={variable}/>;
            }
            else {

                return <ProductAttributesVariable key={variableIndex} selectVariant={this.props.selectVariant}  {...this.props}
                                                  item={variable}/>;

                // return <Text>{variable.title}</Text>;
            }
        });

    }

    render() {
        const {item, variables, selectedVariable} = this.props;
        ////console.log('selectedVariable');
        return this.renderItems();

    }
}


const styles = StyleSheet.create({

    CenterContent: {
        alignItems: 'center', justifyContent: 'center',
    },
    itemColor: {

        alignItems: 'center',
        justifyContent: 'center',
        marginEnd: hp('1%'),
        marginBottom: hp('1%'),
        paddingStart: hp('2%'),
        paddingEnd: hp('2%'),
        paddingTop: hp('1%'),
        paddingBottom: hp('1%'),
        borderRadius: 5,
        // width:wp("20%"),
        backgroundColor: colors.white,
        borderWidth: 1,
        borderColor: colors.CustomBlack,
    }
    ,
    selectedColor: {
        backgroundColor: colors.yellow,
        borderWidth: 0,
    },
});

export default ProductAttributesListVariable;
