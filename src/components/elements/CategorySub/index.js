import React from 'react';
import {
    ImageBackground,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
    StyleSheet,
} from 'react-native';
import {hp, wp, typography, colors, AppStyle} from './../../../all';
import {NAVIGATION_LOGIN_SCREEN} from '../../../navigation/types';
import FastImage from 'react-native-fast-image';

class CategorySub extends React.Component {
    constructor(props) {
        super(props);

        this.handleOnPress = this.handleOnPress.bind(this);
    }


    handleOnPress() {

        const {item, onPressItem} = this.props;
        //console.log('federder category');
        setTimeout(function () {
            this.props.onPressItem(item);

        }.bind(this), AppStyle.timeOut);

    }

    render() {
        const {item} = this.props;
        return (

            <TouchableWithoutFeedback onPress={this.handleOnPress}>

                <View style={[style.item]}>
                    <Text style={style.title}>{item.name}</Text>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}


const style = StyleSheet.create({
    item: {
        // flex: 0.49,
        paddingHorizontal: wp(2),
        paddingVertical: wp(2),
        backgroundColor: colors.yellow,
        marginVertical: wp(0),
        marginHorizontal: wp(1),
        borderRadius: 30,
    },

    title: {
        ...typography.Text12Regular,
        color: colors.white,

    },


});

export default CategorySub;
