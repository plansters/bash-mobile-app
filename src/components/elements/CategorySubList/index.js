import React from 'react';
import {
    StyleSheet,
    FlatList,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native';
import {Category, hp, wp, typography, colors} from './../../../all';
import CategorySub from '../CategorySub';

class CategorySubList extends React.Component {
    constructor(props) {
        super(props);

        this.onPressItem = this.onPressItem.bind(this);
        this.renderItem = this.renderItem.bind(this);

        ////console.log(this.props);
    }


    onPressItem(item) {

        this.props.onPressItem(item);
    }


    renderItem({item}) {

        return (
            <CategorySub
                {...this.props}
                item={item}
                onPressItem={this.onPressItem}
            />
        );
    }


    render() {
        const {data} = this.props;

        //console.log("sub sub ")
        //console.log(data)
        return (
            <FlatList

                horizontal={true}
                showsHorizontalScrollIndicator={false}
                data={data}
                extraData={this.state}
                contentContainerStyle={styles.row}
                keyExtractor={item => item.id+"CSub"}
                renderItem={this.renderItem}
            />
        );
    }
}

const styles = StyleSheet.create({
    row: {

        flexGrow: 1,
        justifyContent: 'flex-start',

    },
});
export default CategorySubList;
