import React from 'react';
import {
    StyleSheet,
    FlatList,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native';
import {Category, hp, wp, typography, colors} from './../../../all';

class CategoryList extends React.Component {
    constructor(props) {
        super(props);

        this.onPressItem = this.onPressItem.bind(this);
        this.renderItem = this.renderItem.bind(this);

        ////console.log(this.props);
    }


    onPressItem(item) {

        this.props.onPressItem(item);
    }


    renderItem({item}) {

        return (
            <Category
                {...this.props}
                item={item}
                onPressItem={this.onPressItem}
            />
        );
    }


    render() {
        const {data} = this.props;

        return (
            <FlatList
                numColumns={2}
                data={data}
                columnWrapperStyle={style.row}
                extraData={this.state}
                keyExtractor={item => item.id}
                renderItem={this.renderItem}
            />
        );
    }
}

const style = StyleSheet.create({
    row: {
        flex: 1,
        justifyContent: 'space-between',
    },
});
export default CategoryList;
