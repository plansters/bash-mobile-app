import React from 'react';
import {
    ImageBackground,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
    StyleSheet,
} from 'react-native';
import {hp, wp, typography, colors, AppStyle, Reactron} from './../../../all';

class ProductAttributesVariable extends React.Component {
    constructor(props) {
        super(props);

        this.state={
            selected:false,
        }
        this.handleOnPress = this.handleOnPress.bind(this);
    }


    handleOnPress() {
        const {item, onPress} = this.props;
        ////console.log(this.props);
        this.setState({selected:true})
        ////console.log(this.state.selected)
        this.props.selectVariant(item);
    }

    render() {
        const {item, selected} = this.props;
        return (


            <TouchableOpacity style={[styles.CenterContent, !selected ? styles.itemColor : styles.itemColorSelected]}
                              onPress={this.handleOnPress}>


                <Text style={[typography.Text14OpenSansRegular
                    , {
                        color: selected ? 'white' : 'black',
                        textAlign: 'center',
                    }]}> {item}</Text>


            </TouchableOpacity>
        );
    }
}


const styles = StyleSheet.create({

    CenterContent: {
        alignItems: 'center', justifyContent: 'center',
    },
    itemColor: {

        alignItems: 'center',
        justifyContent: 'center',
        marginEnd: hp('1%'),
        marginBottom: hp('1%'),
        paddingStart: hp('2%'),
        paddingEnd: hp('2%'),
        paddingTop: hp('1%'),
        paddingBottom: hp('1%'),
        borderRadius: 5,
        // width:wp("20%"),
        backgroundColor: colors.GrayBorder,
        borderWidth: 1,
        borderColor: colors.GrayBorder,
    }, itemColorSelected: {

        alignItems: 'center',
        justifyContent: 'center',
        marginEnd: hp('1%'),
        marginBottom: hp('1%'),
        paddingStart: hp('2%'),
        paddingEnd: hp('2%'),
        paddingTop: hp('1%'),
        paddingBottom: hp('1%'),
        borderRadius: 5,
        // width:wp("20%"),
        backgroundColor: colors.yellow,
        borderWidth: 1,
        borderColor: colors.GrayBorder,
    }
    ,
    selectedColor: {
        backgroundColor: colors.yellow,
        borderWidth: 0,
    },
});

export default ProductAttributesVariable;
