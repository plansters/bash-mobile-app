import React from 'react';
import {
    ImageBackground,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
    StyleSheet,
} from 'react-native';
import {hp, wp, typography, colors, AppStyle} from './../../../all';
import {NAVIGATION_LOGIN_SCREEN} from '../../../navigation/types';
import FastImage from 'react-native-fast-image';

class Category extends React.Component {
    constructor(props) {
        super(props);

        this.handleOnPress = this.handleOnPress.bind(this);
    }


    handleOnPress() {

        const {item, onPressItem} = this.props;
        //console.log('federder category');
        setTimeout(function () {
            this.props.onPressItem(item);

        }.bind(this), AppStyle.timeOut);

    }

    render() {
        const {item} = this.props;
        return (

            <TouchableOpacity onPress={this.handleOnPress} style={[style.item]}>

                <View style={[style.item]}>
                    <ImageBackground
                        style={style.ImageBackground}
                        source={{
                            uri: item.image_url  ? item.image_url : 'https://static.thenounproject.com/png/220984-200.png',
                            priority: FastImage.priority.high,
                        }}
                    >
                        <View style={[style.itemInside, AppStyle.CenterLeftContent, {flex: 1}]}>
                            <Text
                                style={[typography.Text16penSansRegular, style.TitleStyle, AppStyle.FontWhite]}>{item.name}</Text>
                        </View>
                    </ImageBackground>

                </View>
            </TouchableOpacity>
        );
    }
}


const style = StyleSheet.create({
    item: {
        flex: 0.49,
        marginTop: wp('2%'),
    },
    itemInside: {
        borderRadius: 5,
    },
    ImageBackground: {
        flex: 1,
        height: 105,
        borderRadius: 5,
        overflow: 'hidden',
    },
    TitleStyle: {
        marginEnd: wp('3%'),
    },

});

export default Category;
