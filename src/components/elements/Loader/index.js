import React from 'react';
import {
    ImageBackground,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
    StyleSheet, ActivityIndicator,
} from 'react-native';
import {hp, wp, typography, colors, AppStyle} from './../../../all';

class Loader extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {
        const {item} = this.props;
        return (
            <View style={{flex: 1, backgroundColor:'transparent'}}>
                <ActivityIndicator style={{

                    flex: 1,
                    opacity: !this.props.didFinishInitialAnimation ? 1.0 : 0.0,
                }}
                                   animating={true}
                                   size="large"/>
            </View>
        );
    }
}


const style = StyleSheet.create({});

export default Loader;
