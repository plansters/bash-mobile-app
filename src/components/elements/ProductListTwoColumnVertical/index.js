import React from 'react';
import {
    StyleSheet,
    FlatList,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native';
import {Category, hp, wp, typography, colors, Product, Reactron, localize} from './../../../all';

class ProductListTwoColumnVertical extends React.Component {
    constructor(props) {
        super(props);

        this.onPressItem = this.onPressItem.bind(this);
        this.renderItem = this.renderItem.bind(this);

        ////console.log('=================');
        ////console.log(this.props);
    }


    onPressItem(id) {

    }


    renderItem({item}) {

        return (
            <Product
                {...this.props}
                item={item}
                home={this.props.home}
                onPress={this.onPressItem}
            />
        );
    }


    _listEmptyComponent = () => {
        return (<Text style={[typography.Text16Light, {
            textAlign: 'center',
            padding: wp('3%'),
        }]}>{localize.t('empty_result')}</Text>);
    };

    render() {
        const {data, home} = this.props;

        ////console.log(data);
        return (
            <View>

                <FlatList
                    ListEmptyComponent={data != null && data.length == 0 && !this.props.isLoadingAction ? this._listEmptyComponent : null}
                    home={home}
                    numColumns={2}
                    // style={{paddingTop: home ? 0 : wp("15")}}
                    data={data}
                    columnWrapperStyle={style.row}
                    extraData={this.state}
                    keyExtractor={item => item.id + ''}
                    renderItem={this.renderItem}
                    showsHorizontalScrollIndicator={false}
                    onEndReachedThreshold={0.01}
                    onEndReached={this.props.handleLoadMore}
                    refreshing={this.props.isLoadingAction}
                    onRefresh={this.props.handleRefresh}

                />
            </View>

        );
    }
}

const style = StyleSheet.create({});
export default ProductListTwoColumnVertical;
