import React from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import {colors, typography, AppStyle, wp, hp, localize, EyeUnActive, EyeActive} from './../../../all';

const TextUnderLine = ({
                           onPress,
                           text,
                           style,
                           textStyle,
                           ifHome,
                       }) => (
    <TouchableOpacity style={[styles.TextContainer, {height: ifHome ? wp('5%') : 'auto'}]}
                      onPress={onPress}>
        <View style={[style, styles.underlineTextContainer]}>
            <Text style={
                textStyle === 'Medium' ?
                    [typography.Text12Medium, AppStyle.FontBlackDark] :
                    [typography.Text12Regular, AppStyle.FontBlackDark]


            }>{text}</Text>
        </View>
    </TouchableOpacity>
);

const styles = {
    underlineTextContainer: {
        borderBottomWidth: 1,
        borderColor: colors.Black, alignSelf: 'flex-start',
    },
    TextContainer: {
        paddingHorizontal: wp('2%'),
        paddingVertical: wp('3%'),
    },
};
export default TextUnderLine;
