import React from 'react';
import {View, TouchableOpacity, Text, TextInput} from 'react-native';
import {typography, AppStyle, wp, hp, localize, EyeUnActive, EyeActive} from './../../../all';

const InputElement = ({
                          sizeRow,
                          text,
                          secureTextEntry,
                          onClick,
                          type,
                          disabled,
                          buttonType,
                          onChangeText,
                          multiline, numberOfLines,
                          value,
                          keyboardType,
                          forCheckout,
                          onPressShow,
                          textLength,
                          placeholder,
                      }) => (
    <View size={sizeRow ? sizeRow : 1} style={[styles.FlexDirectionColumn, forCheckout ? styles.CustomMargin : null]}>
        {forCheckout ?
            <Text style={
                [typography.Text16Light]
            }>{text}</Text>
            :
            <Text style={
                [typography.Text14OpenSansRegular, AppStyle.opacity05]
            }>{text}</Text>}

        <TextInput
            placeholder={placeholder}
            keyboardType={keyboardType ? 'numeric' : 'default'}
            multiline={multiline} numberOfLines={numberOfLines} value={value} onChangeText={onChangeText}
            secureTextEntry={secureTextEntry} style={[typography.Text14OpenSansRegular, AppStyle.input]}/>

        {type == 'password' && textLength > 0 ?
            <TouchableOpacity onPress={onPressShow} style={{
                marginTop: -wp('8%'),
                textAlign: 'right',
                alignSelf: 'flex-end',
            }}>{secureTextEntry ? <EyeActive/> : <EyeUnActive/>}</TouchableOpacity> : null}
    </View>
);

const styles = {

    FlexDirectionColumn: {

        flex: 1,
        flexDirection: 'column',

    },
    CustomMargin: {
        marginBottom: wp('4%'),

    },
};

export default InputElement;
