import React from 'react';
import {
    ImageBackground,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
    StyleSheet,
} from 'react-native';
import {hp, wp, typography, colors, AppStyle} from './../../../all';

class CategoryForCollapse extends React.Component {
    constructor(props) {
        super(props);

        this.handleOnPress = this.handleOnPress.bind(this);
    }



    handleOnPress() {
        const {item,onPressItem} = this.props;
        //console.log("categoryyyy");
        setTimeout(function () {
            this.props.onPressItem(item)

        }.bind(this), AppStyle.timeOut);
    }

    render() {
        const {item} = this.props;
        ////console.log("item.sub_categoriesitem.sub_categories")
        ////console.log(item)

        return (


            <TouchableOpacity style={[styles.CenterContent, styles.itemColor]}
                              onPress={this.handleOnPress}>


                <Text style={[typography.Text14Light
                    , {
                        color: 'black',
                        textAlign: 'center',
                    }]}> {item.name}</Text>


            </TouchableOpacity>
        );
    }
}


const styles = StyleSheet.create({

    CenterContent: {
        alignItems: 'center', justifyContent: 'center'
    },
    itemColor: {

        alignItems: 'center',
        justifyContent: 'center',
        marginEnd: hp("1%"),
        marginBottom: hp("1%"),
        paddingStart: hp("2%"),
        paddingEnd: hp("2%"),
        paddingTop: hp("1%"),
        paddingBottom: hp("1%"),
        borderRadius: 5,
        // width:wp("20%"),
        backgroundColor: colors.white,
        borderWidth: 1,
        borderColor: colors.CustomBlack
    }
    ,
    selectedColor: {
        backgroundColor: colors.yellow,
        borderWidth: 0
    }
});

export default CategoryForCollapse;
