import React from 'react';
import {
    StyleSheet,
    FlatList,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native';
import {
    ProductAttributesVariable,
    ProductAttributesListVariable,
    Tabs,
    hp,
    wp,
    typography,
    colors,
} from './../../../all';

class ProductAttributes extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

        this.changeSelectedTab = this.changeSelectedTab.bind(this);
    }

    changeSelectedTab(el) {

        //console.log("fs");
        //console.log(el);
        this.props.updateSelectedTab(el.props.name);

    }

    render() {
        const {data, selectedTab, selectedVariable} = this.props;
        //console.log(data);
        return (
            <View style={{
                justifyContent: 'center',
                alignItems: 'center',
                flex: 1,
            }}>
                <Tabs horizontal={true} selected={this.props.selectedTab}
                      style={{backgroundColor: 'white', borderWidth: 1, borderColor: colors.GrayBorder}}
                      selectedStyle={{color: colors.yellow}}
                      onSelect={el => {
                          this.changeSelectedTab(el);
                      }}>

                    {data.map((e, index) => {

                        return <Text   key={index+""} style={[styles.title]} name={'Attr' + e.attribute_id}>{e.attribute_name}</Text>;
                    })}

                </Tabs>

                <View style={[styles.containerAttr]}>
                    {data.map((e, index) => {
                        return selectedTab === 'Attr' + e.attribute_id ?
                            <ProductAttributesListVariable
                                key={index+""}
                                selectVariant={this.props.selectVariant}
                                selectedVariable={selectedVariable}
                                                           variables={e.values}/> : null;
                    })}
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({

    title: {
        ...typography.Text16penSansRegular,

    },
    containerAttr: {

        justifyContent:'center',
        paddingTop: wp('6%'),
        paddingBottom: wp('8%'),
        width: wp('100%'),
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        flex: 1,

    },

});
export default ProductAttributes;
