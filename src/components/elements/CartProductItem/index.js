import React from 'react';
import {
    ImageBackground,
    Image,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
    StyleSheet, Platform,
} from 'react-native';
import {Touchable, FastImage, hp, wp, typography, colors, AppStyle, localize} from './../../../all';
import NumberFormat from 'react-number-format';

class CartProductItem extends React.Component {
    constructor(props) {
        super(props);

        this.handleOnPress = this.handleOnPress.bind(this);
        this.removeItem = this.removeItem.bind(this);
        this.plusItem = this.plusItem.bind(this);
        this.minusItem = this.minusItem.bind(this);
    }


    handleOnPress() {
        const {onPress, home} = this.props;
        //console.log(this.props);
    }

    removeItem() {
        const {item, home, type} = this.props;

        this.props.removeItem(item.product);

    }

    plusItem() {
        const {item, home, type} = this.props;

        this.props.plusItem(item.product);

    }

    minusItem() {
        const {item, home, type} = this.props;

        this.props.minusItem(item.product);

    }

    render() {
        const {item, home, type} = this.props;


        const type_for_use = type === 'order_details';
        ////console.log('home' + home);
        return (

            <TouchableWithoutFeedback onPress={this.handleOnPress}>

                <View style={[style.ItemContainer]}>

                    <FastImage
                        style={[style.ItemImage]}
                        resizeMode={FastImage.resizeMode.contain}
                        source={{
                            uri: Array.isArray(item.product.product_images) && item.product.product_images.length > 0 ? item.product.product_images[0].media_url : 'https://static.thenounproject.com/png/220984-200.png',
                            priority: FastImage.priority.high,
                        }}/>
                    <View style={[style.itemDescription]}>
                        <Text style={[style.productName]}>{item.product.name}</Text>
                        <Text
                            style={[style.productPrice]}>{item.product.unit_discount_price != null && parseInt(item.product.unit_discount_price) > 0 ? item.product.unit_discount_price : item.product.unit_price} {item.product.currency}</Text>



                        {!type_for_use && item.product && item.product.product_attributes.length > 0 ?
                            item.product.product_attributes.map((e) => {

                                return <Text key={e.value+""} style={[style.productName]}>{e.attribute_name}: <Text
                                    style={[style.productPrice]}>{e.value}</Text></Text>;

                            }) : null}

                        {!type_for_use ?

                            <View style={[style.AddMinusContain, {}]}>

                                <TouchableOpacity onPress={this.plusItem} style={{padding: 12}}>
                                    <FastImage
                                        style={[style.ItemImageIcon]}
                                        resizeMode={FastImage.resizeMode.contain}
                                        source={require('./../../../assets/icons/plus.png')}
                                    />
                                </TouchableOpacity>
                                <Touchable>
                                    <Text style={[typography.Text18OpenSansRegular]}>{item.quantity}</Text>
                                </Touchable>
                                <TouchableOpacity onPress={this.minusItem} style={{padding: 12}}>
                                    <FastImage
                                        style={[style.ItemImageIcon]}
                                        resizeMode={FastImage.resizeMode.contain}
                                        source={require('./../../../assets/icons/minus.png')}
                                    />
                                </TouchableOpacity>
                            </View> : <>

                                <Text style={[style.productName]}>{localize.t('count')}: <Text
                                    style={[style.productPrice]}>{item.quantity}</Text></Text>

                                {item.attribute_details.map((e) => {

                                    return <Text key={e.value+""} style={[style.productName]}>{e.attribute_name}: <Text
                                        style={[style.productPrice]}>{e.value}</Text></Text>;

                                })}

                            </>}

                    </View>
                    {!type_for_use ? <View style={[style.removeItem]}>
                        <TouchableOpacity style={{padding: wp('2%')}} onPress={this.removeItem}>
                            <FastImage
                                style={[style.ItemImageIconClose]}
                                resizeMode={FastImage.resizeMode.contain}
                                source={require('./../../../assets/icons/close.png')}
                            />
                        </TouchableOpacity>
                    </View> : <></>}

                </View>

            </TouchableWithoutFeedback>
        );
    }
}


const style = StyleSheet.create({
    ItemContainer: {
        borderWidth: 1,
        borderColor: colors.GrayBorder,
        borderRadius: 4,
        flexDirection: 'row',
        marginHorizontal: wp('4%'),
        marginVertical: wp('2%'),
        //
        // backgroundColor: 'green',
        alignItems: 'center',
        ...Platform.select({
            ios: {
                shadowColor: colors.GrayBorder,
                shadowOpacity: 1,
                shadowRadius: 3,
                shadowOffset: {
                    height: 3,
                    width: 1,
                },
                paddingTop: hp('1.5%'),
                paddingBottom: hp('1.5%'),
                paddingStart: hp('1.5%'),
            },

            android: {
                shadowColor: colors.GrayBorder,
                paddingTop: hp('1.5%'),
                paddingBottom: hp('1.5%'),
                paddingStart: hp('1.5%'),
            },
        }),
    },
    ItemImage: {
        height: 115,
        width: 115,
    }, ItemImageIcon: {
        height: 12,
        width: 12,
    }, ItemImageIconClose: {
        height: 16,
        width: 16,
    },
    itemDescription: {
        justifyContent: 'space-around',
        alignItems: 'flex-start',
        flex: 1,
        marginHorizontal: wp('2%'),
    },
    productName: {
        ...typography.Text14OpenSansRegular,
    }, productPrice: {
        ...AppStyle.FontGreen,
        ...typography.Text16penSansRegular,
    },
    AddMinusContain: {
        ...AppStyle.CenterContent,
        justifyContent: 'space-between',
        width: wp('35%'),
        flexDirection: 'row',
        backgroundColor: colors.GrayBorder,
        paddingHorizontal: wp('2%'),
        paddingVertical: wp('0.5%'),
    },
    removeItem: {
        padding: wp('1%'),
        // backgroundColor:'red',
        position: 'absolute',
        top: 0,
        right: 1,
    },
});

export default CartProductItem;
