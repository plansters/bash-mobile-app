import React, {useEffect} from 'react';
import AppNavigator from './navigation/';
import store from './store';
import {Alert, I18nManager, Platform} from 'react-native';
import {Provider as StoreProvider} from 'react-redux';
import {enableScreens} from 'react-native-screens';
import SideMenu from 'react-native-side-menu';
import firebase from 'react-native-firebase';
// import type {Notification, NotificationOpen} from 'react-native-firebase';

I18nManager.allowRTL(true);
enableScreens();

class App extends React.Component {

    constructor(props) {
        super(props);

    }


    componentDidMount() {


        console.log("notific111");
        // const channel = new firebase.notifications.Android.Channel(
        //     'MyNewChangle',
        //     'Etloob',
        //     firebase.notifications.Android.Importance.Max
        // ).setDescription('A natural description of the channel');
        // firebase.notifications().android.createChannel(channel);
        //

        // the listener returns a function you can use to unsubscribe
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            console.log("notific2222");

            if (Platform.OS === 'android') {
                console.log("notific33333");
                console.log(notification);


                const localNotification = new firebase.notifications.Notification({
                    // sound: 'sampleaudio',
                    show_in_foreground: true,
                })
                // .setSound('sampleaudio.wav')
                    .setNotificationId(notification.notificationId)
                    .setTitle(notification.title)
                    .setBody(notification.body)
                    .android.setChannelId('fcm_FirebaseNotifiction_default_channel') // e.g. the id you chose above
                    // .android.setSmallIcon('@drawable/ic_launcher') // create this icon in Android Studio
                    .android.setColor('#000000') // you can set a color here
                    .android.setPriority(firebase.notifications.Android.Priority.High);

                firebase.notifications()
                    .displayNotification(localNotification)
                    .catch(err => console.error(err));



                // Alert.alert(notification.title, notification.body)

                setTimeout(function () {
                    Alert.alert(notification.title, notification.body)
                }.bind(notification), 5000)


            } else if (Platform.OS === 'ios') {
                console.log("notific555");

                const localNotification = new firebase.notifications.Notification()
                    .setNotificationId(notification.notificationId)
                    .setTitle(notification.title)
                    .setSubtitle(notification.subtitle)
                    .setBody(notification.body)
                    .setData(notification.data)
                    .ios.setBadge(notification.ios.badge);

                firebase.notifications()
                    .displayNotification(localNotification)
                    .catch(err => console.error(err));

                // Alert.alert(notification.title, notification.body)
                setTimeout(function () {
                    Alert.alert(notification.title, notification.body)
                }.bind(notification), 5000)

            }
        });


        const channel = new firebase.notifications.Android.Channel('fcm_FirebaseNotifiction_default_channel', 'Demo app name', firebase.notifications.Android.Importance.High)
            .setDescription('Demo app description')
            .setSound('sampleaudio.wav');
        firebase.notifications().android.createChannel(channel);

        /*
        * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
        * */
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            const {title, body} = notificationOpen.notification;
            console.log('onNotificationOpened:');
            Alert.alert(title, body)
        });


        this.messageListener = firebase.messaging().onMessage((message) => {
            //process data message
            console.log("JSON.stringify:", JSON.stringify(message));
            console.log(message)

            if (Platform.OS === 'android') {

                console.log("notific33344443");
                console.log(message);


                const localNotification = new firebase.notifications.Notification({
                    // sound: 'sampleaudio',
                    show_in_foreground: true,
                })
                // .setSound('sampleaudio.wav')
                    .setNotificationId(message.messageId)
                    .setTitle(message.data.title)
                    .setBody(message.data.body)
                    .android.setChannelId('fcm_FirebaseNotifiction_default_channel') // e.g. the id you chose above
                    // .android.setSmallIcon('@drawable/ic_launcher') // create this icon in Android Studio
                    .android.setColor('#000000') // you can set a color here
                    .android.setPriority(firebase.notifications.Android.Priority.High);

                firebase.notifications()
                    .displayNotification(localNotification)
                    .catch(err => console.error(err));


                // Alert.alert(message.data.title, message.data.body)

                setTimeout(function () {
                    Alert.alert(message.data.title, message.data.body)
                }.bind(message), 5000)

            } else if (Platform.OS === 'ios') {
                console.log("notific555");

                // const localNotification = new firebase.notifications.Notification()
                //     .setNotificationId(message.messageId)
                //     .setTitle(message.data.title)
                //     .setSubtitle(message.data.body)
                //     .setBody(message.data.body)
                //     .setData(message.data)
                //     .ios.setBadge(notification.ios.badge);
                //
                // firebase.notifications()
                //     .displayNotification(localNotification)
                //     .catch(err => console.error(err));


                setTimeout(function () {
                    Alert.alert(message.data.title, message.data.body)
                }.bind(message), 5000)


            }


        });


    }

    componentWillUnmount() {


        this.notificationListener();
    }



    //
    // async componentDidMount() {
    //     this.messageListener(); //add this line
    // }
    //
    // componentWillUnmount() {
    //
    //
    //     this.messageListener(); //add this line
    //
    //
    // }
    //
    // async messageListener() {
    //     console.log('nottttttttt');
    //     this.notificationListener = firebase.notifications().onNotification((notification) => {
    //         const {title, body} = notification;
    //         console.log('nottttttttt1111');
    //
    //         this.showAlert(title, body);
    //     });
    //
    //     this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
    //         const {title, body} = notificationOpen.notification;
    //         console.log('nottttttttt22222');
    //
    //         this.showAlert(title, body);
    //     });
    //
    //     const notificationOpen = await firebase.notifications().getInitialNotification();
    //     if (notificationOpen) {
    //         console.log('nottttttttt3333');
    //
    //         const {title, body} = notificationOpen.notification;
    //         this.showAlert(title, body);
    //     }
    //
    //     this.messageListener = firebase.messaging().onMessage((message) => {
    //         console.log('nottttttttt44444');
    //
    //         console.log(JSON.stringify(message));
    //     });
    // }

    showAlert(title, body) {
        Alert.alert(
            title, body,
            [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false},
        );
    }

    render() {

        console.disableYellowBox = true;
        return (
            <StoreProvider store={store}>
                <AppNavigator/>

            </StoreProvider>

        );
    }


}

export default App;
