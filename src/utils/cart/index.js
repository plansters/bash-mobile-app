import {NAVIGATION_APP, NAVIGATION_DRAWER} from '../../navigation/types';
import {Reactron} from '../../all';

export default cartManagement => ({

    minusItemCart(item) {
        global.storage.load({
            key: 'cart',
            autoSync: true,
            syncInBackground: true,

        })
            .then(ret => {
                ////////console.log(ret)


                var array_index = ret.line_items.reduce(function (a, e, i) {
                    if (e.product_id === item.id) {
                        a.push(i);
                    }
                    return a;
                }, []);

                array_index.map((e, index) => {

                    if (JSON.stringify(ret.line_items[e].variant) === JSON.stringify(item.product_attributes)) {

                        if (ret.line_items[e].quantity > 1) {
                            ret.line_items[e].quantity = ret.line_items[e].quantity - 1;


                        }


                    }
                });

                // if (ret.line_items.findIndex(x => x.product_id === item.id) !== -1) {
                //
                //     var index = ret.line_items.findIndex(x => x.product_id === item.id);
                //
                //     if (ret.line_items[index].quantity > 1) {
                //         ret.line_items[index].quantity = ret.line_items[index].quantity - 1;
                //
                //
                //     }
                //
                // }

                ////////console.log("before save remove item")

                global.cart = ret;

                global.storage.save({
                    key: 'cart',
                    data: ret,
                });

            })
            .catch(err => {

                ////////console.log("fff");
                console.warn(err.message);
                switch (err.name) {
                    case 'NotFoundError':

                        break;
                    case 'ExpiredError':

                        break;
                }
            });

    },

    plusItemCart(item) {

        //console.log('add item plus');
        //console.log(item);
        global.storage.load({
            key: 'cart',
            autoSync: true,
            syncInBackground: true,

        })
            .then(ret => {
                //console.log('ret');
                //console.log(ret);


                var array_index = ret.line_items.reduce(function (a, e, i) {
                    if (e.product_id === item.id) {
                        a.push(i);
                    }
                    return a;
                }, []);

                array_index.map((e, index) => {

                    if (JSON.stringify(ret.line_items[e].variant) === JSON.stringify(item.product_attributes)) {
                        ret.line_items[e].quantity = ret.line_items[e].quantity + 1;

                    }
                });

                //
                // if (ret.line_items.findIndex(x => x.product_id === item.id) !== -1) {
                //
                //     var index = ret.line_items.findIndex(x => x.product_id === item.id);
                //     ret.line_items[index].quantity = ret.line_items[index].quantity + 1;
                //
                // }


                global.cart = ret;

                global.storage.save({
                    key: 'cart',
                    data: ret,
                });

            })
            .catch(err => {

                ////////console.log("fff");
                console.warn(err.message);
                switch (err.name) {
                    case 'NotFoundError':

                        break;
                    case 'ExpiredError':

                        break;
                }
            });

    },
    removeItemCart(item) {


        global.storage.load({
            key: 'cart',
            autoSync: true,
            syncInBackground: true,

        })
            .then(ret => {


                var array_index = ret.line_items.reduce(function (a, e, i) {
                    if (e.product_id === item.id) {
                        a.push(i);
                    }
                    return a;
                }, []);

                array_index.map((e, index) => {

                    if (JSON.stringify(ret.line_items[e].variant) === JSON.stringify(item.product_attributes)) {
                        ret.line_items.splice(e, 1);
                    }
                });

                // if (ret.line_items.findIndex(x => x.product_id === item.id) !== -1) {
                //     var index = ret.line_items.findIndex(x => x.product_id === item.id);
                //
                //     ret.line_items.splice(index, 1);
                // }


                global.cart = ret;

                global.storage.save({
                    key: 'cart',
                    data: ret,
                });

            })
            .catch(err => {

                ////////console.log("fff");
                console.warn(err.message);
                switch (err.name) {
                    case 'NotFoundError':

                        break;
                    case 'ExpiredError':

                        break;
                }
            });


    },
    addItemToCart(item) {

        //console.log('add to cart action Cart Management');
        ////console.log(item);

        global.storage.load({
            key: 'cart',
            autoSync: true,
            syncInBackground: true,

        })
            .then(ret => {

                //console.log(ret.line_items);
                //console.log(item);

                if (ret.line_items != null && ret.line_items.length > 0) {
                    var array_index = ret.line_items.reduce(function (a, e, i) {
                        if (JSON.stringify(e.variant) === JSON.stringify(item.product_attributes)) {
                            a.push(i);
                        }
                        return a;
                    }, []);
                    //console.log(array_index);

                    if (array_index.length > 0) {

                        var e = array_index[0];
                        //console.log('index add');
                        //console.log(e);

                        ret.line_items[e].quantity = ret.line_items[e].quantity + 1;

                    }
                    else {
                        //console.log('index add not found');
                        //console.log(e);
                        ret.line_items.push({
                            product_id: item.id,
                            quantity: 1,
                            variant: item.product_attributes,
                            product: item,
                        });
                    }

                }
                else {
                    //console.log('index add in 2');

                    ret.line_items.push({
                        product_id: item.id,
                        quantity: 1,
                        variant: item.product_attributes,
                        product: item,
                    });

                }
                //console.log('index add out');

                // if (ret.line_items.findIndex(x => x.product_id === item.id) !== -1) {
                //
                //     var index = ret.line_items.findIndex(x => x.product_id === item.id);
                //     ret.line_items[index].quantity = ret.line_items[index].quantity + 1;
                //
                // }
                // else {
                //
                //     ret.line_items.push({
                //         product_id: item.id,
                //         quantity: 1,
                //         variant: item.product_attributes,
                //         product: item,
                //     });
                //
                // }


                global.storage.save({
                    key: 'cart',
                    data: ret,
                });
                ////console.log('after save1');
                ////console.log(ret);

                global.cart = ret;

            })
            .catch(err => {

                //console.log('fff');
                //console.log(err.message);
                switch (err.name) {
                    case 'NotFoundError':

                        const cartContaint = {
                            line_items: [],
                        };

                        cartContaint.line_items.push({
                            product_id: item.id,
                            quantity: 1,
                            variant: item.product_attributes,
                            product: item,
                        });


                        global.storage.save({
                            key: 'cart',
                            data: cartContaint,
                        });
                        global.cart = cartContaint;
                        ////console.log('after save');

                        ////console.log(global.cart);

                        break;
                    case 'ExpiredError':

                        break;
                }
            });

    },


    addItemToFavorite(item) {

        ////////console.log("add to cart action Cart Management")
        ////////console.log(item)

        global.storage.load({
            key: 'favorite',
            autoSync: true,
            syncInBackground: true,

        })
            .then(ret => {
                //////console.log(ret)

                if (ret.findIndex(x => x.id === item.id) !== -1) {
                    var index = ret.findIndex(x => x.id === item.id);
                    ret.splice(index, 1);

                }
                else {

                    ret.push(item);

                }


                //////console.log("before save")
                global.storage.save({
                    key: 'favorite',
                    data: ret,
                });
                //////console.log("after save1")

                //////console.log(ret)

                global.favorite = ret;

            })
            .catch(err => {

                ////////console.log("fff");
                console.warn(err.message);
                switch (err.name) {
                    case 'NotFoundError':

                        const Favorites = [];

                        Favorites.push(item);

                        //////console.log("before save")
                        global.storage.save({
                            key: 'favorite',
                            data: Favorites,
                        });
                        global.favorite = Favorites;
                        //////console.log(global.favorite)
                        //////console.log("after save")

                        break;
                    case 'ExpiredError':

                        break;
                }
            });

    },

    getCart() {

        ////////console.log("insede get cart ")
        ////////console.log(global.cart)

        return global.cart;


    },
    getFavorite() {

        //////console.log("insede get cart ")
        //////console.log(global.favorite)

        return global.favorite


    }
})
