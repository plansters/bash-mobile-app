import {createAppContainer, createStackNavigator} from 'react-navigation';

import {
    LoginScreen,
    WelcomeScreen, SignUpScreen,
} from '../components/pages/';
import {
    NAVIGATION_CART_SCREEN, NAVIGATION_CATEGORY_PRODUCTS_SCREEN,
    NAVIGATION_CATEGORY_SCREEN, NAVIGATION_COMPLAINS_SCREEN, NAVIGATION_EDIT_PROFILE_SCREEN,
    NAVIGATION_HOME2_SCREEN,
    NAVIGATION_HOME_SCREEN,
    NAVIGATION_LOGIN_SCREEN,
    NAVIGATION_OFFER_SCREEN, NAVIGATION_PRODUCT_SCREEN, NAVIGATION_PROFILE_SCREEN,
    NAVIGATION_SEARCH_SCREEN,
    NAVIGATION_SIGNUP_SCREEN,
    NAVIGATION_WELCOME_SCREEN,

} from './types';
import HomeScreen from '../components/pages/HomeScreen';
import SearchScreen from '../components/pages/SearchScreen';
import OffersScreen from '../components/pages/OffersScreen';
import HomeScreen2 from '../components/pages/HomeScreen2';
import CategoryScreen from '../components/pages/CategoryScreen';
import CartScreen from '../components/pages/CartScreen';
import ProductDetailsScreen from '../components/pages/ProductDetailsScreen';
import CategoryProductsScreen from '../components/pages/CategoryProductsScreen';
import ComplainsScreen from '../components/pages/ComplainsScreen';
import ProfileScreen from '../components/pages/ProfileScreen';
import EditProfileScreen from '../components/pages/EditProfileScreen';


const AppPagesNavigator = createStackNavigator(
    {
        [NAVIGATION_WELCOME_SCREEN]: WelcomeScreen,
        [NAVIGATION_LOGIN_SCREEN]: LoginScreen,
        [NAVIGATION_SIGNUP_SCREEN]: SignUpScreen,


    },
    {

        defaultNavigationOptions: {
            cardOverlayEnabled: true,
            gestureEnabled: true,
        },
        transitionSpec: {
            duration: 100,
            useNativeDriver: true,
        },
        headerMode: 'none',
        navigationOptions: {
            headerVisible: false,
        },
    },
);

const AppPAgesNavigatorCreator = createAppContainer(AppPagesNavigator);

export default AppPAgesNavigatorCreator;
