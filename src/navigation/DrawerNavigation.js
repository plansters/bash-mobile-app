import {createAppContainer, createDrawerNavigator, createStackNavigator} from 'react-navigation';

import {
    ComplainsScreen,
    CategoryProductsScreen,
    ProductDetailsScreen,
    HomeScreen,
    CartScreen,
    HomeScreen2,
    CategoryScreen,
    SearchScreen,
    OffersScreen,
    ProfileScreen,
    EditProfileScreen, MyOrdersScreen,
    MyOrderDetailsScreen,
    AboutScreen,SearchProductsScreen
} from '../components/pages/';
import {SideMenu} from '../components/template/';
import {I18nManager} from 'react-native';
import {
    NAVIGATION_HOME2_SCREEN,
    NAVIGATION_HOME_SCREEN,
    NAVIGATION_SEARCH_SCREEN,
    NAVIGATION_OFFER_SCREEN,
    NAVIGATION_CATEGORY_SCREEN,
    NAVIGATION_CART_SCREEN,
    NAVIGATION_PRODUCT_SCREEN,
    NAVIGATION_CATEGORY_PRODUCTS_SCREEN,
    NAVIGATION_COMPLAINS_SCREEN,
    NAVIGATION_PROFILE_SCREEN,
    NAVIGATION_EDIT_PROFILE_SCREEN,
    NAVIGATION_MYORDERS_SCREEN,
    NAVIGATION_MYORDER_DETAILS_SCREEN,
    NAVIGATION_ABOUT_SCREEN,
    NAVIGATION_CHECKOUT_SCREEN,
    NAVIGATION_SEARCH_PRODUCTS_SCREEN,
} from './types';
import {wp, hp} from './../all';
import {enableScreens, useScreens} from 'react-native-screens';
import {Platform} from 'react-native';
import CheckoutScreen from '../components/pages/CheckoutScreen';

enableScreens();
const MainAppNavigator = createStackNavigator({


        [NAVIGATION_HOME_SCREEN]: HomeScreen,
        [NAVIGATION_SEARCH_SCREEN]: SearchScreen,
        [NAVIGATION_OFFER_SCREEN]: OffersScreen,
        [NAVIGATION_HOME2_SCREEN]: HomeScreen2,
        [NAVIGATION_CATEGORY_SCREEN]: CategoryScreen,
        [NAVIGATION_CART_SCREEN]: CartScreen,
        [NAVIGATION_CATEGORY_PRODUCTS_SCREEN]: CategoryProductsScreen,
        [NAVIGATION_COMPLAINS_SCREEN]: ComplainsScreen,
        [NAVIGATION_PROFILE_SCREEN]: ProfileScreen,
        [NAVIGATION_EDIT_PROFILE_SCREEN]: EditProfileScreen,
        [NAVIGATION_MYORDERS_SCREEN]: MyOrdersScreen,
        [NAVIGATION_MYORDER_DETAILS_SCREEN]: MyOrderDetailsScreen,
        [NAVIGATION_ABOUT_SCREEN]: AboutScreen,
        [NAVIGATION_PRODUCT_SCREEN]: ProductDetailsScreen,
        [NAVIGATION_CHECKOUT_SCREEN]: CheckoutScreen,
        [NAVIGATION_SEARCH_PRODUCTS_SCREEN]: SearchProductsScreen,

    }, {


        defaultNavigationOptions: {
            cardOverlayEnabled: true,
            gestureEnabled: true,
        },
        transitionSpec: {
            duration: 100,
            useNativeDriver: true,
        },
        headerMode: 'none',
        navigationOptions: {
            headerVisible: false,
        },
    },
);


const DrawerNavigator = createDrawerNavigator({
        MainAppNavigator,
    },
    {
        contentComponent: SideMenu,
        drawerPosition: I18nManager.isRTL ? 'right' : 'left',
        drawerWidth: wp('100%'),

    },
);
const DrawerNavigatorCreator = createAppContainer(DrawerNavigator);

export default DrawerNavigatorCreator;
