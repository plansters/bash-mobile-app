import {createAppContainer} from 'react-navigation';
import AppNavigator from './AppNavigator';

const Navigator = createAppContainer(AppNavigator);

export default Navigator;
