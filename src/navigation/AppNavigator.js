import {createStackNavigator, createSwitchNavigator} from 'react-navigation';

import {
    SplashScreen,
} from '../components/pages';
import {
    NAVIGATION_SPLASH_SCREEN,
    NAVIGATION_APP,
    NAVIGATION_DRAWER, NAVIGATION_ChooseLanguage_SCREEN, NAVIGATION_HOME_SCREEN, NAVIGATION_HOME2_SCREEN,

} from './types';
import AppPAgesNavigatorCreator from './AppPagesNavigator';
// import AppPAgesHomeNavigatorCreator from './AppPagesHomeNavigator';
import DrawerNavigatorCreator from './DrawerNavigation';
import ChooseLanguage from '../components/pages/ChooseLanguage';

const AppNavigator = createSwitchNavigator(
    {
        [NAVIGATION_SPLASH_SCREEN]: SplashScreen,
        [NAVIGATION_APP]: AppPAgesNavigatorCreator,
        // [NAVIGATION_DRAWER]: AppPAgesHomeNavigatorCreator,
        [NAVIGATION_DRAWER]: DrawerNavigatorCreator,
        [NAVIGATION_ChooseLanguage_SCREEN]: ChooseLanguage,


    },
    {

        defaultNavigationOptions: {
            cardOverlayEnabled: true,
            gestureEnabled: true,
        },
        transitionSpec: {
            duration: 100,
            useNativeDriver: true,
        },
        headerMode: 'none',
        navigationOptions: {
            headerVisible: false,
        },
    },
);


export default AppNavigator;
