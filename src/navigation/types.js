export const NAVIGATION_SPLASH_SCREEN = 'SplashScreen';
export const NAVIGATION_APP = 'App';
export const NAVIGATION_DRAWER = 'Drawer';
export const NAVIGATION_DRAWER_VENDOR = 'DrawerVendor';
export const NAVIGATION_WELCOME_SCREEN = 'WelcomeScreen';
export const NAVIGATION_LOGIN_SCREEN = 'LoginScreen';
export const NAVIGATION_SIGNUP_SCREEN = 'SignUpScreen';
export const NAVIGATION_VERIFICATION_SCREEN = 'VerificationScreen';
export const NAVIGATION_HOME_SCREEN = 'HomeScreen';
export const NAVIGATION_HOME2_SCREEN = 'HomeScreen2';
export const NAVIGATION_PRODUCT_SCREEN = 'ProductDetailsScreen';
export const NAVIGATION_SEARCH_SCREEN = 'SearchScreen';
export const NAVIGATION_SEARCH_PRODUCTS_SCREEN = 'SearchProductsScreen';
export const NAVIGATION_CATEGORY_SCREEN = 'CategoryScreen';
export const NAVIGATION_CATEGORY_PRODUCTS_SCREEN = 'CategoryProductsScreen';
export const NAVIGATION_COMPLAINS_SCREEN = 'ComplainsScreen';
export const NAVIGATION_PRODUCTS_SCREEN = 'ProductsScreen';
export const NAVIGATION_CART_SCREEN = 'CartScreen';
export const NAVIGATION_CHECKOUT_SCREEN = 'CheckoutScreen';
export const NAVIGATION_MYORDERS_SCREEN = 'MyOrdersScreen';
export const NAVIGATION_MYORDER_DETAILS_SCREEN = 'MyOrderDetailsScreen';
export const NAVIGATION_FAVORITES_SCREEN = 'FavoriteScreen';
export const NAVIGATION_EDIT_PROFILE_SCREEN = 'EditProfileScreen';
export const NAVIGATION_PROFILE_SCREEN = 'ProfileScreen';
export const NAVIGATION_ABOUTUS_SCREEN = 'AboutUsScreen';
export const NAVIGATION_ChooseLanguage_SCREEN = 'ChooseLanguage';
export const NAVIGATION_VENDORS_SCREEN = 'VendorsScreen';
export const NAVIGATION_VENDORS_PRODUCTS_SCREEN = 'VendorProductScreen';
export const NAVIGATION_SHIPPING_ADDRESS_SCREEN = 'ShippingAddressScreen';
export const NAVIGATION_VENDOR_HOME = 'VendorHome';
export const NAVIGATION_OFFER_SCREEN = 'OffersScreen';
export const NAVIGATION_ABOUT_SCREEN = 'AboutScreen';
