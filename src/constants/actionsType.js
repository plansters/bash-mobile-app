export const REQUEST = 'REQUEST';
export const LOADING = 'LOADING';
export const SUCCESS = 'SUCCESS';
export const FAILURE = 'FAILURE';
export const OPEN_SELECTED_PRODUCT = 'openSelectedProduct';
export const GET_VARIANT_PRODUCT = 'getVariantProduct';
export const CUSTOMER_AUTH_STATE_RESET = 'resetAuth';


export const CUSTOMER_SIGNUP_REQUEST = 'signUpResquest';
export const CUSTOMER_SIGNUP_SUCCESS = 'signUpSuccess';
export const CUSTOMER_SIGNUP_FAIL = 'signUpFail';

export const CUSTOMER_LOGIN_REQUEST = 'loginResquest';
export const CUSTOMER_LOGIN_SUCCESS = 'loginSuccess';
export const CUSTOMER_LOGIN_FAIL = 'loginFail';


export const CUSTOMER_VERIFY_PHONE = 'verifyPhone';
export const CUSTOMER_VERIFY_FAIL = 'verifyPhoneFail';
export const CUSTOMER_VERIFY_SUCCESS = 'verifyPhoneSuccess';


export const HOME_FEATURE_PRODUCTS_LOADING = 'featureProductsLoading';
export const HOME_FEATURE_PRODUCTS = 'featureProducts';
export const HOME_FEATURE_PRODUCTS_SUCCESS = 'featureProductsSuccess';
export const HOME_FEATURE_PRODUCTS_FAIL = 'featureProductsFail';

export const HOME_HOME_CATEGORIES = 'homeHomeCategory';
export const HOME_HOME_CATEGORIES_LOADING = 'homeHomeCategoryLoading';
export const HOME_HOME_CATEGORIES_SUCCESS = 'homeHomeCategorySuccess';
export const HOME_HOME_CATEGORIES_FAIL = 'homeHomeCategoryFail';

export const HOME_OFFERS = 'homeOffers';
export const HOME_OFFERS_LOADING = 'homeOffersLoading';
export const HOME_OFFERS_SUCCESS = 'homeOffersSuccess';
export const HOME_OFFERS_FAIL = 'homeOffersFail';

export const HOME_HOME_SLIDERS = 'homeHomeSlider';
export const HOME_HOME_SLIDERS_LOADING = 'homeHomeSliderLoading';
export const HOME_HOME_SLIDERS_SUCCESS = 'homeHomeSliderSuccess';
export const HOME_HOME_SLIDERS_FAIL = 'homeHomeSliderFail';
export const HOME_UPDATE_COUNT_CART = 'updateCountCart';


export const HOME_RECENT_PRODUCTS_LOADING = 'recentProductsLoading';
export const HOME_RECENT_PRODUCTS = 'recentProducts';
export const HOME_RECENT_PRODUCTS_SUCCESS = 'recentProductsSuccess';
export const HOME_RECENT_PRODUCTS_FAIL = 'recentProductsFail';

export const HOME_TOP_CATEGORIES_LOADING = 'topCategoriesLoading';
export const HOME_TOP_CATEGORIES = 'topCategories';
export const HOME_TOP_CATEGORIES_SUCCESS = 'topCategoriesSuccess';
export const HOME_TOP_CATEGORIES_FAIL = 'topCategoriesFail';

export const HOME_TRENDING_PRODUCTS_LOADING = 'trendingProductsLoading';
export const HOME_TRENDING_PRODUCTS = 'trendingProducts';
export const HOME_TRENDING_PRODUCTS_SUCCESS = 'trendingProductsSuccess';
export const HOME_TRENDING_PRODUCTS_FAIL = 'trendingProductsFail';

export const CATEGORIES = 'categories';
export const CATEGORIES_LOADING = 'categoriesLoading';
export const CATEGORIES_SUCCESS = 'categoriesSuccess';
export const CATEGORIES_FAIL = 'categoriesFail';
export const OPEN_SELECTED_CATEGORY = 'openSelectedCategory';
export const RESET_SELECTED_CATEGORY = 'resetSelectedCategory';

export const VENDORS = 'vendors';
export const VENDORS_LOADING = 'vendorsLoading';
export const VENDORS_SUCCESS = 'vendorsSuccess';
export const VENDORS_FAIL = 'vendorsFail';
export const OPEN_SELECTED_VENDORS = 'openSelectedVendor';
export const REST_SELECTED_VENDORS = 'resetSelectedVendor';

export const PRODUCT_LIST = 'productList';
export const PRODUCT_DURATION_RESET = 'productDuration';
export const PRODUCT_LIST_RESET = 'productListReset';
export const PRODUCT_LIST_LOADING = 'productListLoading';
export const PRODUCT_LIST_SUCCESS = 'productListSuccess';
export const PRODUCT_LIST_FAIL = 'productListFail';

export const CATEGORY_SUB_LIST = 'subCategoryList';
export const CATEGORY_SUB_LIST_RESET = 'subCategoryListReset';
export const CATEGORY_SUB_LIST_LOADING = 'subCategoryListLoading';
export const CATEGORY_SUB_LIST_SUCCESS = 'subCategoryListSuccess';
export const CATEGORY_SUB_LIST_FAIL = 'subCategoryListFail';


export const OFFER_LIST = 'offerList';
export const OFFER_DURATION_RESET = 'offerDuration';
export const OFFER_LIST_RESET = 'offerListReset';
export const OFFER_LIST_LOADING = 'offerListLoading';
export const OFFER_LIST_SUCCESS = 'offerListSuccess';
export const OFFER_LIST_FAIL = 'offerListFail';

export const SEARCH_LIST = 'searchList';
export const SEARCH_DURATION_RESET = 'searchDuration';
export const SEARCH_LIST_RESET = 'searchListReset';
export const SEARCH_LIST_LOADING = 'searchListLoading';
export const SEARCH_LIST_SUCCESS = 'searchListSuccess';
export const SEARCH_LIST_FAIL = 'searchListFail';

export const REVIEW_LIST = 'reviewList';
export const REVIEW_LIST_RESET = 'reviewListReset';
export const REVIEW_LIST_LOADING = 'reviewListLoading';
export const REVIEW_LIST_SUCCESS = 'reviewListSuccess';
export const REVIEW_LIST_FAIL = 'reviewListFail';

export const PRODUCT_VARIANT = 'productVariant';
export const PRODUCT_VARIANT_RESET = 'productVariantReset';
export const PRODUCT_VARIANT_LOADING = 'productVariantLoading';
export const PRODUCT_VARIANT_SUCCESS = 'productVariantSuccess';
export const PRODUCT_VARIANT_FAIL = 'productVariantFail';

export const VENDOR_LIST = 'vendorList';
export const VENDOR_LIST_RESET = 'vendorListReset';
export const VENDOR_LIST_LOADING = 'vendorListLoading';
export const VENDOR_LIST_SUCCESS = 'vendorListSuccess';
export const VENDOR_LIST_FAIL = 'vendorListFail';

export const PRODUCT_LIST_SEARCH = 'productListSearch';
export const PRODUCT_LIST_SEARCH_RESET = 'productListSearchReset';
export const PRODUCT_LIST_SEARCH_LOADING = 'productListSearchLoading';
export const OPEN_SELECTED_SEARCH_WORD = 'openASelectedSearchWord';
export const RESET_SELECTED_SEARCH_WORD = 'resetSelectedSearchWord';
export const PRODUCT_LIST_SEARCH_HSUCCESS = 'productListSearchSuccess';
export const PRODUCT_LIST_SEARCH_FAIL = 'productListSearchFail';
export const PRODUCT_ADD_TO_CART = 'productAddToCart';
export const PRODUCT_ADD_TO_CART_LOADING = 'productAddToCartLoading';
export const PRODUCT_ADD_TO_CART_SUCCESS = 'productAddToCartSUCCESS';
export const PRODUCT_ADD_TO_CART_RESET_FROM_SAGA = 'productAddToCartSUCCESSFROSAGAS';
export const PRODUCT_ADD_TO_CART_FAIL = 'productAddToCartFAIL';

export const PRODUCT_ADD_TO_FAVORITE = 'productAddToFavorite';
export const PRODUCT_ADD_TO_FAVORITE_LOADING = 'productAddToFavoriteLoading';
export const PRODUCT_ADD_TO_FAVORITE_SUCCESS = 'productAddToFavoriteSUCCESS';
export const PRODUCT_ADD_TO_FAVORITE_RESET_FROM_SAGA = 'productAddToFavoriteSUCCESSFROSAGAS';
export const PRODUCT_ADD_TO_FAVORITE_FAIL = 'productAddToFavoriteFAIL';
export const PRODUCT_RELATED = 'productRelated';
export const PRODUCT_RELATED_LOADING = 'productRelatedLoading';
export const PRODUCT_RELATED_SUCCESS = 'productRelatedSuccess';
export const PRODUCT_RELATED_FAIL = 'productRelatedFail';
export const PRODUCT_ADD_TO_CART_RESET = 'productAddToCartReset';
export const PRODUCT_ADD_TO_FAVORITE_RESET = 'productAddToFavoriteReset';

export const CUSTOMER_CART_REQUEST = 'cartRequest';
export const CUSTOMER_CART_RESET = 'cartReset';
export const CUSTOMER_CART_LOADING = 'cartLoading';
export const CUSTOMER_CART_SUCCESS = 'cartSuccess';
export const CUSTOMER_CART_FAIL = 'cartFail';

export const NEW_REQUEST = 'FindRequest';
export const NEW_REQUEST_LOADING = 'FindRequestLoading';
export const NEW_REQUEST_SUCCESS = 'FindRequestSuccess';
export const NEW_REQUEST_FAIL = 'FindRequestFail';
export const NEW_REQUEST_RESET = 'FindRequestReset';


export const CUSTOMER_FAVORITE_REQUEST = 'FavoriteRequest';
export const CUSTOMER_FAVORITE_RESET = 'FavoriteReset';
export const CUSTOMER_FAVORITE_LOADING = 'favoriteLoading';
export const CUSTOMER_FAVORITE_SUCCESS = 'favoriteSuccess';
export const CUSTOMER_FAVORITE_FAIL = 'favoriteFail';

export const PRODUCT_REMOVE_FROM_CART = 'removeProduct';
export const PRODUCT_MINUS_FROM_CART = 'plusProduct';
export const PRODUCT_PLUS_FROM_CART = 'minusProduct';
export const PRODUCT_REMOVE_FROM_CART_LOADING = 'removeProductLoading';
export const PRODUCT_REMOVE_FROM_CART_FAIL = 'removeProductFAil';
export const PRODUCT_REMOVE_FROM_CART_SUCCESS = 'removeProductSUCCESS';

export const PLACE_ORDER_REQUEST = 'placeOrderRequest';
export const PLACE_ORDER_RESET = 'placeOrderReset';
export const PLACE_ORDER_REQUEST_LOADING = 'placeOrderRequestLoading';
export const PLACE_ORDER_REQUEST_SUCCESS = 'placeOrderRequestSUCCESS';
export const PLACE_ORDER_REQUEST_FAIL = 'placeOrderRequestFAIL';

export const OPEN_SELECTED_ORDER = 'openSelectedOrder';
export const ORDER_LIST = 'orderList';
export const ORDER_LIST_RESET = 'orderListReset';
export const ORDER_LIST_LOADING = 'orderListLoading';
export const ORDER_LIST_SUCCESS = 'orderListSuccess';
export const ORDER_LIST_FAIL = 'orderListFail';

export const CANCEL_ORDER = 'cancelOrder';
export const CANCEL_ORDER_LOADING = 'cancelOrderLoading';
export const CANCEL_ORDER_SUCCESS = 'cancelOrderSuccess';
export const CANCEL_ORDER_FAIL = 'cancelOrderFail';
export const CANCEL_ORDER_RESET = 'cancelOrderReset';

export const COMPLAINT = 'complaint';
export const COMPLAINT_LOADING = 'complaintLoading';
export const COMPLAINT_SUCCESS = 'complaintSuccess';
export const COMPLAINT_FAIL = 'complaintFail';
export const COMPLAINT_RESET = 'complaintReset';

export const EDIT_PROFILE = 'editProfile';
export const EDIT_PROFILE_LOADING = 'editProfileLoading';
export const EDIT_PROFILE_SUCCESS = 'editProfileSuccess';
export const EDIT_PROFILE_ERROR = 'editProfileError';
export const EDIT_PROFILE_RESET = 'editProfileRESET';
