import {I18nManager, Platform, StyleSheet} from 'react-native';
import {typography, colors, wp, hp} from './../all';

// noinspection JSAnnotator
export default {

    ////////////////////////////App Style/////////////////////////
    CenterContent: {
        justifyContent: 'center',
        alignItems: 'center',
    }, CenterContentRow: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    CenterBottomContent: {
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    CenterBottomLeftContent: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    CenterBottomRightContent: {
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
    },
    CenterLeftContent: {
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    CenterRightContent: {
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    CenterTopContent: {
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    CenterRightTopContent: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    itemButton: {
        width: wp('65%'),
        alignItems: 'center', justifyContent: 'center',
        marginEnd: hp('1%'),
        marginTop: hp('1%'),
        marginBottom: hp('1%'),
        paddingTop: hp('1.3%'),
        paddingBottom: hp('1.3%'),
        borderRadius: 8,
        backgroundColor: colors.white,
        borderWidth: 1,
        borderColor: colors.CustomBlack,
    },
    itemButtonAutoWidth: {
        alignItems: 'center', justifyContent: 'center',
        marginEnd: hp('1%'),
        marginTop: hp('1%'),
        marginBottom: hp('1%'),
        paddingTop: hp('1.3%'),
        paddingBottom: hp('1.3%'),
        paddingHorizontal:hp("3%"),
        borderRadius: 8,
        backgroundColor: colors.white,
        borderWidth: 1,
        borderColor: colors.CustomBlack,
    },

    CenterText: {
        textAlign: 'center',
    },

    ////////////////////////////App Style/////////////////////////


    ////////////////////////////App Color And font/////////////////////////

    FontBlack: {
        color: colors.CustomBlack,
    },
    FontYellow: {
        color: colors.yellow,
    },
    FontGreen: {
        color: colors.green,
    },
    FontBlackDark: {
        color: colors.Black,
    },

    FontWhite: {
        color: colors.white,
    },
    GradientColorDark: [
        '#3F3F3F',
        '#000000',
    ],
    GradientColor: [
        '#d7b653',
        '#F1D467',
    ],


    ////////////////////////////App Color And font/////////////////////////


    opacity05: {

        opacity: 0.4,
    },
    Yellow: {
        color: colors.yellow,
    },
    input: {
        borderBottomWidth: 1,
        borderColor: '#909090',
        height: wp('10%'),
        textAlign: I18nManager.isRTL ? 'right' : 'left',
        writingDirection: I18nManager.isRTL ? 'rtl' : 'ltr',
    },
    inputBordered: {
        borderWidth: 1,
        borderColor: '#909090',
        height: wp('10%'),
        paddingHorizontal:wp("2%"),
        paddingVertical:wp("2%"),
        borderRadius:4,
        textAlign: I18nManager.isRTL ? 'right' : 'left',
        writingDirection: I18nManager.isRTL ? 'rtl' : 'ltr',
    },
    spinnerText: {
        ...typography.Text30Bold,
        color: 'white',
    },


    timeOut:1,

    stylesPicker : StyleSheet.create({

        viewContainer: {

            // justifyContent:'flex-start',
            // alignItems:'center',

            ...Platform.select({
                ios: {
                    borderWidth: 1,
                    borderRadius: 5,
                    borderColor: colors.Gray,
                    paddingBottom: wp('2%'),
                    paddingTop: wp('2%'),
                    paddingStart: wp('2%'),
                    paddingEnd: wp('2%'),
                    alignItems: 'flex-start',
                },

                android: {
                    borderWidth: 1,
                    borderRadius: 5,
                    borderColor: colors.Gray,
                    // marginBottom: wp('3%'),
                    height: hp("5%"),
                    justifyContent:'center',
                    // alignItems: 'flex-start',

                },
            }),
        },

    }),

    stylesPickerSearch : StyleSheet.create({

        viewContainer: {


            ...Platform.select({
                ios: {
                    borderWidth: 1,
                    borderRadius: 5,
                    borderColor:colors.Gray,
                    paddingBottom: wp('2%'),
                    paddingTop: wp('2%'),
                    paddingStart: wp('2%'),
                    paddingEnd: wp('2%'),
                    alignItems: 'flex-start',
                    width: wp("72%"),
                    marginStart:wp("3%"),
                },

                android: {
                    borderWidth: 1,
                    borderRadius: 5,
                    borderColor: colors.Gray,
                    // marginBottom: wp('3%'),
                    height: hp("5%"),
                    justifyContent:'center',
                    width: wp("75%"),
                    marginStart:wp("3%"),

                },
            }),
        },

        headlessAndroidContainer:{

            alignItems:'center',
            justifyContent:'center',
            backgroundColor:'red',
        }

    }),

};
