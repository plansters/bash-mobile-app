export default {
    CustomBlack: '#42413d',
    Black: '#000',
    Gray: '#e2e2e1',
    GrayTransparent: '#d4d4d4',
    GrayBorder: '#dadad991',
    white: '#fff',
    yellow: '#F1D467',
    yellowBorder: '#ffdb6d',
    yellowTransparent: '#ffd140e0',
    LoaderTransparent: '#0000001f',
    red: '#f01518',
    blue: '#2da7dc',
    green: '#3ea739',
    Shadow:"#fbfbfb",

};
