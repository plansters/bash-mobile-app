import colors from './colors';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const fontWeightRegular = 'normal';
const fontWeightSemiBold = '600';
const fontWeightBold = 'bold';
import {I18nManager} from 'react-native'

export default {
    Text30Bold: {
        fontSize: hp('3.7%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Black" : "Raleway-bold",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text12Light: {
        fontSize: hp('1.5%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Light" : "Raleway-light",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text14Light: {
        fontSize: hp('1.8%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Light" : "Raleway-light",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text16Light: {
        fontSize: hp('2.1%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Regular" : "Raleway-Regular",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text18Light: {
        fontSize: hp('2.4%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Regular" : "Raleway-Regular",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text22Light: {
        fontSize: hp('3.4%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Regular" : "Raleway-Regular",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text13: {
        fontSize: hp('1.5%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Regular" : "Raleway-Regular",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text12Medium: {
        fontSize: hp('1.5%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Regular" : "Raleway-Medium",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text14Medium: {
        fontSize: hp('1.9%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Regular" : "Raleway-Medium",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text12Regular: {
        fontSize: hp('1.5%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Regular" : "Raleway-Regular",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text14OpenSansRegular: {
        fontSize: hp('1.8%'),

        fontFamily: I18nManager.isRTL ? "Cairo-Regular" : "OpenSans-Regular",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text16penSansRegular: {
        fontSize: hp('2.1%'),

        fontFamily: I18nManager.isRTL ? "Cairo-Regular" : "OpenSans-Regular",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text18OpenSansRegular: {
        fontSize: hp('2.4%'),

        fontFamily: I18nManager.isRTL ? "Cairo-Regular" : "OpenSans-Regular",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text2OpenSansRegular: {
        fontSize: hp('2.7%'),

        fontFamily: I18nManager.isRTL ? "Cairo-Regular" : "OpenSans-Regular",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text22penSansRegular: {
        fontSize: hp('3%'),

        fontFamily: I18nManager.isRTL ? "Cairo-Regular" : "OpenSans-Regular",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text24penSansRegular: {
        fontSize: hp('3.3%'),

        fontFamily: I18nManager.isRTL ? "Cairo-Regular" : "OpenSans-Regular",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text14OpenSansBold: {
        fontSize: hp('1.8%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Bold" : "OpenSans-Bold",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text16OpenSansBold: {
        fontSize: hp('2.1%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Bold" : "OpenSans-Bold",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text18OpenSansBold: {
        fontSize: hp('2.35%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Bold" : "OpenSans-Bold",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text20OpenSansBold: {
        fontSize: hp('2.5%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Bold" : "OpenSans-Bold",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text22OpenSansBold: {
        fontSize: hp('3%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Bold" : "OpenSans-Bold",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text12OpenSansBold: {
        fontSize: hp('1.5%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Bold" : "OpenSans-Bold",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },
    Text10OpenSansBold: {
        fontSize: hp('1.2%'),
        fontFamily: I18nManager.isRTL ? "Cairo-Bold" : "OpenSans-Bold",
        textAlign: I18nManager.isRTL ? 'left' : 'auto'
    },


};
