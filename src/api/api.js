import {Platform} from 'react-native';

export const config = {
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Cache-Control': 'max-age=31536000',
    },
    timeout: 5000,


};
export const url = Platform.OS === 'ios' ? 'http://bashformen.com/api/' : 'https://bashformen.com/api/';
