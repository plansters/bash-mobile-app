import {
    FAILURE,
    PRODUCT_LIST_RESET,
    LOADING,
    SUCCESS,
    CUSTOMER_CART_REQUEST,
    CUSTOMER_CART_RESET,
    CUSTOMER_CART_SUCCESS,
    CUSTOMER_CART_FAIL,
    CUSTOMER_CART_LOADING, PRODUCT_REMOVE_FROM_CART_SUCCESS, PRODUCT_ADD_TO_CART_FAIL,
} from '../../constants/actionsType';

const getInitialState = product => ({
    isLoading: false,
    errorMessage: '',
    status: '',
    cart: null,

});

export default (state = getInitialState(null), {type, payload}) => {
    switch (type) {

        case CUSTOMER_CART_RESET:
            return {
                isLoading: false,
                errorMessage: '',
                status: '',
                cart: null,

            };

        case CUSTOMER_CART_LOADING:
            return {
                ...state,
                isLoading: true,
                status: LOADING,
                errorMessage: '',
            };

        case CUSTOMER_CART_SUCCESS:
            return {
                ...state,
                status: SUCCESS,
                errorMessage: '',
                cart: {
                    ...payload,
                },
            };


        case PRODUCT_REMOVE_FROM_CART_SUCCESS:
            return {
                ...state,
                status: SUCCESS,
                errorMessage: '',
            };


        case CUSTOMER_CART_FAIL:
            return {
                ...state,
                status: FAILURE,
                errorMessage: payload,
                cart: null,
            };
        case PRODUCT_ADD_TO_CART_FAIL:
            return {
                ...state,
                status: FAILURE,
                errorMessage: 'error add to cart',
                cart: null,
            };

        default:
            return state;
    }
};
