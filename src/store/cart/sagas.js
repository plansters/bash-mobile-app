import {takeLatest, takeEvery, call, put} from 'redux-saga/effects';
import {I18nManager} from 'react-native';
import axios from 'axios';
import {ManageSys} from './../../utils';
import {
    CATEGORIES,
    CATEGORIES_FAIL,
    CATEGORIES_LOADING,
    CATEGORIES_SUCCESS,
    CUSTOMER_CART_FAIL,
    CUSTOMER_CART_LOADING,
    CUSTOMER_CART_REQUEST,
    CUSTOMER_CART_SUCCESS,
    LOADING,
    PRODUCT_LIST,
    PRODUCT_LIST_FAIL,
    PRODUCT_LIST_LOADING,
    PRODUCT_LIST_SUCCESS, PRODUCT_MINUS_FROM_CART, PRODUCT_PLUS_FROM_CART,
    PRODUCT_REMOVE_FROM_CART,
    PRODUCT_REMOVE_FROM_CART_FAIL,
    PRODUCT_REMOVE_FROM_CART_LOADING,
    PRODUCT_REMOVE_FROM_CART_SUCCESS,
} from '../../constants/actionsType';
import {config, url} from '../../api/api';
import {Reactron} from '../../all';


function* getCustomerCard({payload}) {
    try {


        ManageSys.setOptions({});

        const cart = yield call({content: ManageSys, fn: ManageSys.cart.getCart});

        yield put({type: CUSTOMER_CART_SUCCESS, payload: cart});

    } catch (error) {
        ////////console.log(error)
        yield put({type: CUSTOMER_CART_FAIL, payload: 'Fail to load cart'});


    }
}

function* removeProductFromCart({payload}) {
    try {


        ManageSys.setOptions({});
        ////console.log(payload)
        yield put({type: PRODUCT_REMOVE_FROM_CART_LOADING});

        const data = yield call({content: ManageSys, fn: ManageSys.cart.removeItemCart}, payload);


        yield put({type: PRODUCT_REMOVE_FROM_CART_SUCCESS, payload: {data}});
        yield put({type: CUSTOMER_CART_REQUEST, payload: {}});


    } catch (error) {
        ////////console.log(error)
        yield put({type: PRODUCT_REMOVE_FROM_CART_FAIL, payload: 'Fail to load cart'});


    }
}

function* plusProductFromCart({payload}) {
    try {

        ManageSys.setOptions({});
        yield put({type: PRODUCT_REMOVE_FROM_CART_LOADING});

        const data = yield call({content: ManageSys, fn: ManageSys.cart.plusItemCart}, payload);


        yield put({type: PRODUCT_REMOVE_FROM_CART_SUCCESS, payload: {data}});

        yield put({type: CUSTOMER_CART_REQUEST, payload: {}});


    } catch (error) {
        ////////console.log(error)
        yield put({type: PRODUCT_REMOVE_FROM_CART_FAIL, payload: 'Fail to load cart'});


    }
}

function* minusProductFromCart({payload}) {
    try {

        ////////console.log("item to remove ")
        ////////console.log(payload)
        ManageSys.setOptions({});
        yield put({type: PRODUCT_REMOVE_FROM_CART_LOADING});

        const data = yield call({content: ManageSys, fn: ManageSys.cart.minusItemCart}, payload);


        yield put({type: PRODUCT_REMOVE_FROM_CART_SUCCESS, payload: {data}});
        yield put({type: CUSTOMER_CART_REQUEST, payload: {}});


    } catch (error) {
        ////////console.log(error)
        yield put({type: PRODUCT_REMOVE_FROM_CART_FAIL, payload: 'Fail to load cart'});


    }
}

export default function* watcherSaga() {
    yield takeLatest(CUSTOMER_CART_REQUEST, getCustomerCard);
    yield takeEvery(PRODUCT_REMOVE_FROM_CART, removeProductFromCart);
    yield takeEvery(PRODUCT_PLUS_FROM_CART, plusProductFromCart);
    yield takeEvery(PRODUCT_MINUS_FROM_CART, minusProductFromCart);
}
