import {
    CUSTOMER_CART_REQUEST, CUSTOMER_CART_RESET,
    PRODUCT_MINUS_FROM_CART,
    PRODUCT_PLUS_FROM_CART,
    PRODUCT_REMOVE_FROM_CART,
} from '../../constants/actionsType';


export const removeProductFromCart = (payload) => ({
    type: PRODUCT_REMOVE_FROM_CART,
    payload: payload,
});


export const plusProductFromCart = (payload) => ({
    type: PRODUCT_PLUS_FROM_CART,
    payload: payload,
});

export const minusProductFromCart = (payload) => ({
    type: PRODUCT_MINUS_FROM_CART,
    payload: payload,
});


export const getCustomerCart = () => ({
    type: CUSTOMER_CART_REQUEST,
});
export const resetCustomerCart = () => ({
    type: CUSTOMER_CART_RESET,
});

