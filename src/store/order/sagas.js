import {takeLatest, call, put} from 'redux-saga/effects';
import {I18nManager} from 'react-native';
import axios from 'axios';
import {
    CANCEL_ORDER, CANCEL_ORDER_FAIL, CANCEL_ORDER_LOADING, CANCEL_ORDER_SUCCESS,
    ORDER_LIST, ORDER_LIST_FAIL, ORDER_LIST_LOADING, ORDER_LIST_SUCCESS, PRODUCT_LIST_SUCCESS,

} from '../../constants/actionsType';
import {config, url} from '../../api/api';

const actionGetOrderList = (payload) => {


    var lang = I18nManager.isRTL ? 'lang=ar&' : 'lang=en&';

    return axios.get(url + 'users/' + global.user.id + '/orders' + '?page=' + payload.page + '&per_page=3&' + lang + payload.order_by,
        config)
        .then(function (response) {
            return {status: 200, response: response.data, message: ''};
        })
        .catch(function (error) {
            return {status: 400, response: null, message: error.response.data.errors};
        });
};
const actionCancelOrder = (payload) => {


    var lang = I18nManager.isRTL ? 'lang=ar&' : 'lang=en&';

    return axios.post(url + 'orders/cancel?'+lang,
        payload,
        config)
        .then(function (response) {
            return {status: 200, response: response.data, message: ''};
        })
        .catch(function (error) {
            return {status: 400, response: null, message: error.response.data.errors};
        });
};


function* getOrderList({payload}) {
    try {


        yield put({type: ORDER_LIST_LOADING});

        const data = yield call(actionGetOrderList, payload);

        if (data.status === 200) {

            yield put({
                type: ORDER_LIST_SUCCESS,
                payload: {refresh: payload.refresh, data: data.response.data, last_page: data.response.meta.last_page},
            });
        }
        else {
            yield put({type: ORDER_LIST_FAIL, payload: data.message});
        }

    } catch (error) {
        yield put({type: ORDER_LIST_FAIL, payload: []});
    }
}

function* cancelOrder({payload}) {
    try {


        yield put({type: CANCEL_ORDER_LOADING});

        const data = yield call(actionCancelOrder, payload);

        if (data.status === 200) {

            yield put({
                type: CANCEL_ORDER_SUCCESS,
                payload: {refresh: payload.refresh, data: data.response.data},
            });
        }
        else {
            yield put({type: CANCEL_ORDER_FAIL, payload: data.message});
        }

    } catch (error) {
        yield put({type: CANCEL_ORDER_FAIL, payload: []});
    }
}

export default function* watcherSaga() {
    yield takeLatest(ORDER_LIST, getOrderList);
    yield takeLatest(CANCEL_ORDER, cancelOrder);
}
