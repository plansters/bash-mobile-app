import {
    FAILURE,
    LOADING,
    SUCCESS,
    OPEN_SELECTED_ORDER,
    ORDER_LIST_LOADING,
    ORDER_LIST_SUCCESS,
    ORDER_LIST_FAIL,
    PRODUCT_LIST_LOADING,
    PRODUCT_LIST_SUCCESS,
    PRODUCT_LIST_FAIL,
    PRODUCT_LIST_RESET,
    OPEN_SELECTED_PRODUCT,
    ORDER_LIST_RESET,
    CUSTOMER_LOGIN_SUCCESS,
    CUSTOMER_LOGIN_FAIL,
    CANCEL_ORDER_LOADING, CANCEL_ORDER_SUCCESS, CANCEL_ORDER_FAIL, CANCEL_ORDER_RESET,
} from '../../constants/actionsType';


const getInitialState = order => ({
    current: order,
    isLoadingAction: false,
    isLoadingPro: false,
    status: '',
    statusCancel: '',
    orderList: [],
    last_page: 1,
    ErrorMessageCancel:[],
    ErrorMessage:[]

});

export default (state = getInitialState(null), action) => {
    switch (action.type) {
        case OPEN_SELECTED_ORDER:
            return {
                ...state,
                current: action.payload,
            };
        case ORDER_LIST_LOADING:
            return {
                ...state,
                isLoadingAction: true,
                status: LOADING,
            };


        case ORDER_LIST_SUCCESS:
            if (action.payload.refresh) {

                return {
                    ...state,
                    isLoadingAction: false,
                    status: SUCCESS,
                    ErrorMessage: '',
                    orderList: action.payload.data,
                    last_page: action.payload.last_page,
                };
            }
            else {

                return {
                    ...state,
                    isLoadingAction: false,
                    status: SUCCESS,
                    ErrorMessage: '',
                    orderList: [...state.orderList, ...action.payload.data],
                    last_page: action.payload.last_page,

                };
            }
        case ORDER_LIST_FAIL:

            return {
                ...state,
                isLoadingAction: false,
                status: FAILURE,
                ErrorMessage: action.payload,
                last_page: 1,
            };


        case ORDER_LIST_RESET:
            return getInitialState(null);


        case CANCEL_ORDER_LOADING:
            return {
                ...state,
                isLoadingAction: true,
                ErrorMessageCancel: LOADING,
            };


        case CANCEL_ORDER_SUCCESS:

            return {
                ...state,
                isLoadingAction: false,
                statusCancel: SUCCESS,
                ErrorMessageCancel: [],
            };
        case CANCEL_ORDER_FAIL:

            return {
                ...state,
                isLoadingAction: false,
                statusCancel: FAILURE,
                ErrorMessageCancel: action.payload,
            };

        case CANCEL_ORDER_RESET:

            return {
                ...state,
                isLoadingAction: false,
                statusCancel: '',
                ErrorMessageCancel: [],
            };


        default:
            return state;
    }
};
