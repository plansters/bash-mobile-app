import {
    OFFER_LIST,
    OFFER_LIST_RESET,
} from '../../constants/actionsType';

export const OfferList = payload => (
    {
        type: OFFER_LIST,
        payload: payload,
    });

export const OfferListRest = payload => (
    {
        type: OFFER_LIST_RESET,
        payload: payload,
    });

