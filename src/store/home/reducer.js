import {
    HOME_FEATURE_PRODUCTS_SUCCESS,
    HOME_FEATURE_PRODUCTS_FAIL,
    FAILURE,
    LOADING,
    SUCCESS,
    HOME_FEATURE_PRODUCTS_LOADING,
    HOME_RECENT_PRODUCTS_LOADING,
    HOME_RECENT_PRODUCTS_SUCCESS,
    HOME_RECENT_PRODUCTS_FAIL,
    HOME_TRENDING_PRODUCTS_LOADING,
    HOME_TRENDING_PRODUCTS_SUCCESS,
    HOME_TRENDING_PRODUCTS_FAIL,
    HOME_TOP_CATEGORIES_LOADING,
    HOME_TOP_CATEGORIES_SUCCESS,
    HOME_TOP_CATEGORIES_FAIL,
    HOME_HOME_CATEGORIES_LOADING,
    HOME_HOME_CATEGORIES_SUCCESS,
    HOME_HOME_CATEGORIES_FAIL,
    PRODUCT_RELATED_LOADING,
    PRODUCT_RELATED_SUCCESS,
    PRODUCT_RELATED_FAIL,
    PRODUCT_LIST_LOADING,
    PRODUCT_LIST_SUCCESS,
    PRODUCT_LIST_FAIL,
    PRODUCT_LIST_RESET,
    VENDOR_LIST,
    VENDOR_LIST_LOADING,
    VENDOR_LIST_SUCCESS,
    VENDOR_LIST_FAIL,
    NEW_REQUEST_SUCCESS,
    NEW_REQUEST_LOADING,
    NEW_REQUEST_FAIL,
    NEW_REQUEST_RESET,
    HOME_HOME_SLIDERS_LOADING,
    HOME_HOME_SLIDERS_SUCCESS,
    HOME_HOME_SLIDERS_FAIL,
    HOME_RANDOM_PRODUCTS_LOADING,
    HOME_RANDOM_PRODUCTS_SUCCESS,
    HOME_RANDOM_PRODUCTS_FAIL,
    HOME_OFFERS_LOADING,
    HOME_OFFERS_SUCCESS, HOME_OFFERS_FAIL, HOME_UPDATE_COUNT_CART,
} from '../../constants/actionsType';

const INITIAL_STATE = {

    isLoadingHomeCategories: false,
    isLoadingHomeSliders: false,
    isLoadingHomeOffers: false,
    statusHomeCategories: '',
    statusHomeSliders: '',
    statusHomeOffers: '',
    ErrorMessage: '',

    homeCategories: [],
    homeOffers: [],
    homeSliders: [],
    cartCount: 0,

};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {


        case HOME_HOME_CATEGORIES_LOADING:
            return {
                ...state,
                isLoadingHomeCategories: true,
                statusHomeCategories: LOADING,
            };

        case HOME_HOME_CATEGORIES_SUCCESS:
            return {
                ...state,
                isLoadingHomeCategories: false,
                statusHomeCategories: SUCCESS,
                ErrorMessage: '',
                homeCategories: payload,
            };
        case HOME_HOME_CATEGORIES_FAIL:

            return {
                ...state,
                isLoadingHomeCategories: false,
                statusHomeCategories: FAILURE,
                ErrorMessage: payload,
            };


        case HOME_OFFERS_LOADING:
            return {
                ...state,
                isLoadingHomeOffers: true,
                statusHomeOffers: LOADING,
            };

        case HOME_OFFERS_SUCCESS:
            return {
                ...state,
                isLoadingHomeOffers: false,
                statusHomeOffers: SUCCESS,
                ErrorMessage: '',
                homeOffers: payload,
            };
        case HOME_OFFERS_FAIL:

            return {
                ...state,
                isLoadingHomeOffers: false,
                statusHomeOffers: FAILURE,
                ErrorMessage: payload,
            };


        case HOME_HOME_SLIDERS_LOADING:
            return {
                ...state,
                isLoadingHomeSliders: true,
                statusHomeSliders: LOADING,
            };

        case HOME_HOME_SLIDERS_SUCCESS:
            return {
                ...state,
                isLoadingHomeSliders: false,
                statusHomeSliders: SUCCESS,
                ErrorMessage: '',
                homeSliders: payload,
            };
        case HOME_HOME_SLIDERS_FAIL:

            return {
                ...state,
                isLoadingHomeSliders: false,
                statusHomeSliders: FAILURE,
                ErrorMessage: payload,
            };

        case HOME_UPDATE_COUNT_CART:

            return {
                ...state,
                cartCount: global.cart && global.cart.line_items && global.cart.line_items.length > 0 ? global.cart.line_items.length : 0,
            };


        default:
            return state;
    }
};


