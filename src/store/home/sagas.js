import {takeLatest, call, put, delay} from 'redux-saga/effects';
import {I18nManager} from 'react-native';
import axios from 'axios';
import {
    HOME_FEATURE_PRODUCTS,
    HOME_FEATURE_PRODUCTS_FAIL,
    HOME_FEATURE_PRODUCTS_LOADING,
    HOME_FEATURE_PRODUCTS_SUCCESS,
    HOME_HOME_CATEGORIES,
    HOME_HOME_CATEGORIES_FAIL,
    HOME_HOME_CATEGORIES_LOADING,
    HOME_HOME_CATEGORIES_SUCCESS,
    HOME_HOME_SLIDERS,
    HOME_HOME_SLIDERS_FAIL,
    HOME_HOME_SLIDERS_LOADING,
    HOME_HOME_SLIDERS_SUCCESS, HOME_OFFERS, HOME_OFFERS_FAIL, HOME_OFFERS_LOADING, HOME_OFFERS_SUCCESS,
    HOME_RANDOM_PRODUCTS,
    HOME_RANDOM_PRODUCTS_FAIL,
    HOME_RANDOM_PRODUCTS_LOADING,
    HOME_RANDOM_PRODUCTS_SUCCESS,
    HOME_RECENT_PRODUCTS,
    HOME_RECENT_PRODUCTS_FAIL,
    HOME_RECENT_PRODUCTS_LOADING,
    HOME_RECENT_PRODUCTS_SUCCESS,
    HOME_TOP_CATEGORIES,
    HOME_TOP_CATEGORIES_FAIL,
    HOME_TOP_CATEGORIES_LOADING,
    HOME_TOP_CATEGORIES_SUCCESS,
    HOME_TRENDING_PRODUCTS,
    HOME_TRENDING_PRODUCTS_FAIL,
    HOME_TRENDING_PRODUCTS_LOADING,
    HOME_TRENDING_PRODUCTS_SUCCESS,
    LOADING,
    NEW_REQUEST,
    NEW_REQUEST_FAIL,
    NEW_REQUEST_LOADING,
    NEW_REQUEST_RESET,
    NEW_REQUEST_SUCCESS,
    PLACE_ORDER_REQUEST_FAIL,
    PLACE_ORDER_REQUEST_LOADING,
    PLACE_ORDER_REQUEST_SUCCESS,
    PLACE_ORDER_RESET,
    PRODUCT_LIST,
    PRODUCT_LIST_FAIL,
    PRODUCT_LIST_LOADING,
    PRODUCT_LIST_SUCCESS,
    PRODUCT_RELATED,
    PRODUCT_RELATED_FAIL,
    PRODUCT_RELATED_LOADING,
    PRODUCT_RELATED_SUCCESS,
    VENDOR_LIST,
    VENDOR_LIST_FAIL,
    VENDOR_LIST_LOADING,
    VENDOR_LIST_SUCCESS,
} from '../../constants/actionsType';
import {config, url, url_S} from '../../api/api';



const actionGetHomeCategories = (payload) => {


    var lang = I18nManager.isRTL ? 'lang=ar&' : 'lang=en&';
    return axios.get(url + 'categories?per_page=10&' + lang,
        config)
        .then(function (response) {
          //  ////console.log(response);

            return {status: 200, response: response.data, message: ''};
        })
        .catch(function (error) {
          //  ////console.log(error);

            return {status: 400, response: null, errors: error.response.data.errors};
        });

};
const actionGetHomeOffers = (payload) => {


    var lang = I18nManager.isRTL ? 'lang=ar&' : 'lang=en&';
    return axios.get(url + 'products/offers/all?per_page=10&' + lang,
        config)
        .then(function (response) {
           // ////console.log(response);

            return {status: 200, response: response.data, message: ''};
        })
        .catch(function (error) {
          //  ////console.log(error);

            return {status: 400, response: null, errors: error.response.data.errors};
        });

};
const actionGetHomeSliders = (payload) => {


    var lang = I18nManager.isRTL ? '&lang=ar&' : '&lang=en&';
    return axios.get(url + 'ads?'+lang,
        config)
        .then(function (response) {
           // ////console.log("response.data")
           // ////console.log(response.data)
            return {status: 200, response: response.data, message: ''};
        })
        .catch(function (error) {
            return {status: 400, response: null, message: error.response.data.message};
        });
    //return axios.get('https://etloob.com/wp-json/wc/v3/products?featured=true', payload)
};

function* getHomeCategories({payload}) {
    try {

        yield put({type: HOME_HOME_CATEGORIES_LOADING});

        const data = yield call(actionGetHomeCategories, payload);

        if (data.status === 200) {

            yield put({type: HOME_HOME_CATEGORIES_SUCCESS, payload: data.response.data});
        }
        else {

            yield put({type: HOME_HOME_CATEGORIES_FAIL, payload: data.errors});
        }

    } catch (error) {
        ////////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}

function* getHomeOffers({payload}) {
    try {

        yield put({type: HOME_OFFERS_LOADING});

        const data = yield call(actionGetHomeOffers, payload);

        if (data.status === 200) {

            yield put({type: HOME_OFFERS_SUCCESS, payload: data.response.data});
        }
        else {

            yield put({type: HOME_OFFERS_FAIL, payload: data.errors});
        }

    } catch (error) {

        yield put({type: HOME_OFFERS_FAIL, payload: []});

    }
}

function* getHomesliders({payload}) {
    try {

        yield put({type: HOME_HOME_SLIDERS_LOADING});

        const data = yield call(actionGetHomeSliders, payload);

        if (data.status === 200) {

            ////////console.log("instide func 200 ");
            ////////console.log(data.response.data)
            yield put({type: HOME_HOME_SLIDERS_SUCCESS, payload: data.response});
        }
        else {

            ////////console.log("instide func 400 ");
            ////////console.log(data.message)
            yield put({type: HOME_HOME_SLIDERS_FAIL, payload: data.message});
        }

    } catch (error) {
        ////////console.log(error);
        // yield put({ type: MAGENTO.SIGN_UP_FAILURE, payload: { errorMessage: error.message } });
    }
}


export default function* watcherSaga() {
    yield takeLatest(HOME_HOME_CATEGORIES, getHomeCategories);
    yield takeLatest(HOME_OFFERS, getHomeOffers);
    yield takeLatest(HOME_HOME_SLIDERS, getHomesliders);


}
