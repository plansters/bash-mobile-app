import {
    HOME_FEATURE_PRODUCTS,
    HOME_RECENT_PRODUCTS,
    HOME_TRENDING_PRODUCTS,
    HOME_TOP_CATEGORIES,
    HOME_RANDOM_PRODUCTS,
    HOME_HOME_CATEGORIES,
    PRODUCT_RELATED,
    PRODUCT_LIST,
    VENDOR_LIST,
    NEW_REQUEST,
    HOME_HOME_SLIDERS,
    HOME_OFFERS,
    HOME_UPDATE_COUNT_CART,
} from '../../constants/actionsType';

export const HomeCategories = payload => ({
    type: HOME_HOME_CATEGORIES,
    payload,
});
export const HomeOffers = payload => ({
    type: HOME_OFFERS,
    payload,
});
export const HomeSliders = payload => ({
    type: HOME_HOME_SLIDERS,
    payload,
});

export const CountCartUpdate = payload => ({
    type: HOME_UPDATE_COUNT_CART,
    payload,
});
