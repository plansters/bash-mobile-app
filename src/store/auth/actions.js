import {CUSTOMER_SIGNUP_REQUEST, CUSTOMER_AUTH_STATE_RESET} from "../../constants/actionsType";

export const CustomerSignUp = payload => ({
    type: CUSTOMER_SIGNUP_REQUEST,
    payload,
});
export const resetAuthState = () => ({
    type: CUSTOMER_AUTH_STATE_RESET,
});
