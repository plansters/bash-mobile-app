import {
    CUSTOMER_AUTH_STATE_RESET,
    CUSTOMER_SIGNUP_FAIL,
    CUSTOMER_SIGNUP_SUCCESS,
    FAILURE,
    LOADING,
    SUCCESS,
} from '../../constants/actionsType';

const INITIAL_STATE = {
    isLoadingAction: false,
    status: '',
    ErrorMessage: [],
    user: null,
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {
        case LOADING:
            return {
                ...state,
                isLoadingAction: true,
                status: LOADING,
            };

        case CUSTOMER_SIGNUP_SUCCESS:

            return {
                ...state,
                isLoadingAction: false,
                status: SUCCESS,
                ErrorMessage: '',
                user: payload,
            };
        case CUSTOMER_SIGNUP_FAIL:
            return {
                ...state,
                isLoadingAction: false,
                status: FAILURE,
                ErrorMessage: payload,
            };


        case CUSTOMER_AUTH_STATE_RESET:
            return {
                isLoadingAction: false,
                status: '',
                ErrorMessage: [],
                user: null,
            };

        default:
            return state;
    }
};


