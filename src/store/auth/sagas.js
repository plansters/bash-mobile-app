import {takeLatest, call, put} from 'redux-saga/effects';
import axios from 'axios';
import {
    CUSTOMER_LOGIN_FAIL,
    CUSTOMER_SIGNUP,
    CUSTOMER_SIGNUP_FAIL, CUSTOMER_SIGNUP_FAILl,
    CUSTOMER_SIGNUP_REQUEST,
    CUSTOMER_SIGNUP_SUCCESS, CUSTOMER_VERIFY_FAIL, CUSTOMER_VERIFY_PHONE, CUSTOMER_VERIFY_SUCCESS,
    LOADING,
} from '../../constants/actionsType';
import {config, url} from '../../api/api';
import {I18nManager} from 'react-native';


const actionSignup = (payload) => {
    var lang = I18nManager.isRTL ? 'lang=ar&' : 'lang=en&';

    return axios.post(url + 'auth/register?'+lang,
        payload
        , config)
        .then(function (response) {
        //console.log(response);

            return {status: 200, response: response.data, message: ''};
        })
        .catch(function (error) {

           //console.log(error.response.data);
            return {status: 400, response: null, errors: error.response.data.errors};
        });
};

function* CustomerSignUp({payload}) {
    try {

        yield put({type: LOADING});
        //console.log(payload);

        const data = yield call(actionSignup, payload);

        if (data.status === 200) {

            yield put({type: CUSTOMER_SIGNUP_SUCCESS, payload: data.response.data});
        }
        else {

            yield put({type: CUSTOMER_SIGNUP_FAIL, payload: data.errors});
        }

    } catch (error) {
        yield put({type: CUSTOMER_SIGNUP_FAIL, payload: []});
    }
}

export default function* watcherSaga() {
    yield takeLatest(CUSTOMER_SIGNUP_REQUEST, CustomerSignUp);
}
