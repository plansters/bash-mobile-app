import {combineReducers} from 'redux';

import authReducer from './auth/reducer';
import loginReducer from './login/reducer';
import categoryReducer from './category/reducer';
import productListReducer from './ProductList/reducer';
import productReducer from './product/reducer';
import cartReducer from './cart/reducer';
import homeReducer from './home/reducer';
import offerListReducer from './OfferList/reducer';
import checkoutReducer from './checkout/reducer';
import orderReducer from './order/reducer';
import complaintReducer from './complaint/reducer';
import profileReducer from './profile/reducer';
import searchListReducer from './SearchList/reducer';

export default combineReducers({
    auth: authReducer,
    login: loginReducer,
    category: categoryReducer,
    productList: productListReducer,
    product: productReducer,
    cart: cartReducer,
    home: homeReducer,
    offerList: offerListReducer,
    checkout: checkoutReducer,
    order: orderReducer,
    complaint: complaintReducer,
    profile: profileReducer,
    searchList: searchListReducer,


});
