import {takeLatest, call, put,takeEvery} from 'redux-saga/effects';
import {I18nManager} from 'react-native';
import axios from 'axios';
import {
    CATEGORIES,
    CATEGORIES_FAIL,
    CATEGORIES_LOADING,
    CATEGORIES_SUCCESS,
    CATEGORY_SUB_LIST, CATEGORY_SUB_LIST_FAIL,
    CATEGORY_SUB_LIST_LOADING, CATEGORY_SUB_LIST_SUCCESS,
    CUSTOMER_LOGIN_FAIL,
    LOADING,
} from '../../constants/actionsType';
import {config, url} from '../../api/api';


const actionGetCategories = (payload) => {


    var lang = I18nManager.isRTL ? 'lang=ar&' : 'lang=en&';
    return axios.get(url + 'categories?per_page=10&'+lang,
        config)
        .then(function (response) {
           // ////console.log(response);

            return {status: 200, response: response.data, message: ''};
        })
        .catch(function (error) {
            //////console.log(error);

            return {status: 400, response: null, errors: error.response.data.errors};
        });
};


function* getCategories({payload}) {
    try {

        yield put({type: CATEGORIES_LOADING});

        ////////console.log(payload)
        payload = '';
        const data = yield call(actionGetCategories, payload);

        if (data.status === 200) {

            yield put({type: CATEGORIES_SUCCESS, payload: data.response.data});
        }
        else {

            yield put({type: CATEGORIES_FAIL, payload: data.errors});
        }

    } catch (error) {
        yield put({type: CATEGORIES_FAIL, payload: []});

    }
}

const actionGetCategoriesSub = (payload) => {


    var lang = I18nManager.isRTL ? 'lang=ar&' : 'lang=en&';
    return axios.get(url + 'categories/' + payload.category + '?per_page=10&' + lang,
        config)
        .then(function (response) {
            //console.log("Response Sub ");
            //console.log(response);

            return {status: 200, response: response.data.data, message: ''};
        })
        .catch(function (error) {
            //////console.log(error);

            return {status: 400, response: null, errors: error.response.data.errors};
        });
};


function* getSubcategoryList({payload}) {
    try {

        yield put({type: CATEGORY_SUB_LIST_LOADING});

        const data = yield call(actionGetCategoriesSub, payload);

        if (data.status === 200) {

            //console.log("data.response")
            //console.log(data.response.sub_categories)
            yield put({type: CATEGORY_SUB_LIST_SUCCESS, payload: data.response.sub_categories});
        }
        else {

            yield put({type: CATEGORY_SUB_LIST_FAIL, payload: data.errors});
        }

    } catch (error) {
        yield put({type: CATEGORY_SUB_LIST_FAIL, payload: []});

    }
}

export default function* watcherSaga() {
    yield takeLatest(CATEGORIES, getCategories);
    yield takeEvery(CATEGORY_SUB_LIST, getSubcategoryList);

}
