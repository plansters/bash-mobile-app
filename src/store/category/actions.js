import {
    CATEGORIES, CATEGORY_SUB_LIST, CATEGORY_SUB_LIST_RESET, OPEN_SELECTED_CATEGORY, RESET_SELECTED_CATEGORY,
} from '../../constants/actionsType';

export const Categories = payload => ({
    type: CATEGORIES,
    payload,
});

export const openSelectedCategory = category => (
    {
        type: OPEN_SELECTED_CATEGORY,
        payload: category,
    });

export const ResetSelectedCategory = category => (
    {
        type: RESET_SELECTED_CATEGORY,
        payload: category,
    });
export const SubCategoryLists = payload => (
    {
        type: CATEGORY_SUB_LIST,
        payload: payload,
    });

export const SubCategoriesRest = payload => (
    {
        type: CATEGORY_SUB_LIST_RESET,
        payload: payload,
    });
