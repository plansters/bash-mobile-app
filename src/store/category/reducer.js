import {
    FAILURE,
    LOADING,
    SUCCESS,
    CATEGORIES_LOADING,
    CATEGORIES_SUCCESS,
    CATEGORIES_FAIL,
    OPEN_SELECTED_CATEGORY,
    RESET_SELECTED_CATEGORY,
    CATEGORY_SUB_LIST_LOADING,
    CATEGORY_SUB_LIST_SUCCESS, CATEGORY_SUB_LIST_FAIL, CATEGORY_SUB_LIST_RESET,
} from '../../constants/actionsType';
import {Reactron} from '../../all';

const INITIAL_STATE = {
    currentCategory: null,
    currentSearch: null,
    isLoadingAction: false,
    status: '',
    ErrorMessage: '',
    categories: [],

    ErrorMessageSub: '',
    subCategoryList: [],
    statusSub: '',
    isLoadingActionSub: false,
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {
        case OPEN_SELECTED_CATEGORY:
            return {
                ...state,
                currentCategory: payload,
            };
        case RESET_SELECTED_CATEGORY:
            //console.log('ffreset');
            return {
                ...state,
                currentCategory: null,
            };
        case CATEGORIES_LOADING:
            return {
                ...state,
                isLoadingAction: true,
                status: LOADING,
            };

        case CATEGORIES_SUCCESS:
            ////console.log('CATEGORIES_SUCCESS');
            ////console.log(payload);
            return {
                ...state,
                isLoadingAction: false,
                status: SUCCESS,
                ErrorMessage: '',
                categories: payload,
            };
        case CATEGORIES_FAIL:

            ////console.log('CATEGORIES_FAIL');
            ////console.log(payload);
            return {
                ...state,
                isLoadingAction: false,
                status: FAILURE,
                ErrorMessage: payload,
            };


        case CATEGORY_SUB_LIST_LOADING:
            return {
                ...state,
                isLoadingActionSub: true,
                statusSub: LOADING,
            };

        case CATEGORY_SUB_LIST_SUCCESS:


            return {
                ...state,
                isLoadingActionSub: false,
                statusSub: SUCCESS,
                ErrorMessageSub: '',
                subCategoryList: payload,
            };


        case CATEGORY_SUB_LIST_FAIL:
            return {
                ...state,
                isLoadingActionSub: false,
                statusSub: FAILURE,
                ErrorMessageSub: payload,

            };

        case CATEGORY_SUB_LIST_RESET:
            return {
                isLoadingActionSub: false,
                ErrorMessageSub: '',
                subCategoryList: [],
                statusSub: '',

            };



        default:
            return state;
    }
};


