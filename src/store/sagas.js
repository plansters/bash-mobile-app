import {fork} from 'redux-saga/effects';
import authSagas from './auth/sagas';
import loginSagas from './login/sagas';
import categorySagas from './category/sagas';
import productListSagas from './ProductList/sagas';
import productSagas from './product/sagas';
import cartSagas from './cart/sagas';
import homeSagas from './home/sagas';
import offerListSagas from './OfferList/sagas';
import checkoutSagas from './checkout/sagas';
import orderSagas from './order/sagas';
import complaintSagas from './complaint/sagas';
import profileSagas from './profile/sagas';
import searchListSagas from './SearchList/sagas';


export default function* root() {
    yield fork(authSagas);
    yield fork(categorySagas);
    yield fork(loginSagas);
    yield fork(productListSagas);
    yield fork(productSagas);
    yield fork(cartSagas);
    yield fork(homeSagas);
    yield fork(offerListSagas);
    yield fork(checkoutSagas);
    yield fork(orderSagas);
    yield fork(complaintSagas);
    yield fork(profileSagas);
    yield fork(searchListSagas);

}
