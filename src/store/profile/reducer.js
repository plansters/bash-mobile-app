import {
    FAILURE,
    LOADING,
    SUCCESS,
    OPEN_SELECTED_ORDER,
    ORDER_LIST_LOADING,
    ORDER_LIST_SUCCESS,
    ORDER_LIST_FAIL,
    PRODUCT_LIST_LOADING,
    PRODUCT_LIST_SUCCESS,
    PRODUCT_LIST_FAIL,
    PRODUCT_LIST_RESET, EDIT_PROFILE_LOADING, EDIT_PROFILE_SUCCESS, EDIT_PROFILE_ERROR, EDIT_PROFILE_RESET,
} from '../../constants/actionsType';

const getInitialState = order => ({

    isLoadingAction: false,
    statusEditProfile: '',
    ErrorMessageEditProfile: [],
    user: null,
});

export default (state = getInitialState(null), action) => {
    switch (action.type) {

        case EDIT_PROFILE_LOADING:
            return {
                ...state,
                isLoadingAction: true,
                statusEditProfile: LOADING,
            };


        case EDIT_PROFILE_SUCCESS:
            return {
                ...state,
                isLoadingAction: false,
                statusEditProfile: SUCCESS,
                ErrorMessageEditProfile: [],
                user: action.payload,
            };

        case EDIT_PROFILE_ERROR:

            return {
                ...state,
                isLoadingAction: false,
                statusEditProfile: FAILURE,
                ErrorMessageEditProfile: action.payload,
            };


        case EDIT_PROFILE_RESET:

            return {
                isLoadingAction: false,
                statusEditProfile: '',
                ErrorMessageEditProfile: [],
            };


        default:
            return state;
    }
};
