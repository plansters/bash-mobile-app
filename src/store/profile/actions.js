import {
    COMPLAINT_RESET,
    EDIT_PROFILE, EDIT_PROFILE_RESET,
    OPEN_SELECTED_ORDER, ORDER_LIST,
} from '../../constants/actionsType';

export const editProfileReset = payload => (
    {
        type: EDIT_PROFILE_RESET,
        payload: payload,
    });


export const editProfile = payload => (
    {
        type: EDIT_PROFILE,
        payload: payload,
    });

