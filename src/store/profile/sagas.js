import {takeLatest, call, put, delay} from 'redux-saga/effects';
import {I18nManager} from 'react-native';
import axios from 'axios';
import {
    EDIT_PROFILE, EDIT_PROFILE_ERROR, EDIT_PROFILE_LOADING, EDIT_PROFILE_RESET, EDIT_PROFILE_SUCCESS,
    ORDER_LIST, ORDER_LIST_FAIL, ORDER_LIST_LOADING, ORDER_LIST_SUCCESS, PRODUCT_LIST_SUCCESS,

} from '../../constants/actionsType';
import {config, url} from '../../api/api';

const actionEditProfile = (payload) => {

    var lang = I18nManager.isRTL ? 'lang=ar&' : 'lang=en&';
    return axios.put(url + 'users/' + global.user.id+"?"+lang,
        payload,
        config)
        .then(function (response) {
            return {status: 200, response: response.data, message: ''};
        })
        .catch(function (error) {
            return {status: 400, response: null, message: error.response.data.errors};
        });
};


function* postEditProfile({payload}) {
    try {


        yield put({type: EDIT_PROFILE_LOADING});

        const data = yield call(actionEditProfile, payload);

        if (data.status === 200) {


            yield put({type: EDIT_PROFILE_SUCCESS, payload: data.response.data});

        }
        else {
            yield put({type: EDIT_PROFILE_ERROR, payload: data.message});
        }

    } catch (error) {

        yield put({type: EDIT_PROFILE_ERROR, payload: []});
    }
}

export default function* watcherSaga() {
    yield takeLatest(EDIT_PROFILE, postEditProfile);
}
