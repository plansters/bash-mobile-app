import {takeLatest, call, put} from 'redux-saga/effects';
import axios from 'axios';

import {
    CUSTOMER_LOGIN_FAIL,
    CUSTOMER_LOGIN_REQUEST, CUSTOMER_LOGIN_SUCCESS,
    CUSTOMER_SIGNUP, CUSTOMER_SIGNUP_FAILl,
    LOADING,
} from '../../constants/actionsType';
import {config, url} from '../../api/api';
import {I18nManager} from 'react-native';

const actionLogin = (payload) => {
    var lang = I18nManager.isRTL ? 'lang=ar&' : 'lang=en&';

    return axios.post(url + 'auth/login?'+lang,
        payload
        , config)
        .then(function (response) {
           //console.log(response);

            return {status: 200, response: response.data, message: ''};
        })
        .catch(function (error) {

            //console.log(error.response.data);
            return {status: 400, response: null, errors: error.response.data.errors};
        });
};

function* CustomerLogin({payload}) {
    try {

        yield put({type: LOADING});

        const data = yield call(actionLogin, payload);

        if (data.status === 200) {

            yield put({type: CUSTOMER_LOGIN_SUCCESS, payload: data.response.data});
        }
        else {

            yield put({type: CUSTOMER_LOGIN_FAIL, payload: data.errors});
        }

    } catch (error) {
        yield put({type: CUSTOMER_LOGIN_FAIL, payload: []});

    }
}

export default function* watcherSaga() {
    yield takeLatest(CUSTOMER_LOGIN_REQUEST, CustomerLogin);
}
