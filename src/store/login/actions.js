import { CUSTOMER_AUTH_STATE_RESET,CUSTOMER_LOGIN_REQUEST} from "../../constants/actionsType";

export const CustomerLogin = payload => ({
    type: CUSTOMER_LOGIN_REQUEST,
    payload,
});
export const resetAuthState = () => ({
    type: CUSTOMER_AUTH_STATE_RESET,
});
