import {
    CUSTOMER_AUTH_STATE_RESET, CUSTOMER_LOGIN_FAIL, CUSTOMER_LOGIN_SUCCESS,
    FAILURE,
    LOADING,
    SUCCESS
} from "../../constants/actionsType";


const INITIAL_STATE = {
    isLoadingAction: false,
    status: "",
    ErrorMessage: [],
    user: null,
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {
        case LOADING:
            return {
                ...state,
                isLoadingAction: true,
                status: LOADING,
            };


        case CUSTOMER_LOGIN_SUCCESS:

          //  ////console.log(payload)
            return {
                ...state,
                isLoadingAction: false,
                status: SUCCESS,
                ErrorMessage: [],
                user: payload
            };
        case CUSTOMER_LOGIN_FAIL:

           // ////console.log("CUSTOMER_LOGIN_FAIL")
           // ////console.log(payload)
            return {
                ...state,
                isLoadingAction: false,
                status: FAILURE,
                ErrorMessage: payload,
            };


        case CUSTOMER_AUTH_STATE_RESET:
            return INITIAL_STATE;

        default:
            return state;
    }
};


