import {
    CANCEL_ORDER, CANCEL_ORDER_RESET, COMPLAINT, COMPLAINT_RESET,
    OPEN_SELECTED_ORDER, OPEN_SELECTED_PRODUCT, ORDER_LIST, ORDER_LIST_RESET, PRODUCT_LIST_RESET,
} from '../../constants/actionsType';

export const CustomerAddComplaint = payload => (
    {
        type: COMPLAINT,
        payload: payload,
    });

export const CustomerComplaintReset = payload => (
    {
        type: COMPLAINT_RESET,
        payload: payload,
    });

