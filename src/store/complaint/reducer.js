import {
    FAILURE,
    LOADING,
    SUCCESS,
    OPEN_SELECTED_ORDER,
    ORDER_LIST_LOADING,
    ORDER_LIST_SUCCESS,
    ORDER_LIST_FAIL,
    PRODUCT_LIST_LOADING,
    PRODUCT_LIST_SUCCESS,
    PRODUCT_LIST_FAIL,
    PRODUCT_LIST_RESET,
    OPEN_SELECTED_PRODUCT,
    ORDER_LIST_RESET,
    CUSTOMER_LOGIN_SUCCESS,
    CUSTOMER_LOGIN_FAIL,
    CANCEL_ORDER_LOADING,
    CANCEL_ORDER_SUCCESS,
    CANCEL_ORDER_FAIL,
    CANCEL_ORDER_RESET,
    COMPLAINT_LOADING,
    COMPLAINT_SUCCESS, COMPLAINT_FAIL, COMPLAINT_RESET,
} from '../../constants/actionsType';


const getInitialState = order => ({
    isLoadingAction: false,
    statusComplaint: '',
    ErrorMessageComplaint: [],

});

export default (state = getInitialState(null), action) => {
    switch (action.type) {


        case COMPLAINT_LOADING:
            return {
                ...state,
                isLoadingAction: true,
                ErrorMessageComplaint: LOADING,
                statusComplaint: '',
            };


        case COMPLAINT_SUCCESS:

            return {
                ...state,
                isLoadingAction: false,
                statusComplaint: SUCCESS,
                ErrorMessageComplaint: [],
            };
        case COMPLAINT_FAIL:

            return {
                ...state,
                isLoadingAction: false,
                statusComplaint: FAILURE,
                ErrorMessageComplaint: action.payload,
            };

        case COMPLAINT_RESET:

            return {
                ...state,
                isLoadingAction: false,
                statusComplaint: '',
                ErrorMessageComplaint: [],
            };


        default:
            return state;
    }
};
