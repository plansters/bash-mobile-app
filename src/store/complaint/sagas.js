import {takeLatest, call, put} from 'redux-saga/effects';
import {I18nManager} from 'react-native';
import axios from 'axios';
import {
    CANCEL_ORDER,
    CANCEL_ORDER_FAIL,
    CANCEL_ORDER_LOADING,
    CANCEL_ORDER_SUCCESS,
    COMPLAINT, COMPLAINT_FAIL,
    COMPLAINT_LOADING,
    COMPLAINT_SUCCESS,
    ORDER_LIST,
    ORDER_LIST_FAIL,
    ORDER_LIST_LOADING,
    ORDER_LIST_SUCCESS,
    PRODUCT_LIST_SUCCESS,

} from '../../constants/actionsType';
import {config, url} from '../../api/api';


const actionAddComplaint = (payload) => {


    var lang = I18nManager.isRTL ? 'lang=ar&' : 'lang=en&';

    return axios.post(url + 'complaints?'+lang,
        payload,
        config)
        .then(function (response) {
            return {status: 200, response: response.data, message: ''};
        })
        .catch(function (error) {
            return {status: 400, response: null, message: error.response.data.errors};
        });
};


function* AddComplaint({payload}) {
    try {


        yield put({type: COMPLAINT_LOADING});

        const data = yield call(actionAddComplaint, payload);

        if (data.status === 200) {

            yield put({
                type: COMPLAINT_SUCCESS,
                payload: {refresh: payload.refresh, data: data.response.data},
            });
        }
        else {
            yield put({type: COMPLAINT_FAIL, payload: data.message});
        }

    } catch (error) {
        yield put({type: COMPLAINT_FAIL, payload: []});
    }
}

export default function* watcherSaga() {
    yield takeLatest(COMPLAINT, AddComplaint);
}
