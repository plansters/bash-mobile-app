import {
    FAILURE,
    LOADING,
    OPEN_SELECTED_PRODUCT,
    SUCCESS,
    PRODUCT_ADD_TO_CART_LOADING,
    PRODUCT_ADD_TO_CART_SUCCESS,
    PRODUCT_ADD_TO_CART_RESET,
    PRODUCT_ADD_TO_CART_RESET_FROM_SAGA,
    PRODUCT_ADD_TO_FAVORITE_LOADING,
    PRODUCT_ADD_TO_FAVORITE_RESET,
    PRODUCT_ADD_TO_FAVORITE_SUCCESS,
    PRODUCT_VARIANT_LOADING, PRODUCT_VARIANT_SUCCESS, PRODUCT_VARIANT_FAIL,
} from '../../constants/actionsType';

const getInitialState = product => ({
    current: product,
    isLoadingAction: false,
    statusVariant: '',
    ErrorMessageVariant: '',
    variant: null,



});

export default (state = getInitialState(null), action) => {
    switch (action.type) {
        case OPEN_SELECTED_PRODUCT:
            return {
                ...state,
                current: action.payload,
                addToCartLoading: false,
                addToCartError: null,
                qtyInput: 1,
                selectedOptions: {},
                ErrorMessageADDProductToCart: '',
            };

        case PRODUCT_ADD_TO_CART_LOADING:
            return {
                ...state,
                isLoadingAddToCart: true,
                statusAddToCart: LOADING,
                ErrorMessageADDProductToCart: '',
            };


        case PRODUCT_ADD_TO_CART_RESET:

            return {

                isLoadingAddToCart: false,
                statusAddToCart: '', ErrorMessageADDProductToCart: '',
                ...state
            };
        case PRODUCT_ADD_TO_CART_RESET_FROM_SAGA:

            return {

                isLoadingAddToCart: false,
                statusAddToCart: '', ErrorMessageADDProductToCart: '',
                ...state
            };

        case PRODUCT_ADD_TO_CART_SUCCESS:


            return {
                ...state,
                isLoadingAddToCart: false,
                statusAddToCart: SUCCESS,
                ErrorMessage: action.payload,
                ErrorMessageADDProductToCart: action.payload,
            };


        case PRODUCT_VARIANT_LOADING:
            return {
                ...state,
                isLoadingAction: true,
                statusVariant: LOADING,
                ErrorMessageVariant: '', variant: null,
            };

        case PRODUCT_VARIANT_SUCCESS:


            return {
                ...state,
                isLoadingAction: false,
                statusVariant: SUCCESS,
                ErrorMessageVariant: '',
                variant: action.payload,
            };

        case PRODUCT_VARIANT_FAIL:


            return {
                ...state,
                isLoadingAction: false,
                statusVariant: FAILURE,
                ErrorMessageVariant: '',
                variant: null,
            };

        default:
            return state;
    }
};
