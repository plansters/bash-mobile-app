import {takeLatest, delay, call, put} from 'redux-saga/effects';
import {I18nManager} from 'react-native';
import axios from 'axios';
import {ManageSys} from './../../utils';
import strings from './../../locales/i18n';
import {
    CATEGORIES,
    CATEGORIES_FAIL,
    CATEGORIES_LOADING,
    CATEGORIES_SUCCESS,
    CUSTOMER_CART_REQUEST, GET_VARIANT_PRODUCT,
    LOADING,
    PRODUCT_ADD_TO_CART,
    PRODUCT_ADD_TO_CART_FAIL,
    PRODUCT_ADD_TO_CART_LOADING,
    PRODUCT_ADD_TO_CART_RESET,
    PRODUCT_ADD_TO_CART_RESET_FROM_SAGA,
    PRODUCT_ADD_TO_CART_SUCCESS,  PRODUCT_VARIANT_FAIL, PRODUCT_VARIANT_LOADING, PRODUCT_VARIANT_SUCCESS,
} from '../../constants/actionsType';
import {config, url} from '../../api/api';


const actionGetProductVendor = (payload) => {

    var lang = I18nManager.isRTL ? 'lang=ar&' : 'lang=en&';
    return axios.post(url + 'products/variation?' + lang,
        payload,
        config)
        .then(function (response) {
           //console.log("dddddd====");
           //console.log(response);

            return {status: 200, response: response.data, message: ''};
        })
        .catch(function (error) {
           //console.log(error);

            return {status: 400, response: null, errors: error.response.data.errors};
        });
};

function* addToCart({payload}) {
    try {
        //
        ManageSys.setOptions({});


       ////console.log(payload)
        const data = yield call({content: ManageSys, fn: ManageSys.cart.addItemToCart}, payload.item);

        //////console.log("after yeild add to cart ")
        // yield delay(10);
        yield put({type: PRODUCT_ADD_TO_CART_SUCCESS, payload: strings.t('success_added_to_cart')});

        // yield delay(2000);
        yield put({type: PRODUCT_ADD_TO_CART_RESET, payload: {}});

        //yield put({type: CUSTOMER_CART_REQUEST, payload: payload});


    } catch (error) {
        // yield delay(10);
        //////console.log(error)

        yield put({type: PRODUCT_ADD_TO_CART_FAIL, payload: payload});

    }
}

function* getVariant({payload}) {
    try {

        yield put({type: PRODUCT_VARIANT_LOADING});


        var data = null;

        data = yield call(actionGetProductVendor, payload);


        if (data.status === 200) {

            yield put({type: PRODUCT_VARIANT_SUCCESS, payload: data.response.data});
        }
        else {

            yield put({type: PRODUCT_VARIANT_FAIL, payload: data.errors});
        }

    } catch (error) {
        yield put({type: PRODUCT_VARIANT_FAIL, payload: []});

    }
}

export default function* watcherSaga() {
    yield takeLatest(PRODUCT_ADD_TO_CART, addToCart);
    yield takeLatest(GET_VARIANT_PRODUCT, getVariant);

}
