import {
    GET_VARIANT_PRODUCT,
    OPEN_SELECTED_PRODUCT,
    PRODUCT_ADD_TO_CART, PRODUCT_ADD_TO_CART_RESET, PRODUCT_ADD_TO_FAVORITE,
    PRODUCT_LIST,
    PRODUCT_LIST_RESET,
    PRODUCT_RELATED
} from "../../constants/actionsType";

export const openSelectedProduct = product => (
    {
        type: OPEN_SELECTED_PRODUCT,
        payload: product,
    });

export const getVariant = product => (
    {
        type: GET_VARIANT_PRODUCT,
        payload: product,
    });


export const resetProductList = payload => (
    {
        type: PRODUCT_LIST_RESET,
        payload: payload,
    });

export const resetAddToCart = payload => (
    {
        type: PRODUCT_ADD_TO_CART_RESET,
        payload: payload,
    });


export const addToCart = (cartItem) => ({
    type: PRODUCT_ADD_TO_CART,
    payload: cartItem,
});

