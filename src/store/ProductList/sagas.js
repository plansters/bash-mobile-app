import {takeLatest, call, put, takeEvery} from 'redux-saga/effects';
import {I18nManager} from 'react-native';
import axios from 'axios';
import {
    CATEGORIES_FAIL,
    CATEGORIES_LOADING, CATEGORIES_SUCCESS,
    CATEGORY_SUB_LIST, CATEGORY_SUB_LIST_FAIL, CATEGORY_SUB_LIST_LOADING, CATEGORY_SUB_LIST_SUCCESS,
    CUSTOMER_LOGIN_FAIL,
    HOME_FEATURE_PRODUCTS,
    HOME_FEATURE_PRODUCTS_FAIL,
    HOME_FEATURE_PRODUCTS_LOADING,
    HOME_FEATURE_PRODUCTS_SUCCESS,
    HOME_HOME_CATEGORIES,
    HOME_HOME_CATEGORIES_FAIL,
    HOME_HOME_CATEGORIES_LOADING,
    HOME_HOME_CATEGORIES_SUCCESS,
    HOME_RECENT_PRODUCTS,
    HOME_RECENT_PRODUCTS_FAIL,
    HOME_RECENT_PRODUCTS_LOADING,
    HOME_RECENT_PRODUCTS_SUCCESS,
    HOME_TOP_CATEGORIES,
    HOME_TOP_CATEGORIES_FAIL,
    HOME_TOP_CATEGORIES_LOADING,
    HOME_TOP_CATEGORIES_SUCCESS,
    HOME_TRENDING_PRODUCTS,
    HOME_TRENDING_PRODUCTS_FAIL,
    HOME_TRENDING_PRODUCTS_LOADING,
    HOME_TRENDING_PRODUCTS_SUCCESS,
    LOADING,
    PRODUCT_LIST, PRODUCT_LIST_FAIL,
    PRODUCT_LIST_LOADING, PRODUCT_LIST_SUCCESS,
    PRODUCT_RELATED,
    PRODUCT_RELATED_FAIL,
    PRODUCT_RELATED_LOADING,
    PRODUCT_RELATED_SUCCESS,
} from '../../constants/actionsType';
import {config, url, url_dokan} from '../../api/api';
import {Reactron} from '../../all';

const actionGetProductList = (payload) => {


    var lang = I18nManager.isRTL ? 'lang=ar&' : 'lang=en&';

    return axios.get(url + 'categories/' + payload.category + '/products' + '?page=' + payload.page + '&per_page=5&' + lang + payload.order_by,
        config)
        .then(function (response) {
            return {status: 200, response: response.data, message: ''};
        })
        .catch(function (error) {
            return {status: 400, response: null, message: error.response.data.errors};
        });
};


function* getProductList({payload}) {
    try {


        yield put({type: PRODUCT_LIST_LOADING});


        var data = null;
        ////console.log(payload)

        if (payload.category) {

            ////console.log(payload)
            data = yield call(actionGetProductList, payload);
        }

        if (data.status === 200) {

            yield put({
                type: PRODUCT_LIST_SUCCESS,
                payload: {refresh: payload.refresh, data: data.response.data, last_page: data.response.meta.last_page},
            });
        }
        else {

            yield put({type: PRODUCT_LIST_FAIL, payload: data.message});
        }

    } catch (error) {

        yield put({type: PRODUCT_LIST_FAIL, payload: []});

    }
}

export default function* watcherSaga() {
    yield takeEvery(PRODUCT_LIST, getProductList);


}
