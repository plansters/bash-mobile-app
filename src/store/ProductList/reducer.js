import {
    FAILURE,
    LOADING,
    SUCCESS,
    PRODUCT_LIST_LOADING,
    PRODUCT_LIST_SUCCESS,
    PRODUCT_LIST_FAIL,
    PRODUCT_LIST_RESET,
    PRODUCT_DURATION_RESET,
    CATEGORY_SUB_LIST_LOADING, CATEGORY_SUB_LIST_SUCCESS, CATEGORY_SUB_LIST_FAIL, CATEGORY_SUB_LIST_RESET,
} from '../../constants/actionsType';

const INITIAL_STATE = {
    isLoadingAction: false,
    onEndReachedCalledDuringMomentum: true,
    ErrorMessage: '',
    productList: [],
    status: '',
    last_page: 1,

};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {

        case PRODUCT_LIST_LOADING:
            return {
                ...state,
                isLoadingAction: true,
                status: LOADING,
            };

        case PRODUCT_LIST_SUCCESS:
            if (payload.refresh) {

                return {
                    ...state,
                    isLoadingAction: false,
                    status: SUCCESS,
                    ErrorMessage: '',
                    productList: payload.data,
                    last_page: payload.last_page,
                    onEndReachedCalledDuringMomentum: true,
                };
            }
            else {

                return {
                    ...state,
                    isLoadingAction: false,
                    status: SUCCESS,
                    ErrorMessage: '',
                    productList: [...state.productList, ...payload.data],
                    last_page: payload.last_page,
                    onEndReachedCalledDuringMomentum: true,
                };
            }

        case PRODUCT_LIST_FAIL:
            return {
                ...state,
                isLoadingAction: false,
                status: FAILURE,
                ErrorMessage: payload,
                last_page: 1,
                onEndReachedCalledDuringMomentum: true,

            };

        case PRODUCT_LIST_RESET:
            return {

                isLoadingAction: false,
                ErrorMessage: '',
                productList: [],
                status: '',
                last_page: 1,
                onEndReachedCalledDuringMomentum: true,

            };


        default:
            return state;
    }
};


