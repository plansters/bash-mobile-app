import {
    HOME_FEATURE_PRODUCTS,
    HOME_RECENT_PRODUCTS,
    HOME_TRENDING_PRODUCTS,
    HOME_TOP_CATEGORIES,
    HOME_HOME_CATEGORIES, PRODUCT_RELATED, PRODUCT_LIST, PRODUCT_LIST_RESET, PRODUCT_DURATION_RESET, CATEGORY_SUB_LIST,
} from '../../constants/actionsType';

export const productLists = payload => (
    {
        type: PRODUCT_LIST,
        payload: payload,
    });


export const productListsRest = payload => (
    {
        type: PRODUCT_LIST_RESET,
        payload: payload,
    });

