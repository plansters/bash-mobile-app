import {takeLatest, call, put, delay} from 'redux-saga/effects';
import {I18nManager} from 'react-native';
import axios from 'axios';
import {
    CATEGORIES,
    CATEGORIES_FAIL,
    CATEGORIES_LOADING,
    CATEGORIES_SUCCESS,
    LOADING,
    PLACE_ORDER_REQUEST,
    PLACE_ORDER_REQUEST_FAIL,
    PLACE_ORDER_REQUEST_LOADING,
    PLACE_ORDER_REQUEST_SUCCESS,
    PLACE_ORDER_RESET,
} from '../../constants/actionsType';
import {config, url} from '../../api/api';
import {Reactron} from '../../all';

const actionPlaceOrder = (payload) => {

    var lang = I18nManager.isRTL ? 'lang=ar&' : 'lang=en&';

    return axios.post(url + 'orders?'+lang,
        payload,
        config)
        .then(function (response) {

            console.log('سعؤؤثسسsubmit Order');
            return {status: 200, response: response.data, message: ''};
        })
        .catch(function (error) {
            console.log('error sumbit  Order');

            return {status: 400, response: null, message: error.response.data.errors};
        });

};


function* placeOrder({payload}) {
    try {

        yield put({type: PLACE_ORDER_REQUEST_LOADING});

        // console.log('submit Order');
        // console.log(payload);
        // Reactron.warn(payload);
        ////console.log(JSON.stringify(payload));
        const data = yield call(actionPlaceOrder, payload);

        if (data.status === 200) {

            yield put({type: PLACE_ORDER_REQUEST_SUCCESS, payload: data.response});
        }
        else {

            yield delay(100)
            yield put({type: PLACE_ORDER_REQUEST_FAIL, payload: data.message});
           //
           //  yield delay(2000)
           // yield put({type: PLACE_ORDER_RESET, payload: data.message});
        }

    } catch (error) {
        //////console.log(error);
        yield put({type: PLACE_ORDER_REQUEST_FAIL, payload: error.errorMessage});
    }
}

export default function* watcherSaga() {
    yield takeLatest(PLACE_ORDER_REQUEST, placeOrder);
}
