import {
    FAILURE,
    LOADING,
    SUCCESS,
    CATEGORIES_LOADING,
    CATEGORIES_SUCCESS,
    CATEGORIES_FAIL,
    OPEN_SELECTED_CATEGORY,
    PLACE_ORDER_REQUEST_LOADING,
    PLACE_ORDER_REQUEST_SUCCESS,
    PLACE_ORDER_REQUEST_FAIL, PLACE_ORDER_RESET,
} from '../../constants/actionsType';

const INITIAL_STATE = {
    isLoadingAction: false,
    statusPlaceHolder: '',
    ErrorMessagePlaceHolder:[],
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {
        case PLACE_ORDER_RESET:
            return {
                isLoadingAction: false,
                statusPlaceHolder: '',
                ErrorMessagePlaceHolder:[] ,
            };
        case PLACE_ORDER_REQUEST_LOADING:
            return {
                ...state,
                isLoadingAction: true,
                statusPlaceHolder: LOADING,
                ErrorMessagePlaceHolder: [],

            };

        case PLACE_ORDER_REQUEST_SUCCESS:
            return {
                ...state,
                isLoadingAction: false,
                statusPlaceHolder: SUCCESS,
                ErrorMessagePlaceHolder: [],
            };
        case PLACE_ORDER_REQUEST_FAIL:

            return {
                ...state,
                isLoadingAction: false,
                statusPlaceHolder: FAILURE,
                ErrorMessagePlaceHolder: payload,
            };

        default:
            return state;
    }
};


