import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
// import logger from 'redux-logger';
import reducers from './reducers';
import rootSaga from './sagas';
import {sagaMonitor} from './../../ReactotronConfig';


const sagaMiddleware = createSagaMiddleware({sagaMonitor});
const middlewares = [sagaMiddleware];

const store = createStore(
    reducers,
    applyMiddleware(...middlewares),
);

sagaMiddleware.run(rootSaga);

export default store;
