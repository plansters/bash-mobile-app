import {
    OFFER_LIST,
    OFFER_LIST_RESET, SEARCH_LIST, SEARCH_LIST_RESET,
} from '../../constants/actionsType';

export const SearchList = payload => (
    {
        type: SEARCH_LIST,
        payload: payload,
    });

export const SearchListRest = payload => (
    {
        type: SEARCH_LIST_RESET,
        payload: payload,
    });

