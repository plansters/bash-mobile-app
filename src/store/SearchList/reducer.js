import {
    FAILURE,
    LOADING,
    SUCCESS,
    PRODUCT_LIST_LOADING,
    PRODUCT_LIST_SUCCESS,
    PRODUCT_LIST_FAIL,
    PRODUCT_LIST_RESET,
    PRODUCT_DURATION_RESET,
    OFFER_LIST_LOADING,
    OFFER_LIST_SUCCESS,
    OFFER_LIST_FAIL,
    OFFER_LIST_RESET,
    SEARCH_LIST_LOADING,
    SEARCH_LIST_SUCCESS,
    SEARCH_LIST_FAIL, SEARCH_LIST_RESET,
} from '../../constants/actionsType';

const INITIAL_STATE = {
    isLoadingAction: false,
    ErrorMessage: '',
    searchList: [],
    status: '',
    last_page: 1,
};

export default (state = INITIAL_STATE, {type, payload}) => {
    switch (type) {

        case SEARCH_LIST_LOADING:
            return {
                ...state,
                isLoadingAction: true,
                status: LOADING,
            };

        case SEARCH_LIST_SUCCESS:
            if (payload.refresh) {

                return {
                    ...state,
                    isLoadingAction: false,
                    status: SUCCESS,
                    ErrorMessage: '',
                    searchList: payload.data,
                    last_page: payload.last_page,
                };
            }
            else {

                return {
                    ...state,
                    isLoadingAction: false,
                    status: SUCCESS,
                    ErrorMessage: '',
                    searchList: [...state.searchList, ...payload.data],
                    last_page: payload.last_page,
                };
            }
        case SEARCH_LIST_FAIL:
            return {
                ...state,
                isLoadingAction: false,
                status: FAILURE,
                ErrorMessage: payload,
                last_page: 1,

            };


        case SEARCH_LIST_RESET:
            return {
                isLoadingAction: false,
                ErrorMessage: '',
                searchList: [],
                status: '',
                last_page: 1,


            };


        default:
            return state;
    }
};


