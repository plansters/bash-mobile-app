import I18n from "i18n-js";
import * as RNLocalize from "react-native-localize";
import en from "./en";
import ar from "./ar";
import {I18nManager} from 'react-native'

const locales = RNLocalize.getLocales();
////////console.log("DEf language");
////////console.log(locales)
if (Array.isArray(locales)) {
    I18nManager.allowRTL(true)
    // I18nManager.forceRTL(true)
    ////////console.log(I18nManager.isRTL)
    if (I18nManager.isRTL) {

        I18n.locale ="ar-US";

        I18nManager.forceRTL(true)
        // I18n.locale = "ar";
    }
    else {
        I18nManager.forceRTL(false)

        I18n.locale = "en-US";

        // I18n.locale = "en";
    }
}
// I18n.locale = RNLanguages.language;
// I18n.fallbacks = true;
I18n.fallbacks = true;
I18n.translations = {
    en,
    ar
};

export default I18n;
